\chapter{Desarrollo del proyecto}

En el siguiente capítulo se detalla el proceso de recopilación de los datos, una explicación del procesamiento de los mismos y los resultados obtenidos.

El \emph{framework} implementado en este \gls{tfg} sigue el esquema de la figura \ref{fig:workflow}.


\section{Datos y recopilación}

\paragraph{Información de los jugadores}
\label{par:datos}

Información objetiva como la posición usual del jugador, generalizada a las 4 posiciones habituales portero, defensa, mediocentro y delantero.

Y por otro lado, información relativa al jugador. En concreto 34 indicadores disponibles en \url{https://sofifa.com/}. Esta página ofrece valoraciones respectivas a jugadores de fútbol todas las ligas del mundo, información que pueder ser de utilidad para modelar el comportamiento de los jugadores fielmente en videojuegos como Fifa. Dichos indicadores son asignados a cada jugador por unos 1300 ojeadores según afirman Shin y Gasparyan \cite{shin2014novel}. Estos datos son actualizados aproximadamente cada 4 días y la gran ventaja de este portal es su accesibilidad a la evolución histórica de estos indicadores a lo largo de los años.

Estos 34 atributos se dividen en 7 categorías (ejemplo en la Figura \ref{fig:sofifa}):
\begin{itemize}
    \item \textit{Ofensiva}: centros, definición, precisión cabeza, pases cortos y voleas.
    \item \textit{Técnica}: regates, efecto, precisión faltas, pases largos y control de balón.
    \item \textit{Movimiento}: aceleración, velocidad, agilidad, reflejos y equilibrio.
    \item \textit{Potencia}: potencia, salto, resistencia, fuerza y tiros lejanos.
    \item \textit{Mentalidad}: agresividad, intercepción, colocación, visión, penaltis y compostura.
    \item \textit{Defensa}: marcaje, robos y entrada agresiva.
    \item \textit{Portero}: estirada, paradas, saques, colocación y reflejos.
\end{itemize}

\begin{figure}[!htbh]
    \center\includegraphics[scale=0.5]{sofifa.png}
    \caption{\label{fig:sofifa}Información subjetiva ofrecida en la web.}
\end{figure}

Como este servicio no dispone de una API que facilite el acceso a toda esta información, se ha tenido que \textit{scrapear\footnote{Técnica utilizada para extraer información de sitios web, normalmente simulando a un humano.}} directamente la página web para posteriormente guardar los datos en una base de datos.

\paragraph{Información de los partidos}

Es difícil que un mismo proveedor suministre tanto información de los partidos como de los jugadores. Por ello, se solicitaron los servicios de \url{https://www.football-data.org/}. De esta manera, se pudo acceder y guardar el once inicial de cada partido de las temporadas 2017-2018, 2018-2019 y 2019-2020 de las 15 mayores competiciones mundiales a un precio asequible, como han sido: el mundial de fútbol \emph{FIFA World Cup}, \emph{Bundesliga} alemana, \emph{Eredivisie} holandesa, \emph{Série A} brasileña, \emph{Primera} y \emph{Segunda División} españolas, \emph{FFA Cup} australiana, \emph{Playoffs} austríacos, \emph{Primeira Liga} portuguesa, \emph{European Championship} europea, \emph{Serie A} italiana, \emph{Premier League} inglesa, \emph{Supercopa} y \emph{Copa del Rey} españolas. Sin embargo, alguna de ellas no han estado disponibles durante el período de desarrollo o simplemente no se han tenido en cuenta por la naturaleza ``eliminatoria'' del propio campeonato, como la \emph{Copa del Rey} o la \emph{European Championship}, por lo que se han omitido el análisis de dichas competiciones. Un total de 3640 partidos se han analizado y procesado como se explica a continuación.

\section{Almacenamiento de los datos}

Una vez recopilados los datos en bruto, la forma de almacenamiento pensada ha sido la de una base de datos. En la Figura \ref{fig:database} del anexo se muestra el diseño de la base de datos relacional implementada en MySQL.



\section{Preprocesamiento}

\subsection{Clase a predecir}

Al igual que en el estudio de \cite{shin2014novel}, el resultado a predecir será un valor que toma únicamente los dos valores siguientes para una clasificación binaria:
\label{eq:clase}
\begin{equation*}
    C =
    \begin{cases}
        0 & \text{Si el equipo local gana} \\
        1 & \text{En cualquier otro caso} \\
    \end{cases}
\end{equation*}

En caso de una clasificación multiclase de 3 resultados posibles:
\begin{equation*}
    C =
    \begin{cases}
        0 & \text{Si el equipo local gana} \\
        1 & \text{Empate} \\
        2 & \text{Si el equipo visitante gana} \\
    \end{cases}
\end{equation*}

\subsection{Agregación: variables de ``alto nivel''}
\label{sub:agregacion}

Como se muestra en el apartado \ref{par:datos}, los atributos se dividen en 7 categorías distintas, cada una representando cierta \textit{cualidad} que todos lo jugadores pueden compartir. Por ejemplo, un portero destacará en el apartado de \emph{Portero} a diferencia de un delantero que destacará posiblemente en \emph{Ofensiva}.

Cada uno de los atributos mencionados se encuentra en el rango $[0, 100]$, así que una vez obtenidas las alineaciones de un partido entre un equipo A y un equipo B, sólo quedaría generar las dos instancias del partido, una para cada equipo, para posteriormente analizarlas. Es decir, sacar una \textit{foto\footnote{El término \textit{foto} viene a representar una captura del estado del equipo en ese momento, de manera que le defina, como posteriormente veremos, mediante 7 valores numéricos.}} de la situación de cada equipo antes del partido, según se indica en el estudio\cite{shin2014novel}.

Dicha \textit{foto} recogerá el concepto con el que nutrir los clasificadores, las variables con los que aprenden los modelos. El cómputo de las variables finales se realiza de la siguiente manera:
\begin{enumerate}
    \item Calcular la media de los atributos en cada una de los 7 grupos de cada jugador del 11 inicial. Es decir, la media de \textit{Ofensiva}, \textit{Defensa}, \textit{Técnica}, \textit{Movimiento}, \textit{Potencia}, \textit{Mentalidad} y \textit{Portero}.
    \item Obtener los 4 jugadores\footnote{Los autores del estudio\cite{shin2014novel} realizaron varias pruebas con distintas características y cantidades de jugadores para computar las variables finales con las que computan los predictores. Finalmente se quedaron con el \emph{top} $[4, 5, 5, 5, 5, 4, 1]$ de jugadores, correspondientes a $[Ofensiva, Tecnica, Movimiento, Potencia,$ $Mentalidad, Defensa, Portero]$, respectivamente.} del once inicial con mejor valoración en los grupos de variables \textit{Ofensiva} y \textit{Defensa} y los 5 mejores de \textit{Técnica}, \textit{Movimiento}, \textit{Potencia} y \textit{Mentalidad}. Sumar los valores correspondientes, por cada apartado de ese top de 4/5 jugadores obtenido.

    Es decir, obtener una \emph{suma} en \textit{Ofensiva} que se encuentre entre $[0, 2000]$, una suma en \textit{Técnica} que se encuentre entre $[0, 2500]$, una suma en \textit{Movimiento} que se encuentre entre $[0, 2500]$, una suma en \textit{Potencia} que se encuentre entre $[0, 2500]$, una suma en \textit{Mentalidad} que se encuentre entre $[0, 2500]$ y una suma en \textit{Defensa} que se encuentre entre $[0, 1200]$.

    \item Añadirle al vector de estas características los 5 atributos \textit{Portero} del portero titular sumados. Esto es porque el portero tiene un rol genuino y diferente al resto de jugadores.
\end{enumerate}

De esta manera, se obtiene un \emph{estado} actual del equipo previo al partido en forma de vector numérico. Es decir, a un equipo lo representa este vector de 7 variables agregadas que representan 7 sumas:
\label{eq:agreg}
\begin{equation*}
    foto(equipo) =
    \begin{bmatrix*}[l]
        \sum Ofensiva, \\
        \sum Tecnica, \\
        \sum Movimiento, \\
        \sum Potencia,  \\
        \sum Mentalidad,  \\
        \sum Defensa, \\
        \sum Portero \\
    \end{bmatrix*}
\end{equation*}

\subsection{Interpretación como \emph{características de bajo nivel}}

Se propone una seguna forma de computar las variables de un equipo. La representación propuesta aquí da un enfoque distinto, en el que no existen agregaciones como las mencionadas y en la se podría asimilar que un partido se ``visualiza'', literalmente, como una imagen.

\begin{figure}[!htbh]
    \center\includegraphics[scale=0.75]{input}
    \caption{\label{fig:input}Interpretación de bajo nivel de un partido aleatorio de la base de datos.}
\end{figure}

La Figura \ref{fig:input} representa un partido, disputado por dos equipos, local y visitante. La imagen cuenta con una distribución de $22 \times 34$ píxeles en escala de grises, con valores en el rango $[0, 100]$. Los valores cercanos a 0 se representan en tonos más oscuros, mientras que los cercanos a 100, en tonos más claros, en la que la mitad superior identifica al equipo local y la inferior al equipo visitante.

En cuanto al significado de cada celda, se trata de visualizaciones de los mismos atributos individuales explicados en la Sección \ref{fig:sofifa}. Las columnas representan las valoraciones de los 34 atributos de cada jugador mientras que las filas almacenan el once titular de cada equipo. Se puede apreciar el mismo patrón, al inicio del equipo local en la fila 1 y al inicio del equipo visitante en la fila 12. Estas dos indican los atributos de ambos porteros titulares, donde se muestran claramente que las aptitudes propias de \emph{Portero} sobresalen respecto a las de \emph{Ofensiva} o \emph{Técnica}. En cambio, el bloque predominante oscuro de la parte derecha indica que son jugadores de campo usuales, puesto que las aptitudes referidas a los porteros son prácticamente nulas.

De esta manera se ha podido construir un conjunto de 3640 imágenes (tensores 2D) de los 3640 partidos disputados de las competiciones durante las temporadas 2018-2019 y 2019-2020 con el que entrenar los modelos neuronales.


La diferencia entre una característica de ``alto nivel'' frente a una de ``bajo nivel'', es la información que puede proporcionar. Mientras una de ``alto nivel'' puede contener información esencial para definir la clase para una instancia dada, una característica de ``bajo nivel'' apenas aporta información útil por sí sola. Se puede entender fácilmente con el siguiente símil: mirando un sólo pixel de una imagen, es prácticamente imposible conocer el contenido de la imagen, pero a medida que vamos aumentando la cantidad de píxeles, se puede comenzar a observar líneas, formas y objetos. Tradicionalmente, los modelos de \gls{dl} han utilizado este tipo de características obteniendo buenos resultados. Comprobaremos esta hipótesis experimentalmente.


\section{Configuración paramétrica de los modelos}

En el Anexo \ref{anexo:dl_arch} quedan disponibles los sumarios de los modelos utilizados de forma detallada.

\subsection{Árbol de decisión}

Se han definido dos de los múltiples parámetros que están disponibles en la constructora del modelo \texttt{DecisionTreeClassifier}. Por un lado, se ha elegido como criterio de separación de los nodos la reducción de la entropía de la clase dado el atributo a considerar para la separación y por otro lado, se ha establecido una profundidad máxima de 5 niveles. Se puede visualizar el árbol aprendido para clasificación binaria en la Figura \ref{fig:dt_arch_2}. Para clasificación multiclase, el árbol resultante es demasiado amplio para caber en este documento, por lo que queda disponible online\footnote{\url{https://gitlab.com/r3v1/Football-api/-/blob/master/doc/archivos/images/resultados/dt_arch_3.pdf}}.

\subsection{Perceptrón multicapa}

La arquitectura del perceptrón multicapa ha seguido un diseño simple, como se muestra en la Figura \ref{fig:mlp_arch}. Los datos de entrada se transforman de un tensor 2D, como el de la Figura \ref{fig:input}, en un tensor 1D aplanado.

\begin{figure}
    \center\includegraphics[scale=0.8]{archivos/images/mlp_1.pdf}
    \caption{\label{fig:mlp_arch}Arquitectura propuesta, compuesta por 3 capas densas.}
\end{figure}

\subsection{Red Convolucional}

Gracias a las sugerencias de Unai Garciarena, del grupo de investigación \emph{Intelligent System Group} de la facultad, los procesos de convolución se realizaron de la siguiente manera. Teniendo en cuenta que los píxeles (ver Figura \ref{fig:input}) no presentan ninguna relación con los píxeles circundantes, no tenía ninguna lógica aplicar filtros cuadrados ($e.g.\, n \times n$) a las convoluciones, como se suelen aplicar normalmente. Por ello, se ha optado por aplicar filtros de \emph{filas} ($e.g.\, n \times 1$) o \emph{columnas} ($e.g.\, 1 \times n$).

Tras numerosas pruebas configurando manualmente la arquitectura de la red, se han diseñado las arquitecturas de las Figuras \ref{fig:cnn1} y \ref{fig:cnn2}.


\begin{figure}
    \center\includegraphics[scale=0.5]{archivos/images/cnn_1.pdf}
    \caption{\label{fig:cnn1}\texttt{CNN-1}: Primera arquitectura propuesta. Está compuesta por 2 capas convolucionales (\texttt{Conv1} y \texttt{Conv2}) seguidas de los bloques compuestos por capa convolucional y \emph{pooling layer} (bloques \texttt{Conv3} y \texttt{Conv4}) y finalmente, 2 capas totalmente conectadas donde sucede la clasificación (\texttt{FC1} y \texttt{FC2}).}
\end{figure}


\begin{figure}
    \center\includegraphics[scale=0.5]{archivos/images/cnn_2.pdf}
    \caption{\label{fig:cnn2}\texttt{CNN-2}: Segunda arquitectura propuesta. Está compuesta por 2 capas convolucionales seguidas de \emph{pooling layers} y finalmente, 2 capas totalmente conectadas donde sucede la clasificación (\texttt{FC1} y \texttt{FC2}).}
\end{figure}


\section{Resultados}

A continuación se detallan los resultados obtenidos.

Cabe recordar la nomenclatura utilizada que relaciona las clases con el resultado observado, en la Sección \ref{eq:clase}.

\paragraph{Punto de partida}

El punto de partida ha sido el siguiente. Como se muestra en la representación de las instancias de la Figura \ref{fig:pca}, la tarea principal de los clasificadores ha sido la de establecer las fronteras de decisión que pudieran definir las distintas clases. Sin embargo, no es una tarea sencilla puesto que las clases del problema se encuentran solapadas y divisar estas ``fronteras'' es inviable para una persona, tal vez ni existan sin solapamiento dichas fronteras para este problema.

\paragraph{Métrica utilizada}

Como métrica de evaluación, se ha utilizado el ``porcentaje de bien clasificados" o \emph{accuracy}, que se define como el número de resultados acertados respecto al total de casos.

\subsection{Técnicas de \acrlong{ml}}

Los algoritmos clásicos han sido ejecutados en el siguiente cuaderno Jupyter\footnote{\url{https://gitlab.com/r3v1/Football-api/-/blob/master/footballapi/models/ml_models_comparison.ipynb}}.

Se ha hecho una validación cruzada de 10 hojas del conjunto de datos y así calcular el procentaje de bien clasificados por cada modelo.

\subsection{Técnicas de \acrlong{dl}}

Para el análisis de algoritmos de \gls{dl}, se ha procedido de la misma forma. Se ha realizado una \emph{cross-validación} de 10 hojas, entrenando cada modelo esta vez durante 25 épocas.


\section{Análisis de los resultados}

\begin{table}
    \centering
    \resizebox{15cm}{!}{
        \begin{tabular}{c|cccccccc|ccc}
            \toprule
            \multirow{2}{*}{Fold} & \multicolumn{8}{c|}{\acrlong{ml}} & \multicolumn{3}{c}{\acrlong{dl}} \\
            & \acrshort{tpot} & \acrshort{svm} & \acrshort{xgb} & \acrshort{rf} & \acrshort{dt} & \acrshort{nb} & Voting & \acrshort{mlp} & \acrshort{cnn}-1 & \acrshort{cnn}-2 & \acrshort{mlp}  \\
            \hline\hline
             1  & 0.6154 & \textbf{0.6374} & 0.5549 & 0.6319 & 0.6099 & 0.4670 & 0.6291 & 0.5283 & 0.5780 &0.5988& 0.5201 \\
             2  & 0.6264 & 0.6374 & 0.5934 & \textbf{0.6401} & 0.5934 & 0.4286 & 0.6291 & 0.5548 & 0.5682 &0.5864& 0.5543 \\
             3  & 0.6648 & \textbf{0.6758} & 0.6236 & 0.6676 & 0.6154 & 0.4396 & \textbf{0.6758} & 0.5444 & 0.5721 &0.6039& 0.5663 \\
             4  & 0.5989 & 0.6181 & 0.5687 & \textbf{0.6236} & 0.5769 & 0.4478 & 0.6154 & 0.5572 & 0.5717 &0.6010& 0.5642 \\
             5  & 0.5714 & \textbf{0.6181} & 0.5962 & 0.6016 & 0.5962 & 0.4918 & 0.6044 & 0.5887 & 0.5664 &0.5826& 0.5318 \\
             6  & 0.6044 & 0.6154 & 0.5412 & \textbf{0.6291} & 0.5797 & 0.4533 & 0.6071 & 0.5560 & 0.5274 &0.5721& 0.5141 \\
             7  & 0.6319 & 0.6429 & 0.5907 & \textbf{0.6456} & 0.6319 & 0.4313 & \textbf{0.6456} & 0.5531 & 0.5443 &0.5999& 0.5357 \\
             8  & 0.5495 & 0.5934 & 0.5714 & 0.5852 & 0.5852 & 0.4588 & 0.5824 & 0.5412 & 0.5490 &\textbf{0.5955}& 0.5235 \\
             9  & 0.6264 & \textbf{0.6484} & 0.6044 & 0.6319 & 0.6346 & 0.4038 & 0.6319 & 0.5196 & 0.5478 &0.5728& 0.5222 \\
             10 & 0.5978 & 0.5950 & 0.5813 & 0.5785 & 0.5675 & 0.4601 & 0.5785 & 0.5684 & 0.5682 &\textbf{0.6208}& 0.5399 \\
             \midrule
             $avg$ & 0.6087 & \textbf{0.6282} & 0.5826 & 0.6235 & 0.5991 & 0.4482 & 0.6199 & 0.5512 & 0.5593 & 0.5934 & 0.5372 \\
             $std$ & 0.0308 & 0.0239 & 0.0231 & 0.0262 & 0.0220 & 0.0229 & 0.0276 & 0.0417 & \textbf{0.0153} & 0.0142 & 0.0364 \\
             $min$ & 0.5495 & \textbf{0.5934} & 0.5412 & 0.5785 & 0.5675 & 0.4038 & 0.5785 & 0.4313 & 0.5274 & 0.5721 & 0.4243 \\
             $max$ & 0.6648 & \textbf{0.6758} & 0.6236 & 0.6676 & 0.6346 & 0.4918 & \textbf{0.6758} & 0.6511 & 0.5780 & 0.6208 & 0.6473 \\
            \bottomrule
        \end{tabular}
    }
    \caption{\label{tab:res_2}Porcentaje de bien clasificados obtenido en cada hoja de la \emph{cross-validation} para 2 clases (victoria local o no).}
\end{table}

\begin{table}
    \centering
    \resizebox{15cm}{!}{
        \begin{tabular}{c|cccccccc|ccc}
            \toprule
            \multirow{2}{*}{Fold} & \multicolumn{8}{c|}{\acrlong{ml}} & \multicolumn{3}{c}{\acrlong{dl}} \\
            & \acrshort{tpot} & \acrshort{svm} & \acrshort{xgb} & \acrshort{rf} & \acrshort{dt} & \acrshort{nb} & Voting & \acrshort{mlp} & \acrshort{cnn}-1 & \acrshort{cnn}-2 & \acrshort{mlp}  \\
            \hline\hline
             1  & 0.4973 & \textbf{0.5137} & 0.4258 & \textbf{0.5137} & 0.4725 & 0.4643 & 0.5055 & 0.4451 &0.4673&0.4819& 0.4582 \\
             2  & 0.5000 & 0.4945 & 0.4533 & 0.5027 & 0.5027 & 0.4286 & \textbf{0.5055} & 0.4176 &0.4454&0.4675& 0.4097 \\
             3  & 0.5137 & \textbf{0.5220} & 0.4588 & \textbf{0.5220} & 0.4863 & 0.4396 & 0.5192 & 0.4368 &0.4256&0.4675& 0.4649 \\
             4  & 0.4780 & 0.4863 & 0.4313 & 0.4808 & 0.4615 & 0.4505 & \textbf{0.4890} & 0.4643 &0.4676&0.4869& 0.4622 \\
             5  & 0.5385 & \textbf{0.5467} & 0.4808 & 0.5302 & 0.5082 & 0.4918 & 0.5357 & 0.4286 &0.4236&0.4568& 0.4378 \\
             6  & 0.4835 & \textbf{0.4890} & 0.4313 & 0.4753 & 0.4588 & 0.4533 & 0.4835 & 0.4286 &0.4448&0.4785& 0.4108 \\
             7  & 0.5000 & 0.4890 & 0.4560 & 0.4835 & 0.4698 & 0.4286 & \textbf{0.5027} & 0.4588 &0.4754&0.4573& 0.4541 \\
             8  & 0.5330 & 0.5302 & 0.4698 & \textbf{0.5412} & 0.4780 & 0.4615 & \textbf{0.5412} & 0.4615 &0.4346&0.4683& 0.4081 \\
             9  & 0.4918 & 0.4890 & 0.4615 & 0.4808 & 0.4505 & 0.4038 & \textbf{0.5000} & 0.4643 &0.4198&0.4828& 0.4568 \\
             10 & 0.4918 & 0.4890 & 0.4341 & 0.4890 & 0.4863 & 0.4588 & 0.4945 & 0.4753 &0.4355&0.4484& \textbf{0.5162} \\
             \midrule
             $avg$ & 0.5027 & 0.5049 & 0.4503 & 0.5019 & 0.4775 & 0.4481 & \textbf{0.5077} & 0.4481 & 0.4440 &0.4696 & 0.4479 \\
             $std$ & 0.0189 & 0.0206 & 0.0177 & 0.0224 & 0.0178 & 0.0230 & 0.0180 & 0.0184 & 0.0189 &\textbf{0.0122} & 0.0315\\
             $min$ & 0.4780 & \textbf{0.4863} & 0.4258 & 0.4753 & 0.4505 & 0.4038 & 0.4835 & 0.4176 & 0.4198 &0.0.4484 & 0.4081 \\
             $max$ & 0.5385 & \textbf{0.5467} & 0.4808 & 0.5412 & 0.5082 & 0.4918 & 0.5412 & 0.4753 & 0.4754 &0.0.4869 & 0.5162 \\
            \bottomrule
        \end{tabular}
    }
    \caption{\label{tab:res_3}Porcentaje de bien clasificados obtenido en cada hoja de la \emph{cross-validation} para 3 clases (victoria local, empate o vistoria visitante).}
\end{table}

En las Tablas \ref{tab:res_2} y \ref{tab:res_3} se muestran los resultados experimentales. Como se observa, los algoritmos de \gls{ml} parecen obtener un mejor rendimiento respecto a las redes neuronales de \gls{dl}. Si bien es cierto que la aplicación de \gls{dl} en problemas como segmentación de imágenes o procesamiento de lenguage natural éstas técnicas se adelantan a las técnicas de \gls{ml}, en éste campo de aplicación parece no ser así.

Si se busca justificar el comportamiento mediocre de las redes neuronales, se pueden dar las siguientes razones:
\begin{itemize}
    \item El número de \emph{epochs} que las redes han tenido para aprender ha sido muy pequeño, concretamente de 25, ya que estos entrenamientos requieren mucha capacidad de cálculo y más aún si se están validando 10 veces como se ha hecho. Estos procesos pueden extenderse considerablemente y habiéndose limitado a 25 \emph{epochs}, es posible que la red no haya llegado a converger en su aprendizaje y aún le quedara margen de mejora.
    \item Otro punto importante a tener en cuenta es la configuración de los parámetros y las arquitecturas propuestas. Se trataría de un problema bien conocido, el de la optimización paramétrica de los modelos, que no sólo afecta a los modelos de \gls{dl}, sino a los de \gls{ml} también, aunque es en el \gls{dl} donde pueden suponer una gran ayuda. Como ya se ha explicado anteriormente, las configuraciones implementadas surgieron a raíz de propuestas y pruebas rápidas y no de un proceso de optimización propio.
\end{itemize}

Como también se ha mencionado anteriormente, las fronteras de decisión del problema son difíciles de divisar según se ha planteado y ejecutado el problema. Por ello, aunque pueda existir cierta mejora en los modelos de predicción, será muy difícil obtener un alto procentaje de predicciones correctas.

Sin embargo, también hay que destacar algunos de los resultados que la red \texttt{CNN-2} de la Figura \ref{fig:cnn2} ha obtenido en la Tabla \ref{tab:res_2}. Se puede ver como en las hojas 8 y 10 ha sido capaz de clasificar correctamente más instancias que los demás clasificadores.








