\chapter{Tecnologías y algoritmos utilizados}

En el siguiente capítulo se muestran  las librerías que han sido requeridas para el proyecto, así como los lenguajes la infraestructura y descripciones de los algoritmos utilizados, tanto de \gls{ml} como de \gls{dl}.

\section{Tecnologías}

\subsection{Lenguajes}

Los lenguajes utilizados en el proyecto han sido lo siguientes:

\paragraph{Python 3} Para prácticamente la totalidad de la implementación del proyecto debido a su fácil legibilidad y sus librerías enfocadas al aprendizaje automático.

\paragraph{HTML, JavaScript} Para la implementación \emph{front-end} del servidor web.
\paragraph{PHP} Para la implementación \emph{back-end} del servidor web.

\paragraph{MySQL} Para la administración de la base de datos.

\paragraph{\LaTeX} Para la documentación.

\subsection{Librerías}

Existe una amplia gama de librerías Open Source dedicadas tanto al \gls{ml} como al \gls{dl}, entre ellas:
\begin{itemize}
    \item \gls{tpot}\cite{OlsonGECCO2016}: Se trata de una librería dedicada a la automatización de \emph{pipelines} de los procesos que entrenan un modelo, como son la lectura de los datos, el preprocesamiento de los mismos (elección del escalador apropiado), extracción de características y la elección del clasificador o clasificadores que se pueden aplicar. Se ubica en el marco del \emph{AutoML}.
    \item Scikit-learn\cite{scikit-learn}: cuenta implementaciones de diversos algoritmos de clasificación supervisada y no supervisada, así como funciones de preprocesamiento de datos y evaluaciones de modelos.
    \item Keras\cite{keras}: API de redes neuronales de alto nivel, el cual soporta varios \emph{backends}\footnote{Parte del software encargada de procesar la información.}, como Tensorflow\cite{tensorflow}. Una gran ventaja de utilizar Keras es la fácil y rápida implementación de modelos neuronales.
    \item Numpy, Scipy, Matplotlib y demás paquetes del área de ``Python científico'' para la gestión eficiente de arrays y visualizaciones.
\end{itemize}

\subsection{Infraestructura}

En cuanto a la infraestructura, se ha utilizado un ordenador de gama media para soportar el servidor MySQL y el servidor web, registrado en el dominio \url{http://futbolpredictor.eus}. Cuenta con una instalación del sistema operativo Arch Linux en su versión \texttt{5.5.8}.

Además, se ha hecho uso del servicio de pago \url{https://www.football-data.org/} para complementar los datos de los partidos con las alineaciones de los equipos titulares y sus resultados.

\subsection{Técnicas de análisis de datos}

Por un lado, los algoritmos \emph{clásicos} utilizados: árbol de decisión, Naïve Bayes, XGBoost, \emph{Random Forests} y \gls{svm}. Y por otro lado, los algoritmos \emph{profundos}: \acrfull{mlp} y \acrfull{cnn}.

La hipótesis que se plantea sería la siguiente: las técnicas de aprendizaje \emph{clásicas} deberían obtener peores resultados frente a las técnicas de \gls{dl} con los datos utilizados, ya que las variables utilizadas son ``low-level''. Estas variables se denominan de bajo nivel porque individualmente, cada una de ellas no está correlacionada con la clase, pero agrupándolas en pequeños subconjuntos es posible extraer información de ellas. Como ejemplo en detección de objetos en imágenes, un sólo pixel no puede aportar la información necesaria como para decidir con claridad qué objeto aparece en la imagen. Sin embargo, la unión de éstos puede detectar bordes o sombras clave para reconocer el objeto en cuestión.

\section{Descripción de los algoritmos de análisis de datos}

Partimos de un problema supervisado, ya que cada instancia está etiquetada con el valor de la variable de interés. Además, esta variable no es un valor numérico ($0.85$, $3.6$, ...) sino categórico (``0'', ``1'', ...), es decir, valores discretos, nominales.

El objetivo es el de predecir dicha etiqueta en base a la información proporcionada por cada instancia de la base de datos: un problema de clasificación supervisada.

\subsection{Algoritmos de \gls{ml}}

La gran ventaja de estos algoritmos es su fácil interpretación del conocimiento extraído y el rápido entrenamiento.

\paragraph{Naïve Bayes}

El primer método utilizado es posiblemente el más conocido, basado en conceptos de probabilidad. En él se aplica el Teorema de Bayes, asumiendo ciertas simplificaciones, como la independencia entre las variables predictoras dada la clase. La probabilidad de que una muestra $\pmb{X} =(X_1=x_1, X_2=x_2, ...,X_n=x_n)$ sea de clase $C=c$ viene dada por:

\begin{equation}
        p(C=c\, |\, \pmb{X}) = \frac{p(\pmb{X}\, |\, C=c)\, p(C=c)}{p(\pmb{X})}
\end{equation}

El numerador podría expresarse como una probabilidad condicional con la `regla de la cadena':

\begin{align*}
        &p(C=c)\, p(\pmb{X} | C=c) = \\
        &= p(C=c)\, p(X_1=x_1 | C=c)\, p(X_2=x_2, ..., X_n=x_n | C=c, X_1=x_1) \\
        &= p(C=c)\, p(X_1=x_1 | C=c)\, p(X_2=x_2 | C=c)\, \\
        &\,\,\,\,\,\,\,\,p(X_3=x_3, ..., X_n=x_n | C=c, X_1=x_1, X_2=x_2) \\
        &= ...
\end{align*}

y puesto que se asume una interpretación \emph{naïve}\footnote{Ingenua.} conocida la clase, se asume que las variables $x_i$ y $x_j$ son independientes para cualquier $i$ y $j$ tal que $i \neq j$, lo que hace que la probabilidad compuesta se resuma en:

\begin{align*}
        &p(X_1=x_1, X_2=x_2, ...,X_n=x_n | C=c) \\
        &\propto p(C=c)\, p(X_1=x_1 | C=c)\, p(X_2=x_2 | C=c)\, ... \\
        &= p(C=c)\, \prod\limits_{i=1}^{n} p(X_i=x_i | C=c)
\end{align*}

Se podrá clasificar una instancia $\pmb{X} = (X_1=x_1, X_2=x_2, ...,X_n=x_n)$ en la clase $c_j$ si

\begin{equation}
        c = \argmax_j\, p(C=c_j)\, \prod\limits_{i=1}^{n} p(X_i=x_i | C=c_j)
\end{equation}

\paragraph{Árboles de decisión}

Es un método conocido desde la década de los 60, siendo las dos variantes más conocidas el \emph{ID3} y el \emph{C4.5}. Durante la construcción de estos árboles, es necesario conocer qué atributo es más relevante para decidir la clase utilizando métodos como la ganancia de información:

\begin{equation}
    IG(X_i|C) = H(C) - H(C|X_i)
\end{equation}

basados en conceptos de la teoría de la información, como la entropía:

\begin{equation}
    H = - \sum\limits_{i=1}^{K} p_i \log_2 p_i
\end{equation}

O la ganancia de información. Utilizando estas métricas entre las variables y la clase se obtiene el árbol de clasificación en el que las hojas representan las etiquetas de la clase a predecir mientras que los nodos intermedios representan las reglas. Es un modelo fácil de interpretar y que se puede visualizar.

Hay que destacar que el utilizado en \emph{scikit-learn}\cite{scikit:decision_trees} es el algoritmo \emph{CART}\cite{wiki:cart}.

\paragraph{\emph{Random Forests}}

Se trata de un método de aprendizaje en conjunto (\emph{ensemble learning}) en el que múltiples árboles de decisión son construídos y entrenados para resolver tareas de clasificación y regresión. Propuesto por Breiman \cite{breiman} en 2001, son una alternativa eficaz para evitar el sobreentrenamiento de los árboles de decisión según la profundidad de éstos. Como bien indica, debido a la Ley de los Grandes Números (\gls{lln}) no causa \emph{overfit} en el entrenamiento llegando a la convergencia y además, si las variables presentan aleatoriedad, produce un buen resultado en la clasificación.

\paragraph{Gradient Boosting}

Esta técnica forma parte del \emph{Ensemble Learning} y consiste en agrupar modelos predictivos ``débiles'' para generar un \emph{ensemble} más fuerte. En concreto, esta técnica construye los modelos secuencialmente para ir reduciendo el sesgo de las estimaciones.

Existen varios algoritmos que implementan esta técnica, pero el seleccionado para este proyecto ha sido \emph{XGBoost}. Se trata de una librería \emph{open-source} que ofrece técnicas de \emph{Gradient Boosting} para problemas de regresión y clasificación. Tiene implementaciones para Linux, Windows y macOS, además de disponer de librerías para múltiples lenguajes como C++, Java y Python entre otros. Ofrece la posibilidad de trabajar en entornos distribuidos lo que le aporta versatilidad. Además, ha sudo el algoritmo elegido por ganadores de competiciones de \gls{ml}\footnote{\url{https://github.com/dmlc/xgboost/tree/master/demo\#machine-learning-challenge-winning-solutions}}.


\paragraph{\acrfull{svm}}

El modelo \gls{svm} construye un conjunto de hiperplanos que permitan separar los casos separándose lo máximo posible de las instancias, es decir, dejando un margen entre el hiperplano y los casos más cercanos de clases diferentes. Fue propuesto originalmente por Vapnik y varios colaboradores en 1992, cuando presentan una manera de crear una clasificador no lineal usando \emph{kernels} para generar los hiperplanos\cite{boser1992training}.

Estos \emph{kernels} funcionan trasladando los puntos originales a un espacio de características de mayor dimensionalidad. Los princiaples son lo siguientes:

\begin{itemize}
    \item Lineal: $K(\pmb x_i, \pmb x_j) = \pmb x_i \cdot \pmb x_j $
    \item Polinomial-homogéneo: $K(\pmb x_i, \pmb x_j) = (\pmb x_i \cdot \pmb x_j)^d$
    \item Función de base radial Gaussiana: $K(\pmb x_i, \pmb x_j) = \exp(-\gamma || \pmb x_i - \pmb x_j || ^2)$ para $\gamma > 0$.
    \item Sigmoide: $K(\pmb x_i, \pmb x_j) = \tanh(\gamma \pmb x_i \cdot \pmb x_j + c)$ para $\gamma > 0$ y $c < 0$.
\end{itemize}

%\label{par:tpot}
%\paragraph{}
%
%No se trata de un algoritmo, si no de una librería completa para Python dedicada a la optimización del \emph{pipeline}  Fue publicada en 2016\cite{OlsonGECCO2016} y esta basada en programación genética. En la imagen \ref{fig:tpot-ml} se muestra el proceso que se sigue para obtener un \emph{pipeline}. Se puede ubicar dentro del área del \emph{AutoML} o el \emph{Automated Machine Learning}, permitiendo a usuarios no experta la utilización de estos sistemas sin la necesidad de saber demasiado.
%
%\begin{figure}[!htbh]
%    \center\includegraphics[scale=0.35]{archivos/images/tpot-ml-pipeline.png}
%    \caption{\label{fig:tpot-ml}Automatización realizada por TPOT. (Github, LGPL)}
%\end{figure}


\subsection{Algoritmos de \gls{dl}}

Estos algoritmos son muy utilizados hoy en día campos como el procesamiento de lenguaje natural, visión por camputador e incluso en optimización, debido a sus buenos resultados por el uso de variables de bajo nivel que utilizan. Sin embargo, los basados en redes neuronales como los que se presentan, son difíciles de entender por su no-linearidad y sus complejas arquitecturas. Además, el tiempo de entrenamiento es mucho mayor frente a las técnicas anteriores y se requiere de tecnología actual, como \glspl{gpu} (o \glspl{tpu}\cite{google:tpu}). En definitiva, no es fácil entrenar estos modelos.
\\

\textit{Nota: de aquí en adelante, la notación que define la predicción para las técnicas clásicas deja de ser} \texttt{c} \textit{y pasa a ser} \texttt{y} \textit{para las redes neuronales}.

\paragraph{\acrlong{mlp}}

Se trata de una ampliación del conocido \emph{perceptrón}\cite{perceptron} presentado por F. Rosenblatt en 1957. El esquema básico del perceptrón sería el siguiente:


\begin{figure}[!htbh]
    \center\includegraphics[scale=0.3]{archivos/images/perceptron_esquema.png}
    \caption{\label{fig:perceptron_esquema}Perceptrón simple de 5 entradas $x_i$, 5 pesos $w_i$ y una función de activación $f$. (Wikipedia, \bysa)}
\end{figure}

Dicho esquema se podría resumir en la siguiente fórmula:

\begin{equation}
    y = f\Big(\sum\limits_i x_i \cdot w_i + b \Big)
\end{equation}

Se trata de una suma ponderada de las variables, a la que se le aplica un sesgo $b$ que finalmente se introduce en la función de activación $f$, y así decidir la predicción, $y$. Esta activación es la que aporta la no linearidad al modelo. Existen múltiples funciones, como $ReLU$ (Figura \ref{fig:relu}) y \emph{sigmoides}\footnote{Funciones que se caracterizan por su forma en $S$.} como $tanh$ (Figura \ref{fig:tanh}) o $logistic$ (Figura \ref{fig:logistic}) entre muchas otras.

\begin{figure}[h!]
    \centering
    \begin{subfigure}{.3\textwidth}
        \resizebox{4cm}{4cm}{
        \begin{tikzpicture}
            \begin{axis}[
                xlabel={$x$}, ylabel={$y=\frac{1}{1+e^{-x}}$},
                %xmin=1, xmax=10,
                %ymin=0.1, ymax=0.6,
                grid=both,
                grid style={line width=.1pt, draw=gray!10},
                major grid style={line width=.2pt,draw=gray!50},
                legend columns=5,
                axis lines=middle,
                legend pos=north east,
                ticklabel style = {font=\large, fill=white},
                label style = {font=\large},
                xlabel style={at={(ticklabel* cs:1)},anchor=north west},
                ylabel style={at={(ticklabel* cs:1)},anchor=south west}
                %xtick={1, 2, 3, 4,5,6,7,8,9,10},
                %ytick={0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6},
                ]
                \addplot[
                    line width=1pt,
                    color=blue
                    ]
                    {1/(1+e^(-x))};
            \end{axis}
        \end{tikzpicture}}
        \caption{\label{fig:logistic}Función logística.}
    \end{subfigure}%
    \begin{subfigure}{0.3\textwidth}
        \resizebox{4cm}{4cm}{
        \begin{tikzpicture}
            \begin{axis}[
                xlabel={$x$}, ylabel={$y=tanh(x)$},
                %xmin=1, xmax=10,
                %ymin=0.1, ymax=0.6,
                grid=both,
                grid style={line width=.1pt, draw=gray!10},
                major grid style={line width=.2pt,draw=gray!50},
                legend columns=5,
                axis lines=middle,
                legend pos=north east,
                ticklabel style = {font=\large, fill=white},
                label style = {font=\large},
                xlabel style={at={(ticklabel* cs:1)},anchor=north west},
                ylabel style={at={(ticklabel* cs:1)},anchor=south west}
                %xtick={1, 2, 3, 4,5,6,7,8,9,10},
                %ytick={0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6},
                ]
                \addplot[
                    line width=1pt,
                    color=blue
                    ]
                    {tanh(x)};
            \end{axis}
    \end{tikzpicture}}
        \caption{\label{fig:tanh}Función $tanh$.}
    \end{subfigure}%
    \begin{subfigure}{0.3\textwidth}
        \resizebox{4cm}{4cm}{
        \begin{tikzpicture}
            \begin{axis}[
                xlabel={$x$}, ylabel={$y=max(0, x)$},
                %xmin=1, xmax=10,
                %ymin=0.1, ymax=0.6,
                grid=both,
                grid style={line width=.1pt, draw=gray!10},
                major grid style={line width=.2pt,draw=gray!50},
                legend columns=5,
                axis lines=middle,
                legend pos=north east,
                ticklabel style = {font=\large, fill=white},
                label style = {font=\large},
                xlabel style={at={(ticklabel* cs:1)},anchor=north west},
                ylabel style={at={(ticklabel* cs:1)},anchor=south west}
                %xtick={1, 2, 3, 4,5,6,7,8,9,10},
                %ytick={0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6},
                ]
                \addplot[
                    line width=1pt,
                    color=blue
                    ]
                    {max(0, x)};
            \end{axis}
    \end{tikzpicture}}
        \caption{\label{fig:relu}Función $ReLU$.}
    \end{subfigure}
    \caption{\label{fig:funciones_activacion}Distintas funciones de activación.}
\end{figure}

Una importante desventaja del perceptrón sencillo es que se trata de un clasificador lineal, lo que implica que las instancias deben ser linealmente separables para que éstas sean clasificadas correctamente. Apilando capas de perceptrones una detrás de otra, se logra una no linearidad en la frontera de decisión y así solucionar dicho problema, dando lugar al perceptrón multicapa. De esta manera, se obtiene una abstracción más fiel de los casos de entrada.

La Figura \ref{fig:mlp} muestra un ejemplo de 22 perceptrones, apilados en capas.

% Imagen con una arquitectura de una MLP
\begin{figure}[!htb]
    \centering
    \input{archivos/images/mlp}
    \caption{\label{fig:mlp}Posible arquitectura de un perceptrón multicapa. Cuenta con 4 variables de entrada $x$, 3 capas ocultas $h$ con 8, 8 y 5 neuronas respectivamente, y finalmente la capa de salida con una sola neurona $y$.}
\end{figure}

\paragraph{\acrlong{cnn}}

Las redes neuronales convolucionales van aún más allá. Son una modificación del perceptron multicapa que se aplica en trabajos de visión por computador, como reconocimiento de objetos, clasificación de imágenes o incluso en procesamiento de lenguaje natural. A mediados de 1989, Y. LeCun\cite{lecun-89e} presentaba una manera de entrenar este tipo de redes, pero no fue hasta 2012 cuando la red \emph{AlexNet}\cite{alexnet} ganó el concurso ImageNet de clasificación de imágenes, potenciando este área de investigación y mostrando la importancia del uso de las \glspl{gpu} en el proceso de entrenamiento.

Estas redes estan formadas por varios bloques, como puede apreciarse en la Figura \ref{fig:conv_scheme}, descritos a continuación:

\begin{enumerate}
    \item El campo receptivo: se trataría de la entrada al modelo. Por ejemplo, una imagen de $m\times n$ píxeles.
    \item Capa convolucional: contiene los filtros (los parámetros que la red aprende) que son convolucionados a lo largo del campo receptivo. Esta operación de convolución viene definida en 2 dimensiones por:

    \begin{equation}
        x(m, n) \star h(m, n) = \sum\limits_{i=-\infty}^{+\infty} \sum\limits_{k=-\infty}^{+\infty} x(i, k)h(m-i, n-k)
    \end{equation}

    siendo $x$ la matriz a convolucionar y $h$ el filtro de convolución. En la Figura \ref{fig:convolucion} se muestra la operación con un filtro de $3 \times 3$.

    % Imagen con una convolución de 2 dimensiones
    \begin{figure}
        \centering
        \input{archivos/images/vis_convolucion}
        \caption{\label{fig:convolucion}Visualización de la operación de convolución en 2 dimensiones.}
    \end{figure}

    Una vez realizada la convolución, se obtiene otro conjunto de imágenes que representan características de bajo nivel de la imagen original. También se aplica una función de activación, como las mencionadas anteriormente.

    Se puede reducir la dimensionalidad de este conjunto, manipulando conjuntos de $m \times n$ píxeles para quedarse sólamente con uno, en la capa denominada \emph{pooling}. Esta selección puede extraer el mayor de ellos (\emph{max-pooling)}, la media (\emph{average-pooling)} o utilizar otras métricas como $l_2$-\emph{pooling}.


    Hay que destacar que este bloque convolucional puede apilarse uno detrás de otro generando varias capas en el modelo.

    \item Capa totalmente conectada: se introduce en último lugar y se trata de una pequeña red de perceptrones multicapa (\emph{fully connected layer}), similar a la Figura \ref{fig:mlp}. En este caso, la entrada a la red estaría formada por cada píxel resultante del bloque de convolución. Desaparecen las múltiples dimensiones que pueda haber y se aplanan, de manera que es aquí dónde sucede la clasificación.
\end{enumerate}

\begin{figure}
    \begin{annotate}{\includegraphics[scale=0.21]{conv.png}}{1}
        % \helpgrid
        \note{-6.9, 3.2}{Entrada}
        \draw[very thick,green] (-8.4, 3.5) rectangle (-5.4,-2.6);

        \note{1, 3.2}{Convolución}
        \note{-2.8, -2.3}{Capa 1: Conv + ReLU}
        \note{-0.2, -2.3}{Pooling}
        \note{3.4, -2.3}{Capa 2: Conv + ReLU}
        \note{5.3, -2.3}{Pooling}
        \draw[very thick,blue] (5.8, 3.5) rectangle (-5.35,-2.6);

        \note{7, 3.2}{Salida}
        \note{7.15, -2.3}{Capa FC}
        \draw[very thick,red] (5.85, 3.5) rectangle (8,-2.6);
        %\callout{-7,3}{Entrada}{3,2}
        %\arrow{-3,-2.4}{-4.5,-3}
    \end{annotate}
    \caption{\label{fig:conv_scheme}Taxonomía de una red convolucional.}
\end{figure}

\subsection{Entrenamiento de las redes neuronales}

\subsubsection{Cálculo del gradiente}

Publicada en 1986 por Rumelhart \cite{rumelhart1986learning}, la técnica de \emph{backpropagation} se utiliza para calcular únicamente el gradiente de los pesos $w$ mencionados anteriormente respecto a una función de coste, mientras que el aprendizaje de la red se produce con alguna técnica de optimización.

En general, el cálculo del gradiente $\nabla_{\pmb \theta}J(\pmb \theta)$ esta referido al gradiente de una función de coste respecto a los parámetros $\pmb \theta$. La idea es calcular estas derivadas propagando la información hacia delante: cuando una red toma como entrada un vector $\pmb X$, la información se propaga a través de la red produciendo una salida $\hat y$. Sin embargo, lo acertado que sea el valor producido respecto a la función de coste dependerá de los pesos sinápticos de la red.

\emph{Backpropagation} hace un uso eficiente de la regla de la cadena del cálculo para calcular las derivadas de funciones compuestas. Suponiendo dos funciones $y = g(x)$ y $z = f(y)$ y una variable real $x$, la regla de la cadena calcularía\cite{Goodfellow}:
\begin{equation}
    \frac{dz}{dx} = \frac{dz}{dy}\frac{dy}{dx}
\end{equation}

Si generalizamos la formulación anterior a un espacio $n$-dimensional y suponemos que $\pmb x \in \mathbb{R} ^{m}$, $\pmb y \in \mathbb{R} ^{n}$, la función $g$ transforma una variable de un espacio $\mathbb{R} ^{m}$ a un espacio $\mathbb{R} ^{n}$ y la función $f$ transforma de un espacio $\mathbb{R} ^{n}$ a $\mathbb{R}$. Si $\pmb y = g(\pmb x)$ y $\pmb z = f(\pmb y)$, entonces:
\begin{equation}
    \frac{\partial z}{\partial x_i} = \sum\limits_j \frac{\partial z}{\partial y_j} \frac{\partial y_j}{\partial x_i}
\end{equation}

O lo que es lo mismo, con notación vectorial:
\begin{equation}
    \nabla_{\pmb x} z = \Big( \frac{\partial \pmb y}{\partial \pmb x} \Big)^\top \nabla_{\pmb y} z
\end{equation}

donde $\frac{\partial \pmb y}{\partial \pmb x}$ es la matriz Jacobiana de $n \times m$ de $g$.

\subsubsection{Optimización}

Una vez se obtiene el gradiente (con \emph{backpropagation}, o con otras técnicas), es necesario adaptar los pesos de la red mediante métodos de optimización.

\paragraph{\gls{sgd}}

Se trata de un método iterativo de optimización en el que se recalculan los valores de los pesos $\pmb w$ para ajustar la red y hacerla más precisa según:

\begin{equation}
\pmb \theta = \pmb \theta - \eta \nabla J(\pmb \theta)
\end{equation}
donde $\eta$ sería la tasa de aprendizaje y los parámetros $\pmb \theta$ serían actualizados por una tasa de cambio $\eta \nabla J(\pmb \theta)$.

El problema de este método es que converge lentamente y puede quedarse atrapado en un mínimo local.

\paragraph{\emph{Momentum}}

El método del \emph{momentum} añade un elemento temporal al cálculo, \emph{recordando} las últimas actualizaciones de los pesos realizada:

\begin{equation}
 \pmb \theta^{(t)} = \pmb \theta^{(t-1)} - \eta \nabla J(\pmb \theta^{(t-1)}) + \alpha \sum\limits_{i=1}^{t-1}\eta \nabla J(\pmb \theta^{(t-1)})
\end{equation}
donde $\alpha$ sería una constante y el sumatorio representaría la suma de los cambios realizados hasta la última iteración. Este método tiene la ventaja de converger más rápidamente que \gls{sgd}. Sin embargo, si el \emph{momentum} $\alpha$ es demasiado grande, se puede perder el óptimo local durante la optimización. Existen también otros tipos de \emph{momentum}, como el \emph{momentum Nesterov}\cite{nesterov}.

\paragraph{\gls{rmsp}}

\gls{rmsp} es un optimizador en el que la tasa de aprendizaje se adapta a cada parámetro. La siguiente función $v$ calcularía una media móvil de las magnitudes de los gradientes para los parámetros $\pmb \theta$:

\begin{equation}
    v(\pmb \theta, t) = \gamma v(\pmb \theta^{(t-1)}, t-1) + (1 - \gamma)(\nabla J(\pmb \theta))^2
\end{equation}

donde $\gamma$ sería la constante de \emph{olvido}. De esta forma, la actualización se realizaría dividiendo la tasa de aprendizaje por la raíz cuadrada de la función $v$:

\begin{equation}
    \pmb \theta^{(t)} = \pmb \theta^{(t-1)} - \frac{\eta}{\sqrt{v(\pmb \theta, t-1)}} \nabla J(\pmb \theta^{(t-1)})
\end{equation}


\paragraph{\gls{adam}}

Propuesto por Kingma y Ba\cite{kingma2014adam}, se trata de una de las técnicas más usadas para la optimización de redes neuronales por su buena respuesta. La actualización se realizaría de la siguiente manera:

\begin{equation}
    \pmb \theta^{(t)} = \pmb \theta^{(t-1)} - \frac{\eta \cdot \pmb{\hat m}^{(t-1)}}{\sqrt{\pmb{\hat v}^{(t-1)} + \epsilon}}
\end{equation}

donde $\pmb{\hat m}_{(t)} = \frac{\pmb m^{(t)}}{1 - \beta_{1}^{(t)}}$ sería una corrección de la estimación del primer \emph{momentum} $\pmb m^{(t)} = (1 - \beta_1)\nabla J(\pmb \theta^{(t-1)}) + \beta_{1} \pmb m^{(t-1)}$, $\pmb{\hat v}_{(t)} = \frac{\pmb v^{(t)}}{1 - \beta_{2}^{(t)}}$ sería una corrección de la estimación del segundo \emph{momentum} $\pmb v^{(t)} = (1 - \beta_2)(\nabla J(\pmb \theta^{(t-1)})^2 + \beta_{2} \pmb v^{(t-1)}$. $\epsilon$ indicaría un escalar pequeño para evitar divisiones entre 0, y $\beta_1$ y $\beta_2$ indicarían los términos del primer y segundo \emph{momentum} respectivamente.

\medskip

El algoritmo utilizado para la optimización de las redes neuronales del proyecto ha sido \gls{adam}.

\subsection{Evaluación de los clasificadores supervisados}

La evaluación de los clasificadores se ha realizado mediante la técnica de la validación cruzada, con 10 hojas. Es una técnica ampliamente utilizada en \gls{ml} para evaluar los modelos de clasificación y estimar el comportamiento que pueda tener cierto modelo en un entorno nuevo. Consiste en particionar el conjunto de datos en $k$ subconjuntos, entrenar el modelo con $k-1$ subconjuntos y probarla en el restante durante, $k$ iteraciones, cada iteración con una partición distinta. Finalmente, se realiza la media aritmética de los resultados obtenidos.

Por lo general, esta técnica suele ser suficiente para decidir si un algoritmo realiza la tarea adecuadamente. Hay casos en los que la base de datos tiene un tamaño reducido y realizar particiones supondría reducir el conjunto de entrenamiento, por lo que se suele optar por la técnica de \gls{loo}, que consiste en utilizar el mismo número de instancias como particiones. Es decir, realizar tantas iteraciones como instancias haya.

Ya sea con $k$ particiones o con \gls{loo}, la computación de esta técnica es costosa ya que se entrena el mismo modelo $k$ veces y a veces puede resultar inviable, por ejemplo, para los modelos de \gls{dl} presentados anteriormente.


