import asyncio

from footballapi.data.sofifa import Player, SoFifa


async def single_page_demo(url):
    items = []
    async for item in Player.get_items(url=url):
        items.append(item)

    return items


def test_request_url():
    start_urls = [
        "https://sofifa.com/player/239053/federico-valverde/200012/?units=mks",
        "https://sofifa.com/player/197928/jonathan-bond/200012/?units=mks",
        "https://sofifa.com/player/248243/eduardo-camavinga/200012/?units=mks",
        "https://sofifa.com/player/241568/cristian-ferreira/200012/?units=mks",
        "https://sofifa.com/player/232730/daichi-kamada/200012/?units=mks",
        "https://sofifa.com/player/248243/eduardo-camavinga/200012/?units=mks",
        "https://sofifa.com/player/239085/erling-braut-haland/200012/?units=mks",
        "https://sofifa.com/player/231521/exequiel-palacios/200012/?units=mks",
        "https://sofifa.com/player/235735/ethan-ampadu/200012/?units=mks",
        "https://sofifa.com/player/253004/anssumane-fati/200012/?units=mks",
        "https://sofifa.com/player/239053/federico-valverde/200012/?units=mks",
        "https://sofifa.com/player/244260/daniel-olmo-carvajal/200012/?units=mks"
    ]

    for i in range(len(start_urls)):
        items = asyncio.get_event_loop().run_until_complete(single_page_demo(url=start_urls[i]))
        correct = [
            {'current_position': 'CM', 'height': 182, 'weight': 78, 'name': 'Federico Valverde', 'sofifa_id': 239053,
             'positions': ['LM', 'CDM'], 'generic_stats': [79, 87],
             'specific_stats': [59, 67, 59, 81, 47, 76, 68, 56, 83, 80, 72, 75, 69, 79, 67, 77, 54, 80, 77, 77, 77, 75,
                                68,
                                79, 48, 80, 70, 73, 59, 6, 10, 6, 15, 8]},
            {'current_position': 'GK', 'height': 193, 'weight': 84, 'name': 'Jonathan Bond', 'sofifa_id': 197928,
             'positions': [], 'generic_stats': [64, 69],
             'specific_stats': [11, 13, 13, 33, 14, 11, 14, 13, 45, 19, 54, 42, 56, 61, 55, 46, 71, 38, 64, 15, 37, 23,
                                13,
                                43, 17, 67, 14, 14, 17, 65, 61, 61, 62, 64]},
            {'current_position': 'CM', 'height': 182, 'weight': 68, 'name': 'Eduardo Camavinga', 'sofifa_id': 248243,
             'positions': [], 'generic_stats': [70, 90],
             'specific_stats': [59, 52, 55, 72, 51, 73, 64, 48, 71, 73, 73, 70, 74, 68, 70, 70, 62, 65, 51, 56, 74, 67,
                                63,
                                68, 52, 74, 66, 69, 68, 12, 6, 8, 12, 12]},
            {'current_position': 'CM', 'height': 175, 'weight': 69, 'name': 'Cristian Ferreira', 'sofifa_id': 241568,
             'positions': ['CAM'], 'generic_stats': [71, 87],
             'specific_stats': [71, 66, 56, 75, 72, 73, 75, 76, 72, 75, 74, 76, 77, 70, 77, 75, 61, 70, 62, 75, 54, 52,
                                63,
                                70, 66, 76, 53, 44, 48, 10, 11, 5, 8, 9]},
            {'current_position': 'CAM', 'height': 180, 'weight': 73, 'name': 'Age ', 'sofifa_id': 232730,
             'positions': ['CF', 'ST'], 'generic_stats': [74, 79],
             'specific_stats': [71, 68, 53, 73, 57, 79, 70, 68, 65, 79, 76, 74, 77, 70, 72, 71, 68, 70, 59, 62, 55, 28,
                                74,
                                72, 71, 80, 30, 42, 34, 6, 10, 11, 6, 11]},
            {'current_position': 'CM', 'height': 182, 'weight': 68, 'name': 'Eduardo Camavinga', 'sofifa_id': 248243,
             'positions': [], 'generic_stats': [70, 90],
             'specific_stats': [59, 52, 55, 72, 51, 73, 64, 48, 71, 73, 73, 70, 74, 68, 70, 70, 62, 65, 51, 56, 74, 67,
                                63,
                                68, 52, 74, 66, 69, 68, 12, 6, 8, 12, 12]},
            {'current_position': 'ST', 'height': 194, 'weight': 87, 'name': 'Erling Braut ', 'sofifa_id': 239085,
             'positions': [], 'generic_stats': [77, 88],
             'specific_stats': [46, 81, 67, 69, 72, 72, 69, 62, 49, 76, 79, 89, 76, 75, 65, 78, 70, 75, 85, 67, 75, 35,
                                77,
                                62, 80, 80, 38, 31, 15, 7, 14, 13, 11, 7]},
            {'current_position': 'CM', 'height': 177, 'weight': 72, 'name': 'Exequiel Palacios', 'sofifa_id': 231521,
             'positions': ['RM', 'CAM'], 'generic_stats': [78, 88],
             'specific_stats': [69, 71, 63, 79, 65, 77, 68, 47, 75, 79, 75, 77, 79, 75, 80, 81, 73, 87, 68, 78, 75, 68,
                                73,
                                77, 63, 82, 66, 71, 69, 10, 5, 8, 9, 6]},
            {'current_position': 'CB', 'height': 183, 'weight': 78, 'name': 'Ethan Ampadu', 'sofifa_id': 235735,
             'positions': ['CDM'], 'generic_stats': [67, 86],
             'specific_stats': [27, 23, 63, 70, 25, 57, 34, 27, 64, 62, 62, 60, 56, 64, 68, 52, 74, 60, 67, 22, 67, 64,
                                29,
                                59, 33, 64, 66, 67, 65, 13, 12, 9, 8, 12]},
            {'current_position': 'LW', 'height': 178, 'weight': 55, 'name': 'Anssumane Fati', 'sofifa_id': 253004,
             'positions': ['RW'], 'generic_stats': [71, 90],
             'specific_stats': [68, 72, 58, 71, 59, 71, 53, 45, 69, 70, 89, 85, 81, 65, 79, 65, 69, 60, 41, 70, 42, 19,
                                61,
                                66, 74, 66, 23, 26, 28, 6, 9, 8, 10, 7]},
            {'current_position': 'CM', 'height': 182, 'weight': 78, 'name': 'Federico Valverde', 'sofifa_id': 239053,
             'positions': ['LM', 'CDM'], 'generic_stats': [79, 87],
             'specific_stats': [59, 67, 59, 81, 47, 76, 68, 56, 83, 80, 72, 75, 69, 79, 67, 77, 54, 80, 77, 77, 77, 75,
                                68,
                                79, 48, 80, 70, 73, 59, 6, 10, 6, 15, 8]},
            {'current_position': 'CAM', 'height': 179, 'weight': 72, 'name': 'Daniel Olmo Carvajal',
             'sofifa_id': 244260,
             'positions': ['LM', 'RM'], 'generic_stats': [80, 88],
             'specific_stats': [77, 75, 43, 76, 76, 83, 72, 75, 77, 83, 78, 82, 80, 75, 89, 79, 55, 79, 54, 76, 61, 57,
                                80,
                                77, 66, 82, 50, 50, 44, 15, 15, 13, 12, 11]}
        ]

        for item in items:
            assert item.sofifa_id == correct[i]['sofifa_id']
            assert item.name == correct[i]['name']
            assert item.height == correct[i]['height']
            assert item.weight == correct[i]['weight']
            assert item.current_position == correct[i]['current_position']
            assert item.positions == correct[i]['positions']
            assert item.specific_stats == correct[i]['specific_stats']
            assert item.generic_stats == correct[i]['generic_stats']
            assert item.date == "Nov 22, 2019"


def test_update_history_items():
    sf = SoFifa()
    sf.update_history_items(save=False)
    urls = list(filter(lambda x: int(x[0]) <= 200012, sf.history_urls['roasters']))
    correct_ones = [
        (
            "200012",
            "Nov 22, 2019"
        ),
        (
            "200011",
            "Nov 19, 2019"
        ),
        (
            "200010",
            "Nov 13, 2019"
        ),
        (
            "200009",
            "Nov 6, 2019"
        ),
        (
            "200008",
            "Oct 29, 2019"
        ),
        (
            "200007",
            "Oct 16, 2019"
        ),
        (
            "200006",
            "Oct 4, 2019"
        ),
        (
            "200005",
            "Oct 1, 2019"
        ),
        (
            "200004",
            "Sep 27, 2019"
        ),
        (
            "200003",
            "Sep 26, 2019"
        ),
        (
            "200002",
            "Sep 19, 2019"
        ),
        (
            "190075",
            "Sep 11, 2019"
        ),
        (
            "190074",
            "Aug 29, 2019"
        ),
        (
            "200001",
            "Aug 20, 2019"
        ),
        (
            "190073",
            "Aug 15, 2019"
        ),
        (
            "190072",
            "Jul 18, 2019"
        ),
        (
            "190071",
            "Jul 11, 2019"
        ),
        (
            "190070",
            "Jul 4, 2019"
        ),
        (
            "190069",
            "Jun 27, 2019"
        ),
        (
            "190068",
            "Jun 24, 2019"
        ),
        (
            "190067",
            "Jun 13, 2019"
        ),
        (
            "190066",
            "Jun 6, 2019"
        ),
        (
            "190065",
            "Jun 3, 2019"
        ),
        (
            "190064",
            "May 30, 2019"
        ),
        (
            "190063",
            "May 27, 2019"
        ),
        (
            "190062",
            "May 23, 2019"
        ),
        (
            "190061",
            "May 9, 2019"
        ),
        (
            "190060",
            "May 7, 2019"
        ),
        (
            "190059",
            "May 2, 2019"
        ),
        (
            "190058",
            "Apr 29, 2019"
        ),
        (
            "190057",
            "Apr 25, 2019"
        ),
        (
            "190056",
            "Apr 23, 2019"
        ),
        (
            "190055",
            "Apr 18, 2019"
        ),
        (
            "190054",
            "Apr 16, 2019"
        ),
        (
            "190053",
            "Apr 15, 2019"
        ),
        (
            "190052",
            "Apr 11, 2019"
        ),
        (
            "190051",
            "Apr 8, 2019"
        ),
        (
            "190050",
            "Apr 4, 2019"
        ),
        (
            "190049",
            "Apr 1, 2019"
        ),
        (
            "190048",
            "Mar 28, 2019"
        ),
        (
            "190047",
            "Mar 25, 2019"
        ),
        (
            "190046",
            "Mar 21, 2019"
        ),
        (
            "190045",
            "Mar 20, 2019"
        ),
        (
            "190044",
            "Mar 18, 2019"
        ),
        (
            "190043",
            "Mar 14, 2019"
        ),
        (
            "190042",
            "Mar 4, 2019"
        ),
        (
            "190041",
            "Feb 28, 2019"
        ),
        (
            "190040",
            "Feb 22, 2019"
        ),
        (
            "190039",
            "Feb 21, 2019"
        ),
        (
            "190038",
            "Feb 14, 2019"
        ),
        (
            "190037",
            "Feb 11, 2019"
        ),
        (
            "190036",
            "Feb 7, 2019"
        ),
        (
            "190035",
            "Feb 4, 2019"
        ),
        (
            "190034",
            "Jan 31, 2019"
        ),
        (
            "190033",
            "Jan 28, 2019"
        ),
        (
            "190032",
            "Jan 24, 2019"
        ),
        (
            "190031",
            "Jan 21, 2019"
        ),
        (
            "190030",
            "Jan 17, 2019"
        ),
        (
            "190029",
            "Jan 14, 2019"
        ),
        (
            "190028",
            "Jan 10, 2019"
        ),
        (
            "190027",
            "Jan 7, 2019"
        ),
        (
            "190026",
            "Jan 3, 2019"
        ),
        (
            "190025",
            "Dec 27, 2018"
        ),
        (
            "190024",
            "Dec 20, 2018"
        ),
        (
            "190023",
            "Dec 17, 2018"
        ),
        (
            "190022",
            "Dec 13, 2018"
        ),
        (
            "190021",
            "Dec 3, 2018"
        ),
        (
            "190020",
            "Nov 26, 2018"
        ),
        (
            "190019",
            "Nov 15, 2018"
        ),
        (
            "190018",
            "Nov 13, 2018"
        ),
        (
            "190017",
            "Nov 5, 2018"
        ),
        (
            "190016",
            "Nov 1, 2018"
        ),
        (
            "190015",
            "Oct 29, 2018"
        ),
        (
            "190014",
            "Oct 25, 2018"
        ),
        (
            "190013",
            "Oct 23, 2018"
        ),
        (
            "190012",
            "Oct 22, 2018"
        ),
        (
            "190011",
            "Oct 18, 2018"
        ),
        (
            "190010",
            "Oct 15, 2018"
        ),
        (
            "190009",
            "Oct 11, 2018"
        ),
        (
            "190008",
            "Oct 9, 2018"
        ),
        (
            "190007",
            "Oct 4, 2018"
        ),
        (
            "190006",
            "Oct 1, 2018"
        ),
        (
            "190005",
            "Sep 27, 2018"
        ),
        (
            "190004",
            "Sep 24, 2018"
        ),
        (
            "190003",
            "Sep 20, 2018"
        ),
        (
            "180084",
            "Sep 12, 2018"
        ),
        (
            "180083",
            "Sep 6, 2018"
        ),
        (
            "180082",
            "Aug 30, 2018"
        ),
        (
            "180081",
            "Aug 23, 2018"
        ),
        (
            "190002",
            "Aug 21, 2018"
        ),
        (
            "180080",
            "Aug 3, 2018"
        ),
        (
            "180079",
            "Jul 26, 2018"
        ),
        (
            "180078",
            "Jul 19, 2018"
        ),
        (
            "190001",
            "Jul 19, 2018"
        ),
        (
            "180077",
            "Jun 14, 2018"
        ),
        (
            "180076",
            "Jun 11, 2018"
        ),
        (
            "180075",
            "Jun 7, 2018"
        ),
        (
            "180074",
            "Jun 4, 2018"
        ),
        (
            "180073",
            "May 31, 2018"
        ),
        (
            "180072",
            "May 28, 2018"
        ),
        (
            "180071",
            "May 24, 2018"
        ),
        (
            "180070",
            "May 22, 2018"
        ),
        (
            "180069",
            "May 17, 2018"
        ),
        (
            "180068",
            "May 14, 2018"
        ),
        (
            "180067",
            "May 10, 2018"
        ),
        (
            "180066",
            "May 8, 2018"
        ),
        (
            "180065",
            "May 3, 2018"
        ),
        (
            "180064",
            "Apr 30, 2018"
        ),
        (
            "180063",
            "Apr 26, 2018"
        ),
        (
            "180062",
            "Apr 23, 2018"
        ),
        (
            "180061",
            "Apr 19, 2018"
        ),
        (
            "180060",
            "Apr 16, 2018"
        ),
        (
            "180059",
            "Apr 12, 2018"
        ),
        (
            "180058",
            "Apr 9, 2018"
        ),
        (
            "180057",
            "Apr 5, 2018"
        ),
        (
            "180056",
            "Apr 3, 2018"
        ),
        (
            "180055",
            "Mar 29, 2018"
        ),
        (
            "180054",
            "Mar 26, 2018"
        ),
        (
            "180053",
            "Mar 22, 2018"
        ),
        (
            "180052",
            "Mar 19, 2018"
        ),
        (
            "180051",
            "Mar 15, 2018"
        ),
        (
            "180050",
            "Mar 12, 2018"
        ),
        (
            "180049",
            "Mar 8, 2018"
        ),
        (
            "180048",
            "Mar 5, 2018"
        ),
        (
            "180047",
            "Mar 1, 2018"
        ),
        (
            "180046",
            "Feb 26, 2018"
        ),
        (
            "180045",
            "Feb 22, 2018"
        ),
        (
            "180044",
            "Feb 19, 2018"
        ),
        (
            "180043",
            "Feb 15, 2018"
        ),
        (
            "180042",
            "Feb 12, 2018"
        ),
        (
            "180041",
            "Feb 8, 2018"
        ),
        (
            "180040",
            "Feb 5, 2018"
        ),
        (
            "180039",
            "Feb 1, 2018"
        ),
        (
            "180038",
            "Jan 29, 2018"
        ),
        (
            "180037",
            "Jan 25, 2018"
        ),
        (
            "180036",
            "Jan 22, 2018"
        ),
        (
            "180035",
            "Jan 18, 2018"
        ),
        (
            "180034",
            "Jan 15, 2018"
        ),
        (
            "180033",
            "Jan 11, 2018"
        ),
        (
            "180032",
            "Jan 8, 2018"
        ),
        (
            "180031",
            "Jan 4, 2018"
        ),
        (
            "180030",
            "Jan 2, 2018"
        ),
        (
            "180029",
            "Dec 28, 2017"
        ),
        (
            "180028",
            "Dec 21, 2017"
        ),
        (
            "180027",
            "Dec 18, 2017"
        ),
        (
            "180026",
            "Dec 14, 2017"
        ),
        (
            "180025",
            "Dec 11, 2017"
        ),
        (
            "180024",
            "Dec 7, 2017"
        ),
        (
            "180023",
            "Dec 4, 2017"
        ),
        (
            "180022",
            "Nov 30, 2017"
        ),
        (
            "180021",
            "Nov 27, 2017"
        ),
        (
            "180020",
            "Nov 23, 2017"
        ),
        (
            "180019",
            "Nov 20, 2017"
        ),
        (
            "180018",
            "Nov 16, 2017"
        ),
        (
            "180017",
            "Nov 13, 2017"
        ),
        (
            "180016",
            "Nov 6, 2017"
        ),
        (
            "180015",
            "Nov 2, 2017"
        ),
        (
            "180014",
            "Oct 30, 2017"
        ),
        (
            "180013",
            "Oct 26, 2017"
        ),
        (
            "180012",
            "Oct 23, 2017"
        ),
        (
            "180011",
            "Oct 19, 2017"
        ),
        (
            "180010",
            "Oct 16, 2017"
        ),
        (
            "180009",
            "Oct 12, 2017"
        ),
        (
            "180008",
            "Oct 9, 2017"
        ),
        (
            "180007",
            "Oct 5, 2017"
        ),
        (
            "180006",
            "Oct 2, 2017"
        ),
        (
            "180005",
            "Sep 28, 2017"
        ),
        (
            "180004",
            "Sep 25, 2017"
        ),
        (
            "180003",
            "Sep 21, 2017"
        ),
        (
            "180002",
            "Sep 18, 2017"
        ),
        (
            "180001",
            "Aug 28, 2017"
        )
    ]

    assert urls == correct_ones


if __name__ == '__main__':
    test_request_url()
    test_update_history_items()
