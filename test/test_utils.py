from unittest import TestCase

from footballapi.utils import comp_info
from footballapi.utils import parse_seasons, temporadas_disponibles


class Test(TestCase):
    def test_to_ftp(self):
        self.fail()

    def test_shin_gasparyan(self):
        self.fail()

    def test_parse_seasons(self):
        # Fechas correctas
        self.assertEqual(parse_seasons("17"), ["17-18"])
        self.assertEqual(parse_seasons("10, 15"), temporadas_disponibles)
        self.assertEqual(parse_seasons("10, 15", first=True), '17-18')
        self.assertEqual(parse_seasons("1998"), temporadas_disponibles)
        self.assertEqual(parse_seasons("2019, 2020"), ['19-20'] + temporadas_disponibles[:-1])
        self.assertEqual(parse_seasons("2019, 2020", first=True), "19-20")

        # Fechas incorrectas
        self.assertRaises(ValueError, parse_seasons, [2019, 2020])
        self.assertRaises(ValueError, parse_seasons, 15)
        self.assertRaises(ValueError, parse_seasons, 1998)
        self.assertRaises(ValueError, parse_seasons, True)
        self.assertRaises(ValueError, parse_seasons, None)

    def test_es_portero(self):
        self.fail()

    def test_comp_info(self):
        self.assertEqual(comp_info(2013)['name'], "Série A")
        self.assertEqual(comp_info(2016)['name'], "Championship")
        self.assertEqual(comp_info(2021)['name'], "Premier League")
        self.assertEqual(comp_info(2001)['name'], "UEFA Champions League")
        self.assertEqual(comp_info(2018)['name'], "European Championship")
        self.assertEqual(comp_info(2015)['name'], "Ligue 1")
        self.assertEqual(comp_info(2002)['name'], "Bundesliga")
        self.assertEqual(comp_info(2019)['name'], "Serie A")
        self.assertEqual(comp_info(2003)['name'], "Eredivisie")
        self.assertEqual(comp_info(2017)['name'], "Primeira Liga")
        self.assertEqual(comp_info(2077)['name'], "Segunda División")
        self.assertEqual(comp_info(2078)['name'], "Supercopa de España")
        self.assertEqual(comp_info(2014)['name'], "Primera Division")
        self.assertEqual(comp_info(2079)['name'], "Copa del Rey")
        self.assertEqual(comp_info(2000)['name'], "FIFA World Cup")

        self.assertIsNone(comp_info(1900))
