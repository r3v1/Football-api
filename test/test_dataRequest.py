from unittest import TestCase

from footballapi.data.DataRequest import DataRequest


class TestDataRequest(TestCase):
    def setUp(self):
        self.dr = DataRequest()

    def test_fdid2sofifaid(self):
        self.assertEqual(self.dr.fdid2sofifaid(9861, "Calvin Verdonk", allow_input=False), "227794")
        self.assertEqual(self.dr.fdid2sofifaid(98870, "Jonathan Panzo", allow_input=False), "252001")
        self.assertEqual(self.dr.fdid2sofifaid(8937, "Mathieu Peybernes", allow_input=False), "195313")
        self.assertEqual(self.dr.fdid2sofifaid(8688, "Ciprian Tătăruşanu", allow_input=False), "201325")
        self.assertEqual(self.dr.fdid2sofifaid(8439, "Pierre Lees Melou", allow_input=False), "230020")

    def test_fecha_mas_proxima_a(self):
        # Fechas exactas
        self.assertEqual(self.dr.fecha_mas_proxima_a("2019-04-18"), "2019-04-18")

        # Fechas aproximadas
        self.assertEqual(self.dr.fecha_mas_proxima_a("2019-04-19"), "2019-04-18")
        self.assertEqual(self.dr.fecha_mas_proxima_a("2019-04-20"), "2019-04-18")
        self.assertEqual(self.dr.fecha_mas_proxima_a("2019-04-21"), "2019-04-23")
        self.assertEqual(self.dr.fecha_mas_proxima_a("2019-04-22"), "2019-04-23")

        self.assertEqual(self.dr.fecha_mas_proxima_a("2016-05-18"), "2017-08-28")

        # Fechas incorrectas
        self.assertRaises(ValueError, self.dr.fecha_mas_proxima_a, "15-05-2019")
        self.assertRaises(ValueError, self.dr.fecha_mas_proxima_a, True)
        self.assertRaises(ValueError, self.dr.fecha_mas_proxima_a, "2019")
        self.assertRaises(ValueError, self.dr.fecha_mas_proxima_a, "aaa")
        self.assertRaises(ValueError, self.dr.fecha_mas_proxima_a, None)
