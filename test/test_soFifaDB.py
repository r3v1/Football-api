from unittest import TestCase

from footballapi.data.sofifadb import SoFifaDB


class TestSoFifaDB(TestCase):
    def setUp(self):
        self.sofifadb = SoFifaDB()

    def test_insertar_ids(self):
        self.fail()

    def test_insertar_jugador(self):
        self.fail()

    def test_contains_player_stats(self):
        self.fail()

    def test__dimensiones(self):
        # Dimensiones correctas
        self.assertEqual(self.sofifadb._dimensiones([]), 1)
        self.assertEqual(self.sofifadb._dimensiones([[], []]), 2)
        self.assertEqual(self.sofifadb._dimensiones([[]]), 2)
        self.assertEqual(self.sofifadb._dimensiones(()), 1)
        self.assertEqual(self.sofifadb._dimensiones([("Hola")]), 2)
        self.assertEqual(self.sofifadb._dimensiones((())), 2)
        self.assertEqual(self.sofifadb._dimensiones(((1, 2, 3))), 2)
        self.assertEqual(self.sofifadb._dimensiones(([1], [2])), 2)
        self.assertEqual(self.sofifadb._dimensiones(
            [
                [
                    [
                        [

                        ],
                        [

                        ]
                    ],
                    [

                    ],
                    [

                    ]
                ]
            ]), 4)

        # Dimensiones incorrectas
        self.assertRaises(TypeError, self.sofifadb._dimensiones, "ASDF")
        self.assertRaises(TypeError, self.sofifadb._dimensiones, True)
        self.assertRaises(TypeError, self.sofifadb._dimensiones, False)
        self.assertRaises(TypeError, self.sofifadb._dimensiones, None)
        self.assertRaises(TypeError, self.sofifadb._dimensiones, 1)

    def test_date2roaster(self):
        # Fechas correctas
        self.assertEqual(self.sofifadb.date2roaster("Sep 28, 2017"), "180005")
        self.assertEqual(self.sofifadb.date2roaster("Sep 25, 2017"), "180004")
        self.assertEqual(self.sofifadb.date2roaster("Sep 21, 2017"), "180003")

        # Fechas aproximadas
        self.assertEqual(self.sofifadb.date2roaster("Sep 22, 2017"), "180003")
        self.assertEqual(self.sofifadb.date2roaster("Sep 23, 2017"), "180004")
        self.assertEqual(self.sofifadb.date2roaster("Sep 26, 2017"), "180004")
        self.assertEqual(self.sofifadb.date2roaster("Oct 15, 2000"), "180001")

        # Fechas incorrectas
        self.assertRaises(ValueError, self.sofifadb.date2roaster, "Octt 15, 2000")
        self.assertRaises(ValueError, self.sofifadb.date2roaster, "oct 15 2000")
        self.assertRaises(ValueError, self.sofifadb.date2roaster, "Set 15, 2000")
        self.assertRaises(ValueError, self.sofifadb.date2roaster, "aaaaa")
        self.assertRaises(ValueError, self.sofifadb.date2roaster, True)
        self.assertRaises(ValueError, self.sofifadb.date2roaster, False)
        self.assertRaises(ValueError, self.sofifadb.date2roaster, None)
