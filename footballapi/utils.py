#!/usr/bin/env python

"""
Funciones útiles
"""
import configparser
import os
import re
from ftplib import FTP
from statistics import mean

import numpy as np

from footballapi.colors import bcolors

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.7"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"

temporadas_disponibles = ['17-18', '18-18', '18-19', '19-20']

# Archivo de configuración
config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
config.read(os.path.join(path, 'data', 'settings.cfg'))


def to_ftp(local, remote):
    """
    Sube el fichero al servidor FTP

    Parameters
    ----------
    local: str
        Source file name
    remote: str
        Remote file name

    Examples
    --------
    >>> to_ftp({"predicts":[],"matches":3,"num_res":2}, 'summary_2.json', 'summary_2.json')
    """

    print(
        bcolors.OKBLUE + "[*]" + bcolors.ENDC + f" Subiendo fichero a {config['FTP']['user']}@{config['FTP']['host']}")

    try:
        # Start connection
        with FTP(config['FTP']['host']) as ftp:
            # Login
            ftp.login(config['FTP']['user'], os.environ['FTP_SERVER_PASSW'])

            # Change dir
            listing = []
            dir_regex = re.compile("[0-9]{2}:[0-9]{2} '?([a-zA-Z_0-9]+)'?$")
            ftp.retrlines("LIST", listing.append)

            dirs = list(map(lambda x: dir_regex.search(x).group(1), filter(lambda x: dir_regex.search(x), listing)))
            if 'ftp' in dirs:
                # Si la carpeta 'ftp' es a la vista, entrar
                ftp.cwd('ftp')
            else:
                # Si no esta a la vista, no hacer nada
                pass

            # Write files to FTP server
            ftp.storbinary(f'STOR {remote}', open(local, 'rb'))

        print(bcolors.OKGREEN + "[+]" + bcolors.ENDC + f" Fichero subido")
    except Exception as e:
        print(bcolors.FAIL + "[!]" + bcolors.ENDC + f" No ha podido subirse el fichero: {e}")
        raise e
    finally:
        os.remove(local)


def shin_gasparyan(equipo: list):
    """
    Dado un equipo, calcula la agregación del equipo según la agregación
    realizada por Jongho Shin y Robert Gasparyan.

    Parameters
    ----------
    equipo: list
        Lista con los atributos de cada jugador del equipo

    Returns
    -------
    dict
        Diccionario con la agregación final

    References
    ----------
    http://cs229.stanford.edu/proj2014/Jongho%20Shin,%20Robert%20Gasparyan
    """
    atributos = ['OFENSIVA', 'TÉCNICA', 'MOVIMIENTO', 'POTENCIA', 'MENTALIDAD', 'DEFENSA']
    agregado = np.zeros((7,), dtype=int)
    np_equipo = [np.array([e[k] for e in equipo[1:]]) for k in atributos]
    filtrados = np.array([np_equipo[i].mean(axis=1) for i in range(len(np_equipo))])
    ordenados = (-filtrados).argsort() + 1

    for i, k in enumerate(atributos):
        if k in ('OFENSIVA', 'DEFENSA'):
            # Coger sólo los 4 mejores jugadores
            agregado[i] = sum([sum(equipo[i][k]) for i in ordenados[i, :4]])
        elif k in ('TÉCNICA', 'MOVIMIENTO', 'POTENCIA', 'MENTALIDAD'):
            # Coger sólo los 5 mejores jugadores
            agregado[i] = sum([sum(equipo[i][k]) for i in ordenados[i, :5]])

    # Portero
    agregado[6] = sum(equipo[0]['PORTERO'])
    agregado = dict(zip(atributos + ['PORTERO'], map(int, agregado)))

    return agregado


def parse_seasons(seasons, first=False):
    """
    Transforma el nombre de temporada al formato YY-YY.

    Parameters
    ----------
    seasons: list, str
        Temporadas
    first: bool
        Devolver una única fecha o no

    Notes
    -----
    Sólo son válidas el rango entre 17-20

    Examples
    --------
    >>> parse_seasons('17,18')
    ['17-18', '18-19']
    >>> parse_seasons('2017,2018')
    ['17-18', '18-19']
    >>> parse_seasons('17')
    ['17-18']
    >>> parse_seasons('2017')
    ['17-18']
    """
    if type(seasons) != str:
        raise ValueError

    s_list = []
    for s in seasons.strip().split(','):
        if re.match(r'^[0-9]{2}$', s):
            # Formato yy
            s_list.append(f"{s}-{int(s) + 1}")

        elif re.match(r'^[0-9]{2}-[0-9]{2}$', s):
            # Formato yy-yy
            s_list.append(s)

        elif re.match(r'^20[0-9]{2}$', s):
            # Format yyyy
            year = s[2:]
            s_list.append(f"{year}-{int(year) + 1}")

        else:
            # Sino, exportar todas las temporadas disponibles
            s_list += temporadas_disponibles
            break

    # Eliminar las fechas no válidas y las repetidas
    # https://stackoverflow.com/a/7961390/9079492
    s_list = list(dict.fromkeys([s for s in s_list if s in temporadas_disponibles]))

    if first:
        return s_list[0]
    return s_list


def es_portero(atributos: dict, thres=0.9):
    """
    Comprueba si ciertos atributos corresponden a los de un portero
    o no, para evitar añadir más de un portero por equipo.

    Parameters
    ----------
    atributos: dict
        Atributos del jugador
    thres: float
        Umbral de decisión

    Returns
    -------
    bool
        Decide si es un portero o no
    """
    meadia_portero = mean(atributos['PORTERO'])
    demas_attr = atributos['OFENSIVA'] + atributos['TÉCNICA'] + atributos['MOVIMIENTO'] + atributos['POTENCIA'] + \
                 atributos['MENTALIDAD'] + atributos['DEFENSA']
    media_demas = mean(demas_attr)

    if thres * meadia_portero >= media_demas:
        return True

    return False


def comp_info(competition_id):
    """
    Busca en el archivo de competiciones información de
    la competición

    Parameters
    ----------
    competition_id: int
        Id de la competición

    Returns
    -------
    dict
    """
    competition_id = int(competition_id)

    # TODO: Mejorarlo
    competitions = [
        {'id': 2013, 'area': {'id': 2032, 'name': 'Brazil', 'countryCode': 'BRA', 'ensignUrl': None}, 'name': 'Série A',
         'code': 'BSA', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 460, 'startDate': '2019-04-28', 'endDate': '2019-12-08', 'currentMatchday': 38,
                           'winner': None}, 'numberOfAvailableSeasons': 3, 'lastUpdated': '2019-12-09T22:25:01Z'},
        {'id': 2016, 'area': {'id': 2072, 'name': 'England', 'countryCode': 'ENG',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/en/a/ae/Flag_of_the_United_Kingdom.svg'},
         'name': 'Championship', 'code': 'ELC', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 503, 'startDate': '2019-08-02', 'endDate': '2020-05-27', 'currentMatchday': 36,
                           'winner': None}, 'numberOfAvailableSeasons': 3, 'lastUpdated': '2020-02-29T12:31:09Z'},
        {'id': 2021, 'area': {'id': 2072, 'name': 'England', 'countryCode': 'ENG',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/en/a/ae/Flag_of_the_United_Kingdom.svg'},
         'name': 'Premier League', 'code': 'PL', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 468, 'startDate': '2019-08-09', 'endDate': '2020-05-17', 'currentMatchday': 28,
                           'winner': None}, 'numberOfAvailableSeasons': 27, 'lastUpdated': '2020-02-29T12:31:09Z'},
        {'id': 2001, 'area': {'id': 2077, 'name': 'Europe', 'countryCode': 'EUR', 'ensignUrl': None},
         'name': 'UEFA Champions League', 'code': 'CL',
         'emblemUrl': 'https://upload.wikimedia.org/wikipedia/en/b/bf/UEFA_Champions_League_logo_2.svg',
         'plan': 'TIER_ONE',
         'currentSeason': {'id': 495, 'startDate': '2019-06-25', 'endDate': '2020-05-30', 'currentMatchday': 6,
                           'winner': None}, 'numberOfAvailableSeasons': 19, 'lastUpdated': '2020-02-27T15:55:00Z'},
        {'id': 2018, 'area': {'id': 2077, 'name': 'Europe', 'countryCode': 'EUR', 'ensignUrl': None},
         'name': 'European Championship', 'code': 'EC', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 507, 'startDate': '2020-06-12', 'endDate': '2020-07-12', 'currentMatchday': 1,
                           'winner': None}, 'numberOfAvailableSeasons': 2, 'lastUpdated': '2018-08-23T12:16:01Z'},
        {'id': 2015, 'area': {'id': 2081, 'name': 'France', 'countryCode': 'FRA',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/en/c/c3/Flag_of_France.svg'},
         'name': 'Ligue 1', 'code': 'FL1', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 499, 'startDate': '2019-08-09', 'endDate': '2020-05-31', 'currentMatchday': 27,
                           'winner': None}, 'numberOfAvailableSeasons': 9, 'lastUpdated': '2020-02-29T05:00:02Z'},
        {'id': 2002, 'area': {'id': 2088, 'name': 'Germany', 'countryCode': 'DEU',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/commons/b/ba/Flag_of_Germany.svg'},
         'name': 'Bundesliga', 'code': 'BL1', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 474, 'startDate': '2019-08-16', 'endDate': '2020-05-16', 'currentMatchday': 24,
                           'winner': None}, 'numberOfAvailableSeasons': 24, 'lastUpdated': '2020-02-29T04:50:03Z'},
        {'id': 2019, 'area': {'id': 2114, 'name': 'Italy', 'countryCode': 'ITA',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/en/0/03/Flag_of_Italy.svg'},
         'name': 'Serie A', 'code': 'SA', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 530, 'startDate': '2019-08-24', 'endDate': '2020-05-24', 'currentMatchday': 26,
                           'winner': None}, 'numberOfAvailableSeasons': 15, 'lastUpdated': '2020-02-29T12:31:15Z'},
        {'id': 2003, 'area': {'id': 2163, 'name': 'Netherlands', 'countryCode': 'NLD',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/commons/2/20/Flag_of_the_Netherlands.svg'},
         'name': 'Eredivisie', 'code': 'DED', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 481, 'startDate': '2019-08-09', 'endDate': '2020-05-25', 'currentMatchday': 25,
                           'winner': None}, 'numberOfAvailableSeasons': 10, 'lastUpdated': '2020-02-29T04:20:03Z'},
        {'id': 2017, 'area': {'id': 2187, 'name': 'Portugal', 'countryCode': 'PRT',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/commons/5/5c/Flag_of_Portugal.svg'},
         'name': 'Primeira Liga', 'code': 'PPL', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 484, 'startDate': '2019-08-10', 'endDate': '2020-05-19', 'currentMatchday': 23,
                           'winner': None}, 'numberOfAvailableSeasons': 9, 'lastUpdated': '2020-02-29T05:15:04Z'},
        {'id': 2077, 'area': {'id': 2224, 'name': 'Spain', 'countryCode': 'ESP',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/en/9/9a/Flag_of_Spain.svg'},
         'name': 'Segunda División', 'code': 'SD', 'emblemUrl': None, 'plan': 'TIER_TWO',
         'currentSeason': {'id': 526, 'startDate': '2019-08-17', 'endDate': '2020-06-23', 'currentMatchday': 30,
                           'winner': None}, 'numberOfAvailableSeasons': 8, 'lastUpdated': '2020-02-29T05:10:05Z'},
        {'id': 2078, 'area': {'id': 2224, 'name': 'Spain', 'countryCode': 'ESP',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/en/9/9a/Flag_of_Spain.svg'},
         'name': 'Supercopa de España', 'code': None, 'emblemUrl': None, 'plan': 'TIER_FOUR',
         'currentSeason': {'id': 510, 'startDate': '2020-01-08', 'endDate': '2020-01-12', 'currentMatchday': None,
                           'winner': None}, 'numberOfAvailableSeasons': 3, 'lastUpdated': '2020-01-13T10:40:04Z'},
        {'id': 2014, 'area': {'id': 2224, 'name': 'Spain', 'countryCode': 'ESP',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/en/9/9a/Flag_of_Spain.svg'},
         'name': 'Primera Division', 'code': 'PD', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 519, 'startDate': '2019-08-16', 'endDate': '2020-05-24', 'currentMatchday': 26,
                           'winner': None}, 'numberOfAvailableSeasons': 27, 'lastUpdated': '2020-02-29T12:31:18Z'},
        {'id': 2079, 'area': {'id': 2224, 'name': 'Spain', 'countryCode': 'ESP',
                              'ensignUrl': 'https://upload.wikimedia.org/wikipedia/en/9/9a/Flag_of_Spain.svg'},
         'name': 'Copa del Rey', 'code': 'CDR', 'emblemUrl': None, 'plan': 'TIER_THREE',
         'currentSeason': {'id': 553, 'startDate': '2019-11-13', 'endDate': '2020-04-18', 'currentMatchday': None,
                           'winner': None}, 'numberOfAvailableSeasons': 3, 'lastUpdated': '2020-02-13T23:59:50Z'},
        {'id': 2000, 'area': {'id': 2267, 'name': 'World', 'countryCode': 'INT', 'ensignUrl': None},
         'name': 'FIFA World Cup', 'code': 'WC', 'emblemUrl': None, 'plan': 'TIER_ONE',
         'currentSeason': {'id': 1, 'startDate': '2018-06-14', 'endDate': '2018-07-15', 'currentMatchday': 3,
                           'winner': {'id': 773, 'name': 'France', 'shortName': 'France', 'tla': 'FRA',
                                      'crestUrl': 'https://upload.wikimedia.org/wikipedia/en/c/c3/Flag_of_France.svg'}},
         'numberOfAvailableSeasons': 2, 'lastUpdated': '2018-08-23T12:16:17Z'}]
    filtered = list(filter(lambda x: x['id'] == competition_id, competitions))
    if filtered:
        return filtered[0]
    return None
