#!/usr/bin/env python

"""
Clase encargada de relacionarse con la base de datos.
"""

import configparser
import os
from getpass import getpass

import MySQLdb
from MySQLdb._exceptions import OperationalError

from footballapi.data.Singleton import Singleton

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.5"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"


class Database(metaclass=Singleton):
    def __init__(self, is_admin=False):
        """
        Clase encargada de relacionarse con la base de datos
        MySQL, solicitar datos y escribir.

        Parameters
        ----------
        is_admin: bool
            Indica si se necesitan permisos para escritura
        """
        self.is_admin = is_admin
        self.connected = False
        self._db = None

        # Archivo de configuración
        self.config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
        path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
        self.config.read(os.path.join(path, 'settings.cfg'))

    def __del__(self):
        if self._db:
            print("Cerrando conexión con la base de datos")
            self._db.close()

    def connect(self, force=False):
        """
        Realiza la conexión a la base de datos

        Parameters
        ----------
        force: bool
            Forzar la apertura la conexión
        """

        if force or not self.connected:
            if self.is_admin:
                user = self.config['DATABASE']['admin']
                if os.environ["ADF_ADMIN_PASSW"]:
                    passw = os.environ["ADF_ADMIN_PASSW"]
                else:
                    passw = getpass("Admin password: ")
            else:
                user = self.config['DATABASE']['user']
                if os.environ["ADF_DEFAULT_PASSW"]:
                    passw = os.environ["ADF_DEFAULT_PASSW"]
                else:
                    passw = getpass("default password: ")

            try:
                # Cerrar la base de datos en caso de que este abierta
                if self._db:
                    self._db.close()
                # logging.info("Abriendo la conexión a la base de datos")
                self._db = MySQLdb.connect(self.config['DATABASE']['dir'], user, passw, self.config['DATABASE']['name'],
                                           port=int(self.config['DATABASE']['port']))
                del passw, user
                self.connected = True
                self._db.autocommit(True)
            except OperationalError as e:
                self.connected = False
                print(f"No ha sido posible conectarse a la base de datos: {e}")
                exit(1)

    def consulta(self, query, variables):
        """
        Realiza una consulta en la base de datos

        Parameters
        ----------
        query: str
            Consulta a realizar
        variables: tuple
            Variables de la consulta

        Returns
        -------
        iterable
            Resultados de la consulta
        """
        data = None
        try:
            self.connect()

            cursor = self._db.cursor()

            if variables and len(variables) > 0:
                prepared_query = query % variables
            else:
                prepared_query = query

            cursor.execute(prepared_query)
            data = cursor.fetchall()

        except AttributeError as e:
            self.connect(force=True)

        except Exception as e:
            print(e)

        return data

    def orden(self, orden, variables):
        """
        Ejecuta una orden en la base de datos

        Parameters
        ----------
         orden: str
            Órden a ejecutar
         variables: tuple
            Variables de la orden

        Raises
        ------
        MySQLdb._exceptions.IntegrityError
            En caso de no existir el id del jugador, un dato duplicado
            o los parámetros no sean correctos
        MySQLdb._exceptions.ProgrammingError
            Error sintáctica en la orden
        MySQLdb._exceptions.OperationalError
            Los parámetros no encajan con las columnas
        """
        try:
            self.connect()

            cursor = self._db.cursor()

            prepared_query = (orden % variables).replace('"None"', 'NULL')

            cursor.execute(prepared_query)
            self._db.commit()

        except MySQLdb._exceptions.IntegrityError as e:
            # Propagar la excepción si es:
            # (1452, 'Cannot add or update a child row: a foreign key constraint fails ...)

            # Rollback in case there is any error
            self._db.rollback()

            if e.args[1].find("foreign key constraint fails") >= 0:
                raise e
            elif e.args[1].find("Column count doesn't match value count") >= 0:
                raise e
            elif e.args[1].find("Duplicate entry") >= 0:
                pass

        except MySQLdb._exceptions.ProgrammingError as e:
            # Error en la sintaxis de la orden
            print(f"\nError sintáctico en la orden: ", (orden % variables).replace('"None"', 'NULL'))
            raise e

        except MySQLdb._exceptions.OperationalError as e:
            # No encajan las columnas
            print(f"\nLas columnas no encajan: ", (orden % variables).replace('"None"', 'NULL'))
            raise e
