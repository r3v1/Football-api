"""
Creación de una metaclase.

https://stackoverflow.com/q/6760685
"""

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.5"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
