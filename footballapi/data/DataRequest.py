#!/usr/bin/env python


"""
Clase encargada de solicitar todos los datos necesarios a la base de datos.
"""

import configparser
import os
from datetime import datetime, date

import orjson

from footballapi.data.Database import Database
from footballapi.data.Singleton import Singleton
from footballapi.data.sofifa import SoFifa, SofifaPlayerNotFoundException
from footballapi.data.sofifadb import SoFifaDB

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.5"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"


class DataRequest(metaclass=Singleton):
    def __init__(self):
        self._sofifadb = SoFifaDB()
        self._sf = SoFifa()
        self._db = Database(is_admin=True)

        # Guardar los datos para no solicitarlos de nuevo
        self._partidos = {}
        self._fechas_proximas = {}
        self._fd2sofifaid = {}
        self._history = []

        # Archivo de configuración
        self.config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
        path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
        self.config.read(os.path.join(path, 'settings.cfg'))

    def get_partidos(self, temporada=None, fd_id_equipo=None):
        """
        Devuelve todos los partidos guardados en la base de datos. Es posible filtrar por
        temporada y/o equipo.

        Parameters
        ----------
        temporada: str
            Temporada a obtener (formateada según "yy-yy". Ejemplo: "18-19")
        fd_id_equipo: str
            Id del equipo a obtener sus resultados.
        """
        variables = ()

        if (temporada and temporada not in self._partidos.keys()) or fd_id_equipo:
            consulta = "SELECT TEMPORADA, JORN, LOCAL_ID, VISITANTE_ID, RESULTADO_LOCAL, RESULTADO_VISITANTE, FECHA " \
                       "FROM PARTIDO"
            if temporada and fd_id_equipo:
                consulta += " WHERE TEMPORADA='%s' AND (LOCAL_ID=%s OR VISITANTE_ID=%s)"
                variables = (temporada, fd_id_equipo, fd_id_equipo)
            elif temporada and not fd_id_equipo:
                consulta += " WHERE TEMPORADA='%s'"
                variables = temporada
            elif not temporada and fd_id_equipo:
                consulta += " WHERE LOCAL_ID=%s OR VISITANTE_ID=%s"
                variables = (fd_id_equipo, fd_id_equipo)

            result = self._db.consulta(consulta, variables)

            partidos = []
            if result and len(result) > 0:
                for partido in result:
                    # Información del partido
                    partidos.append(
                        {
                            'TEMPORADA': partido[0],
                            'JORN': partido[1],
                            'LOCAL_ID': partido[2],
                            'VISITANTE_ID': partido[3],
                            'RESULTADO_LOCAL': partido[4],
                            'RESULTADO_VISITANTE': partido[5],
                            'FECHA': partido[6]
                        }
                    )

                self._partidos[temporada] = partidos

        return self._partidos[temporada] if temporada in self._partidos.keys() else None

    def get_equipo_titular(self, temporada, jornada, fd_id):
        """
        Devuelve los ids del equipo titular que jugó la jornada dada de la temporada dada.

        Parameters
        ----------
        temporada: str
            Temporada referida al equipo solicitado
        jornada: str
            Jornada referida al equipo solicitado
        fd_id: str
            Football Data id del equipo solicitado
        """
        consulta = "SELECT TEMPORADA, JORNADA, FD_ID, J0, J1, J2, J3, J4, J5, J6, J7, J8, J9, J10, TACTICA, " \
                   "COMPETITION FROM EQUIPO_TITULAR WHERE TEMPORADA='%s' AND JORNADA='%s' AND FD_ID='%s'"
        variables = (temporada, jornada, fd_id)

        result = self._db.consulta(consulta, variables)

        equipo_titular = {
            'TEMPORADA': result[0][0],
            'JORNADA': result[0][1],
            'FD_ID': result[0][2],
            'J0': result[0][3],
            'J1': result[0][4],
            'J2': result[0][5],
            'J3': result[0][6],
            'J4': result[0][7],
            'J5': result[0][8],
            'J6': result[0][9],
            'J7': result[0][10],
            'J8': result[0][11],
            'J9': result[0][12],
            'J10': result[0][13],
            'TACTICA': result[0][14],
            'COMPETIITON': result[0][15]
        }

        return equipo_titular

    def get_jugador_sofifa(self, sofifa_id, fecha, recursion_level=0):
        """
        Solicita a la base de datos los atributos de un jugador dado
        su sofifa_id en una fecha concreta.

        Parameters
        ----------
        sofifa_id: str
            Sofifa id del jugador buscado
        fecha: str
            Fecha del jugador, i.e. yyyy-MM-dd

        Returns
        -------
        dict
            Diccionario con toda la información del jugador
        """

        orden = "SELECT JUGADOR.SOFIFA_ID, NOMBRE, ATROFENSIVO.FECHA, GENERAL, POTENCIAL, CENTROS, DEFINICION, " \
                "PRECISION_CABEZA, PASES_CORTOS, VOLEAS, REGATES, EFECTO, PRECISION_FALTAS, PASES_LARGOS, " \
                "CONTROL_DE_BALON, ACELERACION, VELOCIDAD, AGILIDAD, ATRMOVIMIENTO.REFLEJOS, EQUILIBRIO, " \
                "POTENCIA, SALTO, RESISTENCIA, FUERZA, TIROS_LEJANOS, AGRESIVIDAD, INTERCEPCION, " \
                "ATRMENTALIDAD.COLOCACION, VISION, PENALTIS, COMPOSTURA, MARCAJE, ROBOS, ENTRADA_AGRESIVA, ESTIRADA, " \
                "PARADAS, SAQUES, ATRPORTERO.COLOCACION, ATRPORTERO.REFLEJOS " \
                "FROM JUGADOR INNER JOIN ATROFENSIVO ON JUGADOR.SOFIFA_ID=ATROFENSIVO.SOFIFA_ID " \
                "INNER JOIN ATRTECNICA ON ATROFENSIVO.SOFIFA_ID=ATRTECNICA.SOFIFA_ID AND " \
                "ATRTECNICA.FECHA=ATROFENSIVO.FECHA INNER JOIN ATRMOVIMIENTO ON " \
                "ATROFENSIVO.SOFIFA_ID=ATRMOVIMIENTO.SOFIFA_ID AND ATROFENSIVO.FECHA=ATRMOVIMIENTO.FECHA " \
                "INNER JOIN ATRPOTENCIA ON ATROFENSIVO.SOFIFA_ID=ATRPOTENCIA.SOFIFA_ID " \
                "AND ATROFENSIVO.FECHA=ATRPOTENCIA.FECHA INNER JOIN ATRMENTALIDAD ON " \
                "ATROFENSIVO.FECHA=ATRMENTALIDAD.FECHA AND ATRMENTALIDAD.SOFIFA_ID=ATROFENSIVO.SOFIFA_ID INNER JOIN " \
                "ATRDEFENSA ON ATROFENSIVO.SOFIFA_ID=ATRDEFENSA.SOFIFA_ID AND ATROFENSIVO.FECHA=ATRDEFENSA.FECHA " \
                "INNER JOIN ATRPORTERO ON ATRPORTERO.SOFIFA_ID=ATROFENSIVO.SOFIFA_ID AND " \
                "ATRPORTERO.FECHA=ATROFENSIVO.FECHA INNER JOIN ATRGENERALES ON " \
                "ATRGENERALES.SOFIFA_ID=ATROFENSIVO.SOFIFA_ID AND ATROFENSIVO.FECHA=ATRGENERALES.FECHA " \
                "WHERE JUGADOR.SOFIFA_ID='%s'"

        variables = (sofifa_id)
        fecha_mas_proxima = None
        if fecha is not None:
            fecha_mas_proxima = self.fecha_mas_proxima_a(fecha)
            if fecha_mas_proxima:
                orden += " AND ATROFENSIVO.FECHA='%s'"
                variables = (sofifa_id, fecha_mas_proxima)

        resultado = self._db.consulta(orden, variables)

        if resultado:
            # Parsear cada atributo
            jugador = {
                'JUGADOR.SOFIFA_ID': resultado[0][0],
                'NOMBRE': resultado[0][1],
                'FECHA': resultado[0][2],
                'GENERAL': resultado[0][3],
                'POTENCIAL': resultado[0][4],
                'OFENSIVA': [resultado[0][5], resultado[0][6], resultado[0][7], resultado[0][8], resultado[0][9]],
                'TÉCNICA': [resultado[0][10], resultado[0][11], resultado[0][12], resultado[0][13], resultado[0][14]],
                'MOVIMIENTO': [resultado[0][15], resultado[0][16], resultado[0][17], resultado[0][18],
                               resultado[0][19]],
                'POTENCIA': [resultado[0][20], resultado[0][21], resultado[0][22], resultado[0][23], resultado[0][24]],
                'MENTALIDAD': [resultado[0][25], resultado[0][26], resultado[0][27], resultado[0][28], resultado[0][29],
                               resultado[0][30]],
                'DEFENSA': [resultado[0][31], resultado[0][32], resultado[0][33]],
                'PORTERO': [resultado[0][34], resultado[0][35], resultado[0][36], resultado[0][37], resultado[0][38]]
            }
        else:
            if not fecha_mas_proxima:
                fecha_mas_proxima = self.fecha_mas_proxima_a(fecha)
            roaster = self._sofifadb.date2roaster(fecha_mas_proxima)

            if recursion_level < 1:
                sf = SoFifa()
                sf.get_player(sofifa_id, history_items=1, from_roaster=roaster, force=True)

                # Llamar recursivamente al método
                jugador = self.get_jugador_sofifa(sofifa_id, fecha_mas_proxima, recursion_level + 1)

                orders = [
                    ('INSERT INTO %s VALUES("%s","%s",%s,%s)', (
                        'ATRGENERALES', jugador['JUGADOR.SOFIFA_ID'], fecha_mas_proxima, jugador['GENERAL'],
                        jugador['POTENCIAL'])),
                    ('INSERT INTO %s VALUES("%s","%s",%s)', (
                        'ATROFENSIVO', jugador['JUGADOR.SOFIFA_ID'], fecha_mas_proxima,
                        ",".join(map(str, jugador['OFENSIVA'])))),
                    ('INSERT INTO %s VALUES("%s","%s",%s)', (
                        'ATRTECNICA', jugador['JUGADOR.SOFIFA_ID'], fecha_mas_proxima,
                        ",".join(map(str, jugador['TÉCNICA'])))),
                    ('INSERT INTO %s VALUES("%s","%s",%s)', (
                        'ATRMOVIMIENTO', jugador['JUGADOR.SOFIFA_ID'], fecha_mas_proxima,
                        ",".join(map(str, jugador['MOVIMIENTO'])))),
                    ('INSERT INTO %s VALUES("%s","%s",%s)', (
                        'ATRPOTENCIA', jugador['JUGADOR.SOFIFA_ID'], fecha_mas_proxima,
                        ",".join(map(str, jugador['POTENCIA'])))),
                    ('INSERT INTO %s VALUES("%s","%s",%s)', (
                        'ATRMENTALIDAD', jugador['JUGADOR.SOFIFA_ID'], fecha_mas_proxima,
                        ",".join(map(str, jugador['MENTALIDAD'])))),
                    ('INSERT INTO %s VALUES("%s","%s",%s)', (
                        'ATRDEFENSA', jugador['JUGADOR.SOFIFA_ID'], fecha_mas_proxima,
                        ",".join(map(str, jugador['DEFENSA'])))),
                    ('INSERT INTO %s VALUES("%s","%s",%s)', (
                        'ATRPORTERO', jugador['JUGADOR.SOFIFA_ID'], fecha_mas_proxima,
                        ",".join(map(str, jugador['PORTERO']))))
                ]

                for order in orders:
                    self._db.orden(*order)
            elif recursion_level < 2:
                # Devolver el registro de la base de datos más cercana a la fecha buscada
                # El caso de Ansu Fati por ejemplo, se registra en sofifa por primera vez
                # el 6 de noviembre de 2019, pero como ha jugado partidos antes de esa fecha,
                # no se pueden obtener esos datos. Es decir, los datos anteriores al 6-11-2019
                # se refereciarán a ese partido.

                # Obtener primero las fechas de un jugador
                fechas_jugador = list(map(lambda x: x[0],
                                          self._db.consulta("SELECT FECHA FROM ATRPORTERO WHERE SOFIFA_ID=%s",
                                                            (sofifa_id))))

                # Obtener la fecha más cercana posible
                fecha_mas_proxima = datetime.strptime(fecha_mas_proxima, '%Y-%m-%d')
                fechas_jugador.sort(key=lambda x: abs((fecha_mas_proxima - datetime(x.year, x.month, x.day)).days))

                return self.get_jugador_sofifa(sofifa_id, fechas_jugador[0].strftime('%Y-%m-%d'), recursion_level + 1)
            else:
                raise SofifaPlayerNotFoundException(f"Id {sofifa_id} con fecha {fecha} no encontrado")

        return jugador

    def fecha_mas_proxima_a(self, fecha):
        """
        Es posible que la fecha de un partido no coincida con la fecha de los
        atributos de un jugador. Por ello, se intentará devolver la fecha más
        cercana de sofifa a una fecha dada.

        Parameters
        ----------
        fecha: str, datetime

        Returns
        -------
        str
            Fecha parseada
        """
        # Buscarla en caché primero
        if fecha in self._fechas_proximas.keys():
            return self._fechas_proximas[fecha]

        if type(fecha) == str:
            try:
                fecha = datetime.strptime(fecha, '%Y-%m-%d')
            except ValueError:
                fecha = datetime.strptime(fecha, '%b %d, %Y')
        elif type(fecha) == date:
            fecha = datetime(fecha.year, fecha.month, fecha.day)
        else:
            raise ValueError

        if len(self._history) == 0:
            with open(self.config['PATHS']['history_file'], 'rb') as f:
                self._history = orjson.loads(f.read())['roasters']

        fechas = self._history.copy()
        fechas.sort(key=lambda x: abs((fecha - datetime.strptime(x[1], '%b %d, %Y')).days))
        # fechas = list(filter(lambda x: (fecha - datetime.strptime(x[1], '%b %d, %Y')).days >= 0, fechas))

        fecha_mas_proxima = datetime.strptime(fechas[0][1], '%b %d, %Y').strftime('%Y-%m-%d')
        self._fechas_proximas[fecha.strftime('%Y-%m-%d')] = fecha_mas_proxima

        return fecha_mas_proxima

    def fdid2sofifaid(self, fdid, name, equipo="", allow_input=True):
        """
        Devuelve el sofifa_id de un jugador dado su fd_id

        Parameters
        ----------
        fdid: str
            Football Data id
        allow_input: bool
            Habilita la opción para introducir los ids a mano.
            Puede ser útil en caso de ejecutarse trás un crond
            en el que no pueda haber intervención del usuario,
            en cuyo caso saltará una excepción.

        Returns
        -------
        str
            Sofifa id
        """
        if fdid not in self._fd2sofifaid.keys():
            orden = "SELECT ID.SOFIFA_ID FROM ID WHERE FD_ID='%s'"
            variables = (str(fdid))

            resultado = self._db.consulta(orden, variables)
            if resultado and len(resultado) > 0:
                self._fd2sofifaid[fdid] = resultado[0][0]
            else:
                # En caso de no existir en la base de datos, buscarlo en sofifa
                if name:
                    self._fd2sofifaid[fdid] = self._sf.find_id_on_google(name, equipo=equipo, allow_input=allow_input)
                else:
                    raise KeyError(f'No se ha encontrado el jugador {fdid}. Revísalo manualmente')

        return self._fd2sofifaid[fdid]
