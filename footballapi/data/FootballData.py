#!/usr/bin/env python


"""
Módulo para guardar la información obtenida de los json descargados
de Football Data en la base de datos.
"""

import argparse
import configparser
import http.client
import os
import re
from datetime import date, timedelta, datetime
from dateutil import tz
from time import time

import orjson
from MySQLdb._exceptions import IntegrityError
from MySQLdb._mysql import OperationalError

from DataRequest import DataRequest
from Database import Database
from Singleton import Singleton
from footballapi.colors import bcolors
from sofifa import SoFifa, SofifaPlayerNotFoundException
from sofifadb import SoFifaDB
from footballapi.utils import shin_gasparyan, comp_info

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.7"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"


class FDException(Exception):
    """
    Football Data retrieve exception
    """
    pass


class BadRequestException(FDException):
    """
    Bad request
    Your request was malformed. Most likely the value of a Filter was not set
    according to the Data Type that is expected.
    """
    pass


class RestrictedResourceException(FDException):
    """
    Restricted Resource
    You tried to access a resource that exists, but is not available to you.
    This can be due to the following reasons:
        - the resource is only available to authenticated clients.
        - the resource is only available to clients with a paid subscription.
        - the resource is not available in the API version you are using.
    """
    pass


class NotFoundException(FDException):
    """
    Not Found
    You tried to access a resource that doesn't exist.
    """
    pass


class TooManyRequestsException(FDException):
    """
    Too Many Requests
    You exceeded your API request quota. See Request-Throttling for more information.
    """
    pass


class KeyNotFoundException(FDException):
    """
    JSON key not found
    """
    pass


class InconsistentLineupException(FDException):
    """If lineup has mora than 11 players or more than 1 goalkeeper"""
    pass


class FDPlayer:
    def __init__(self):
        """Descriptor de un jugador de fútbol obtenido de Football Data"""
        self.fd_id = None
        self.sofifa_id = None
        self.name = None
        self.position = None
        self.shirt_number = None

        self._db = Database(is_admin=True)
        self._dr = DataRequest()
        self._sf = SoFifa()

        self.sofifa_raw_attr = None
        self.full_attr = None

    def __str__(self):
        return f'{self.name} (id: {self.fd_id})'

    def __repr__(self):
        return f"[TEAM] {self.name} (id: {self.fd_id})"

    def load(self, player: dict):
        """
        Carga un jugador en formato JSON y actualiza los valores.

        Parameters
        ----------
        player: dict
            Diccionario representando el jugador
        """
        if player['id']:
            self.fd_id = player['id']
        else:
            raise KeyNotFoundException(f"Player id not found in JSON")

        if player['name']:
            self.name = player['name']
        else:
            raise KeyNotFoundException(f"Player name not found in JSON")

        if player['position']:
            self.position = player['position']
        else:
            raise KeyNotFoundException(f"Player position not found in JSON")

        if player['shirtNumber']:
            self.shirt_number = player['shirtNumber']

    def load_from_id(self, fd_id: int):
        """Dado un fd_id del jugador, accede a la base de datos
        a buscar la información asociada a él"""

        self.fd_id = fd_id

    def to_database(self, equipo=""):
        """
        Combine intructions to update database with
        player data.

        Returns
        list:
            Instructions
        """
        instructions = [
            ('INSERT INTO %s VALUES("%s", "%s", "%s", "%s")',
             ('JUGADOR_FD', self.fd_id, self.name, self.position, self.shirt_number))
        ]

        for instruction in instructions:
            try:
                self._db.orden(*instruction)
            except IntegrityError:
                print()
                self.get_sofifa_id(equipo=equipo)
                self._db.orden(
                    "INSERT INTO %s VALUES(%s, %s)",
                    ("ID", self.sofifa_id, self.fd_id)
                )
                self._db.orden(*instruction)

    def get_sofifa_id(self, equipo=""):
        """Get Sofifa id from player"""
        if not self.sofifa_id:
            try:
                self.sofifa_id = self._dr.fdid2sofifaid(self.fd_id, self.name, equipo=equipo)
                self.to_database()
            except KeyError as e:
                print(f"{bcolors.FAIL}[!]{bcolors.ENDC} {self} no se ha encontrado. Agrégalo manualmente "
                      f"a la base de datos")
                raise e

        return self.sofifa_id

    def build(self, date):
        """
        Genera la información agregada de un equipo en una fecha concreta dada,
        según el algoritmo de agregación dado.

        Parameters
        ----------
        equipo: dict
            Información del equipo que jugó en la fecha dada
        fecha: str
            Fecha en la que generar el equipo
        agregacion: str
            Algoritmo de agregación. 'shin_gasparyan', ...

        Returns
        -------
        str
        """
        # Obtener el sofifa id
        self.get_sofifa_id()

        # Obtener los atributos del jugador
        if not self.sofifa_raw_attr:
            # Insertar el jugador por si las moscas
            try:
                self.sofifa_raw_attr = self._dr.get_jugador_sofifa(self.sofifa_id, date)
            except SofifaPlayerNotFoundException as e:
                raise e
            except Exception as e:
                # Insertar el jugadoe en la base de datos
                self._db.orden('INSERT INTO %s VALUES(%s, %s)', ('ID', self.sofifa_id, self.fd_id))
                self.sofifa_raw_attr = self._dr.get_jugador_sofifa(self.sofifa_id, date)

            self.full_attr = self.sofifa_raw_attr['OFENSIVA'] + self.sofifa_raw_attr['TÉCNICA'] + \
                             self.sofifa_raw_attr['MOVIMIENTO'] + self.sofifa_raw_attr['POTENCIA'] + \
                             self.sofifa_raw_attr['MENTALIDAD'] + self.sofifa_raw_attr['DEFENSA'] + \
                             self.sofifa_raw_attr['PORTERO']

    def in_database(self, date, equipo=""):
        """
        Comprueba si el jugador se encuentra en la base de datos

        Parameters
        ----------
        date: str
            Fecha a buscar

        Returns
        -------
        bool
        """
        self.get_sofifa_id(equipo=equipo)

        data = self._db.consulta("SELECT * FROM ATRGENERALES WHERE SOFIFA_ID=%s AND FECHA='%s'", (self.sofifa_id, date))
        if data and len(data) > 0:
            return True
        else:
            return False


class FDTeam:
    def __init__(self):
        """Descriptor de un equipo de fútbol obtenido de Football Data"""
        self.fd_id = None
        self.sofifa_id = None
        self.name = None
        self.lineup = None
        self.lineup_available = False

        self.sg_attr = None
        self.full_attr = None

        self.tactica = None

        self._dr = DataRequest()

    def __str__(self):
        return f'{self.name}'

    def __repr__(self):
        return f"[TEAM] {self.name} (id: {self.fd_id})"

    def _check_lineup(self):
        """
        Check if lineup is correct

        Raises
        ------
        InconsistentLineupException
        """
        count = {'Goalkeeper': 0, 'Defender': 0, 'Midfielder': 0, 'Attacker': 0}
        for player in self.lineup:
            count[player.position] += 1

        if count['Goalkeeper'] != 1:
            raise InconsistentLineupException(f"Hay {count['Goalkeeper']} porteros en la alineación")
        if sum(count.values()) != 11:
            raise InconsistentLineupException(f"Hay {sum(count.values())} jugadores en la alineación")

        self.lineup_available = True
        self.tactica = (1, count['Defender'], count['Midfielder'], count['Attacker'])

    def load(self, team: dict):
        """
        Carga un equipo en formato JSON y actualiza los valores.

        Parameters
        ----------
        team: dict
            Diccionario representando el equipo

        Raises
        ------
        KeyNotFoundException
        """
        assert type(team) == dict

        if team['id']:
            self.fd_id = team['id']
        else:
            raise KeyNotFoundException(f"Team id not found in JSON")

        if team['name']:
            self.name = team['name']
        else:
            raise KeyNotFoundException(f"Team name not found in JSON")

        if 'lineup' in team.keys() and team['lineup']:
            self.lineup = []
            for player in team['lineup']:
                fdp = FDPlayer()
                fdp.load(player)
                self.lineup.append(fdp)

            self._check_lineup()
        else:
            self.lineup_available = False
            self.lineup = None

    def load_sql_lineup(self, season_short: str, matchday: int, fd_id: int):
        """Lee la base de datos y actualiza los valores del equipo"""
        et = self._dr.get_equipo_titular(season_short, matchday, fd_id)

        self.lineup = []
        for i in range(11):
            fdplayer = FDPlayer()
            fdplayer.load_from_id(int(et[f'J{i}']))

            self.lineup.append(fdplayer)

        # No realizamos la comprobación de la alineación
        # por que estando en la base de datos, se supone
        # que tiene que estar bien
        # self._check_lineup()
        self.lineup_available = True
        self.tactica = tuple(map(int, et['TACTICA'].split(',')))

    def to_database(self):
        """
        Combine intructions to update database with team data.

        Returns
        list:
            Instructions
        """
        for p in self.lineup:
            p.to_database(equipo=self.name)

    def build(self, date):
        """
        Genera la información agregada de un equipo en una fecha concreta dada,
        para la agregación revillas y shin_gasparyan

        Parameters
        ----------
        date: str
            Fecha en la que generar el equipo
        """
        if self.lineup:
            for p in self.lineup:
                p.build(date)

            self.sg_attr = shin_gasparyan([p.sofifa_raw_attr for p in self.lineup])
            self.full_attr = [p.full_attr for p in self.lineup]

    def sg2str(self):
        s = None
        if self.sg_attr:
            s = ",".join(map(str, self.sg_attr.values()))

        return s

    def massive_download(self, date):
        """
        Reúne los ids de los jugadores que aún no
        esten en la base de datos

        Parameters
        ----------
        date: str

        Returns
        -------
        list:
            Lista con ids y fechas
        """
        j = []
        fecha_cercana = self._dr.fecha_mas_proxima_a(date)
        if self.lineup_available:
            for p in self.lineup:
                if not p.sofifa_id and not p.in_database(fecha_cercana, equipo=self.name):
                    p.get_sofifa_id(equipo=self.name)
                    j += [(p.sofifa_id, fecha_cercana)]

        return j


class FDMatch:
    def __init__(self):
        """Descriptor de un partido de fútbol obtenido de Football Data"""
        self.matchday = None
        self.stage = None
        self.season = None
        self.season_short = None
        self.season_id = None
        self.status = None
        self.competition_id = None
        self.fd_id = None
        self.date = None
        self.hour = None
        self.datetime = None
        self.date_full = None
        self.local_team = None
        self.away_team = None
        self.result = None
        self.result_report = None
        self.result_local = None
        self.result_away = None

        self.sg_attr = None
        self.full_attr = None

        self.available_lineups = False

        self._db = Database(is_admin=True)
        self._sfdb = SoFifaDB()

    def __repr__(self):
        lt = str(self.local_team)
        at = str(self.away_team)
        teams = f"{lt:^26}-{at:^26}"
        if self.matchday:
            m = f"J{self.matchday}"
        elif self.stage:
            m = self.stage
        else:
            m = ""
        if self.result_report is not None:
            return f"[T{self.season_short} {m} {self.date_full}] {teams}: {self.result_report}"
        else:
            return f"[T{self.season_short} {m} {self.date_full}] {teams}"

    def load(self, match: dict, competition_id=None):
        """
        Carga un partido en formato JSON y actualiza los valores.

        Parameters
        ----------
        match: dict
            Diccionario representando el partido

        Raises
        ------
        KeyNotFoundException
        """
        assert type(match) == dict

        if match['id']:
            self.fd_id = match['id']
        else:
            raise KeyNotFoundException(f"Match id not found in JSON")

        self.competition_id = competition_id

        if match['season']:
            s = match['season']
            self.season_id = s['id']

            year = re.compile(r"([0-9]{4})")
            self.season = f"{year.search(s['startDate']).group(1)}-{year.search(s['endDate']).group(1)}"
            self.season_short = f"{year.search(s['startDate']).group(1)[-2:]}-{year.search(s['endDate']).group(1)[-2:]}"
        else:
            raise KeyNotFoundException(f"Season information not found in JSON")

        if match['utcDate']:
            # Aumentar una hora (UTC+1)
            utc = datetime.strptime(match['utcDate'], "%Y-%m-%dT%H:%M:%SZ")
            utc = utc.replace(tzinfo=tz.tzutc())
            local = utc.astimezone(tz.tzlocal())

            self.hour = local.strftime("%H:%M:%S")
            self.date = local.strftime("%Y-%m-%d")
            self.date_full = local.strftime("%Y-%m-%d %H:%M:%S")
            self.datetime = local
        else:
            raise KeyNotFoundException(f"Match date not found in JSON")

        if 'stage' in match.keys():
            self.stage = match['stage']

        if match['status']:
            self.status = match['status']
        else:
            raise KeyNotFoundException(f"Match status not found in JSON")

        # if match['matchday']:
        self.matchday = match['matchday']
        # else:
        #     raise KeyNotFoundException(f"Matchday not found in JSON")

        if match['score']:
            winner = match['score']['winner']
            if winner == 'HOME_TEAM':
                self.result = 0
                self.result_local = 'G'
                self.result_away = 'P'
                self.result_report = winner
            elif winner == 'AWAY_TEAM':
                self.result = 2
                self.result_local = 'P'
                self.result_away = 'G'
                self.result_report = winner
            elif winner == 'DRAW':
                self.result = 1
                self.result_local = 'E'
                self.result_away = 'E'
                self.result_report = winner
        else:
            raise KeyNotFoundException(f"Match result not found in JSON")

        if match['homeTeam']:
            self.local_team = FDTeam()
            self.local_team.load(match['homeTeam'])
        else:
            raise KeyNotFoundException(f"Home team not found in JSON")

        if match['awayTeam']:
            self.away_team = FDTeam()
            self.away_team.load(match['awayTeam'])
        else:
            raise KeyNotFoundException(f"Away team not found in JSON")

        self.available_lineups = self.local_team.lineup_available and self.away_team.lineup_available

    def load_sql(self, match: dict):
        """
        Carga la respuesta del servidor y actualiza los valores.

        Parameters
        ----------
        match: dict
            Respuesta del servidor representando el partido

        Raises
        ------
        KeyNotFoundException
        """
        assert type(match) == dict

        if match['TEMPORADA']:
            self.season_short = match['TEMPORADA']
            self.season = f"20{match['TEMPORADA'].split('-')[0]}-20{match['TEMPORADA'].split('-')[1]}"
        else:
            raise KeyNotFoundException(f"Season information not found in JSON")

        if match['FECHA']:
            d = match['FECHA']
            self.hour = d.strftime("%H:%M:%S")
            self.date = d.strftime("%Y-%m-%d")
            self.date_full = d.strftime("%Y-%m-%d %H:%M:%S")
            self.datetime = d
        else:
            raise KeyNotFoundException(f"Match date not found in JSON")

        if match['JORN']:
            self.matchday = match['JORN']
        else:
            raise KeyNotFoundException(f"Matchday not found in JSON")

        self.status = "FINISHED"

        self.result_local = match['RESULTADO_LOCAL']
        self.result_away = match['RESULTADO_VISITANTE']
        if match['RESULTADO_LOCAL'] == 'P':
            self.result = 2
        elif match['RESULTADO_LOCAL'] == 'G':
            self.result = 0
        elif match['RESULTADO_LOCAL'] == 'E':
            self.result = 1

        if match['LOCAL_ID']:
            self.local_team = FDTeam()
            self.local_team.fd_id = int(match['LOCAL_ID'])
        else:
            raise KeyNotFoundException(f"Home team not found in JSON")

        if match['VISITANTE_ID']:
            self.away_team = FDTeam()
            self.away_team.fd_id = int(match['VISITANTE_ID'])
        else:
            raise KeyNotFoundException(f"Away team not found in JSON")

        self.available_lineups = self.local_team.lineup_available and self.away_team.lineup_available

    def to_database(self):
        """
        Combine intructions to update database with
        match data.

        Returns
        list:
            Instructions
        """
        instructions = [
            ('INSERT INTO %s VALUES("%s")', ('TEMPORADA', self.season_short)),  # Insertar la temporada
            ('INSERT INTO %s VALUES("%s")', ('FECHAS', self.date_full)),  # Insert date
            ('INSERT INTO %s VALUES("%s", "%s", "%s")',
             ('JORNADA', self.competition_id, self.season_short, self.matchday)),  # Insert matchday
            ('INSERT INTO %s VALUES("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s")',
             ('PARTIDO', self.competition_id, self.season_short, self.matchday, self.local_team.fd_id,
              self.away_team.fd_id, self.result_local, self.result_away, self.date_full)),  # Insert match
        ]

        for i in instructions:
            self._db.orden(*i)

        self.local_team.to_database()
        self.away_team.to_database()

        instructions = [
            ('INSERT INTO %s '
             'VALUES("%s", "%s", "%s", "%s","%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s")',
             ('EQUIPO_TITULAR', self.competition_id, self.season_short, self.matchday, self.local_team.fd_id,
              self.local_team.lineup[0].fd_id, self.local_team.lineup[1].fd_id, self.local_team.lineup[2].fd_id,
              self.local_team.lineup[3].fd_id, self.local_team.lineup[4].fd_id, self.local_team.lineup[5].fd_id,
              self.local_team.lineup[6].fd_id, self.local_team.lineup[7].fd_id, self.local_team.lineup[8].fd_id,
              self.local_team.lineup[9].fd_id, self.local_team.lineup[10].fd_id,
              ",".join(map(str, self.local_team.tactica)))),  # Insert local lineup
            ('INSERT INTO %s '
             'VALUES("%s", "%s", "%s", "%s","%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s")',
             ('EQUIPO_TITULAR', self.competition_id, self.season_short, self.matchday, self.away_team.fd_id,
              self.away_team.lineup[0].fd_id, self.away_team.lineup[1].fd_id, self.away_team.lineup[2].fd_id,
              self.away_team.lineup[3].fd_id, self.away_team.lineup[4].fd_id, self.away_team.lineup[5].fd_id,
              self.away_team.lineup[6].fd_id, self.away_team.lineup[7].fd_id, self.away_team.lineup[8].fd_id,
              self.away_team.lineup[9].fd_id, self.away_team.lineup[10].fd_id,
              ",".join(map(str, self.away_team.tactica)))),  # Insert away lineup
        ]

        for i in instructions:
            self._db.orden(*i)

    def build(self):
        """
        Genera la información agregada de un equipo en una fecha concreta dada,
        para la agregación revillas y shin_gasparyan
        """
        # Update available lineup status
        self.available_lineups = self.local_team.lineup_available and self.away_team.lineup_available

        if self.available_lineups:
            assert self.date, f"No queda definida la fecha del partido: {self.date}"

            self.local_team.build(self.date)
            self.away_team.build(self.date)

            self.sg_attr = (self.local_team.sg_attr, self.away_team.sg_attr)
            self.full_attr = (self.local_team.full_attr, self.away_team.full_attr)

    def sg2str(self, num_res=None):
        if self.sg_attr:
            lt = self.local_team.sg2str()
            at = self.away_team.sg2str()
            if self.result is None or not num_res:
                r = '?'
            else:
                r = self.res2str(num_res)
            return f"{lt},{at},{r}"

        return None

    def res2str(self, num_res: int):
        """Convierte el resultado final del partido
        a 2 o 3 resultados"""
        if num_res == 2:
            return 0 if not self.result else 1
        elif num_res == 3:
            return self.result
        else:
            raise NotImplementedError("El número de resultados tiene que ser 2 o 3")

    def massive_download(self):
        """
        Reúne los ids de los jugadores que aún no
        esten en la base de datos

        Returns
        -------
        tuple:
            Tupla indicando los jugadores y los roasters
        """
        j = []
        if self.available_lineups:
            j += self.local_team.massive_download(self.date)
            j += self.away_team.massive_download(self.date)

        if len(j):
            jugadores, roasters = list(zip(*map(lambda x: (x[0], self._sfdb.date2roaster(x[1])), j)))
            return jugadores, roasters

        return [], []


class RequestManager(metaclass=Singleton):

    def __init__(self):
        self._requests_done = 0

        # Archivo de configuración
        self.config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
        path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
        self.config.read(os.path.join(path, 'settings.cfg'))

        # requests file
        self.requests_file = self.config['PATHS']['requests_file']

    def _save_response(self, response, request_type):
        """
        Guarda la respuesta de la solicitud en local

        :param response: json
        :param request_type: query
        :return:
        """
        if "errorCode" not in response.keys():
            os.makedirs(self.config['PATHS']['json_dir'], exist_ok=True)
            file_path = os.path.join(self.config['PATHS']['json_dir'], request_type.replace("/", "_").replace("&", "_"))
            with open(file_path + '.json', 'wb') as f:
                f.write(orjson.dumps(response))

            return file_path

    def _add_request(self):
        """
        Incrementa en 1 el número de solicitudes hechas.
        """
        os.makedirs(self.config['PATHS']['db_dir'], exist_ok=True)

        with open(self.requests_file, 'a') as f:
            f.write(str(time()) + "\n")

    def request(self, request_type, silence=True):
        """
        Solicita un recurso al servidor de Footbal Data

        :param request_type: string, query a solicitar
        :param online_request: boolean, buscar en internet o en un archivo local
        :return: json
        """

        assert os.environ['FOOTBALL_DATA_AUTHORIZATION'], "Autorización Football Data no definida"

        # Make online request to football-data
        connection = http.client.HTTPConnection(self.config['REQUESTS']['fd_request_url'])
        headers = {'X-Auth-Token': os.environ['FOOTBALL_DATA_AUTHORIZATION']}
        connection.request('GET', request_type, headers=headers)
        r = orjson.loads(connection.getresponse().read().decode())

        # Logs the last request date time
        self._add_request()
        # Updates requests done
        self.get_requests_done()

        msg = f"[#{self._requests_done}]"
        if 'errorCode' in r.keys():
            # Connection error
            if r['errorCode'] == 400:
                raise BadRequestException(f"{msg} Bad request: {request_type} -> {r['message']}")
            elif r['errorCode'] == 403:
                raise RestrictedResourceException(f"{msg} Restricted resource: {request_type} -> {r['message']}")

            elif r['errorCode'] == 404:
                raise NotFoundException(f"{msg} Not found: {request_type} -> {r['message']}")

            elif r['errorCode'] == 429:
                raise TooManyRequestsException(f"{msg} Too many requests: {request_type} -> {r['message']}")
            else:
                # Unknown error
                raise FDException(f"Unknown error ocurred: {r.status_code} -> {r['message']}")
        else:
            # Ok
            if not silence:
                print(bcolors.OKBLUE + "[REQUEST]" + bcolors.ENDC + f" {msg} successfully obtained.")
            return self._save_response(r, request_type)

    def get_requests_done(self):
        """
        Calcula y devuelve el número de solicitudes hechas
        al servidor de Football Data en el último minuto
        :return:  int
        """
        requests_done = 0

        if os.path.exists(self.requests_file):
            with open(self.requests_file, 'r') as f:
                request_list = f.read().split('\n')[::-1]

            for req in request_list:
                try:
                    if time() - float(req) <= 60:
                        requests_done += 1
                    else:
                        break
                except ValueError:
                    pass

            if requests_done == 0:
                os.remove(self.requests_file)

        self._requests_done = requests_done
        return requests_done

    def requests_available(self):
        """
        Devuelve el número de conexiones que quedan por hacer (30 por minuto)
        :return:
        """
        ra = self.config['REQUESTS']['max_requests_per_minute'] - self.get_requests_done()

        if ra <= 0:
            ra = 0

        return ra

    def _open_saved_data(self, request_type):
        """
        Intenta acceder al archivo guardado en local.

        :param request_type: query
        :return: diccionario
        """
        response = None
        request_type = request_type.replace("/", "_").replace("&", "_")

        try:
            with open(os.path.join(self.config['PATHS']['db_dir'], request_type), 'r') as f:
                response = eval(f.read())
        except IOError:
            print(
                bcolors.WARNING + "[!]" + bcolors.ENDC + f"{request_type} file not found. Try to make online request.")
        except Exception as e:
            print(e)

        return response


class FootballData:
    req_man = RequestManager()

    # Available competitions id
    competitions = [
        2000,  # FIFA Mundial
        2001,  # UEFA Champions League
        2002,  # Bundesliga
        2003, 2013,
        2014,  # Primera División
        2015,
        2016,  # Campeonato inglés
        2017, 2018, 2019, 2021, 2077,
        2078,  # Supercopa del Rey
        2079  # Copa del Rey
    ]

    def __init__(self):
        self._db = Database(is_admin=True)
        self._sf = SoFifa()

        # Archivo de configuración
        self.config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
        path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
        self.config.read(os.path.join(path, 'settings.cfg'))

        # Progress bar
        self._bar_len = 35
        self.verbose = 1

    @staticmethod
    def team_parser(match):
        """
        Asegura que la alineación inicial esta ordanada según
        Goalkeeper - Defender - Midfielder - Attacker y además,
        devuelve el número de ellos (táctica utilizada)

        Parameters
        ----------
        match: dict
            Diccionario original del json de football-data con
            la información del equipo

        Returns
        -------
        int
            Id del equipo
        str
            Nombre del equipo
        dict
            Alineción ordenada del equipo titular
        list
            Táctica utilizada por el equipo
        dict
            Banquillo con los suplentes
        """
        assert type(match) == dict

        lineup = {
            'Goalkeeper': [],
            'Defender': [],
            'Midfielder': [],
            'Attacker': []
        }
        for player in match['lineup']:
            lineup[player['position']].append({
                'fd_id': player['id'],
                'name': player['name'],
                'position': player['position'],
                'number': player['shirtNumber']
            })

        bench = {
            'Goalkeeper': [],
            'Defender': [],
            'Midfielder': [],
            'Attacker': []
        }
        for player in match['bench']:
            bench[player['position']].append({
                'fd_id': player['id'],
                'name': player['name'],
                'position': player['position'],
                'number': player['shirtNumber']
            })

        assert len(lineup['Goalkeeper']) == 1, f"Parece que hay más de un portero: {lineup}\nRevísa el json de FD."
        tactica = list(map(len, lineup.values()))
        lineup = lineup['Goalkeeper'] + sorted(lineup['Defender'], key=lambda p: p['number']) \
                 + sorted(lineup['Midfielder'], key=lambda p: p['number']) \
                 + sorted(lineup['Attacker'], key=lambda p: p['number'])
        bench = bench['Goalkeeper'] + sorted(bench['Defender'], key=lambda p: p['number']) \
                + sorted(bench['Midfielder'], key=lambda p: p['number']) \
                + sorted(bench['Attacker'], key=lambda p: p['number'])

        assert sum(tactica) == 11, f"Parece que no hay 11 jugadores de campo: {lineup}\nRevísa el json de FD."
        assert len(bench) >= 3, f"Parece que no hay al menos 3 suplentes: {bench}"
        tactica = ",".join(map(str, tactica))

        return match['id'], match['name'], lineup, tactica, bench

    def progress_bar(self, season, mensaje, i, total):
        if self.verbose:
            # color = f"\r{bcolors.OKBLUE}[{season}]{bcolors.ENDC}"
            color = f"\r\t>"
            progress = f"[{i:>3}/{total}]"
            completed = "#" * int(self._bar_len * i / total)
            not_completed = " " * (self._bar_len - int(self._bar_len * i / total))

            print(f"{color} {mensaje:<{self._bar_len - 5}} {progress} [{completed}{not_completed}]", end="")

    def fd2db(self):
        """
        Guarda la información que haya en el archivo local del json
        de la temporada en la base de datos. Dicho json debe ser el
        descargado de football-data.org, con toda la información
        relevante de los partidos de la temporada
        """

        def to_txt(name, fdmatch):
            """Escribe un fichero con el id del partido"""
            with open(name, 'a') as f:
                f.write(f"{fdmatch.fd_id}\n")

        exc = [
            2000, 2001, 2002, 2016, 2078, 2079,  # Excluir competiciones de formato eliminatario
            2013,  # Ligra brasileña (muchos jugadores desaparecidos)
        ]

        print(f"\n{'*' * 86}\n*{'FootballData --> Database':^84}*\n{'*' * 86}", end="")
        for archivo in os.listdir(self.config['PATHS']['json_dir']):
            archivo = os.path.join(self.config['PATHS']['json_dir'], archivo)
            if os.path.isfile(archivo) and archivo.find('FINISHED') >= 0:
                # j = self._f2db_rec(archivo, False)
                with open(archivo, 'rb') as f:
                    j = orjson.loads(f.read())
                if 'matches' in j.keys():
                    comp_id = j['competition']['id']
                    if comp_id not in exc:
                        partidos_omitidos = 0
                        partidos_correctos = 0
                        comp_name = j['competition']['name']
                        comp_code = j['competition']['code']
                        print("\n" + bcolors.OKBLUE + "[*]" + bcolors.ENDC + f" Solicitando partidos de: "
                                                                             f"{comp_name} ({comp_code})")
                        for i, match in enumerate(j['matches'], start=1):
                            fdmatch = FDMatch()
                            try:
                                fdmatch.load(match, competition_id=j['competition']['id'])
                                self.progress_bar(f'T{fdmatch.season_short}', 'Actualizando la BBDD', i,
                                                  len(j['matches']))
                                fdmatch.to_database()
                                partidos_correctos += 1
                            except KeyNotFoundException as e:
                                # No esta definido bien el JSON
                                print(f"\n{bcolors.FAIL}[OMITIENDO]{bcolors.ENDC} {e} (ID: {fdmatch.fd_id})")
                                to_txt("corrupt_matches.log", fdmatch)
                                partidos_omitidos += 1
                            except SofifaPlayerNotFoundException as e:
                                # El jugador no existe en sofifa
                                print(f"\n{bcolors.FAIL}[OMITIENDO]{bcolors.ENDC} {e} (ID: {fdmatch.fd_id})")
                                partidos_omitidos += 1
                            except InconsistentLineupException as e:
                                # No añadir el partido si no hay 11 jugadores
                                print(f"\n{bcolors.FAIL}[OMITIENDO]{bcolors.ENDC} {e} (ID: {fdmatch.fd_id})")
                                to_txt("corrupt_matches.log", fdmatch)
                                partidos_omitidos += 1
                            except IntegrityError as e:
                                # Tal vez haya un conflicto entre ids, lo omitimos
                                print(f"\n{bcolors.FAIL}[OMITIENDO]{bcolors.ENDC} {e} (ID: {fdmatch.fd_id})")
                                partidos_omitidos += 1
                            except OperationalError as e:
                                # No existe matchday por ejemplo
                                print(f"\n{bcolors.FAIL}[OMITIENDO]{bcolors.ENDC} {e} (ID: {fdmatch.fd_id})")
                                partidos_omitidos += 1
                            except Exception as e:
                                print(f"\n{bcolors.FAIL}[!]{bcolors.ENDC} {e} (ID: {fdmatch.fd_id})")
                                raise e
                        print(f"\n\n{bcolors.OKBLUE}[*]{bcolors.ENDC} "
                              f"Partidos correctos: {round(100 * partidos_correctos / j['count'], 2)}%; "
                              f"Partidos omitidos: {round(100 * partidos_omitidos / j['count'], 2)}%")
                    print("\nEliminando archivo leído")
                    os.remove(archivo)
        print()

    def get_season(self, season: str, process=False):
        """
        Dada una temporada, descarga el json correspondiente
        de todos los partidos finalizados de dicha temporada,
        siempre que este disponible

        Parameters
        ----------
        season: str
            Año de la temporada
        process: bool
            Crear las instancias de los partidos para
            devolverlos en una lista
        """

        print(f"\n{'*' * 86}\n*{'FootballData --> JSON':^84}*\n{'*' * 86}")

        # Parsear la temporada
        if re.match(r'^[0-9]{2}$', str(season)):
            # Formato yy
            season = f"20{season}"

        elif re.match(r'^[0-9]{2}-[0-9]{2}$', str(season)):
            # Formato yy-yy
            season = f"20{str(season)[:2]}"

        elif re.match(r'^20[0-9]{2}$', str(season)):
            # Format yyyy
            pass

        partidos = []
        for c in FootballData.competitions:
            comp = comp_info(c)
            print(bcolors.OKBLUE + "[MATCHES]" + bcolors.ENDC + f" Solicitando temporada {season} de: "
                                                                f"{comp['name']} ({comp['code']})")

            url = f'/v2/competitions/{c}/matches?season={season}&status=FINISHED'
            try:
                file_path = FootballData.req_man.request(url, silence=True)

                # Process matches
                if process and file_path:
                    with open(os.path.join(self.config['PATHS']['json_dir'], file_path + '.json'), 'rb') as f:
                        j = orjson.loads(f.read())
                    if j['matches']:
                        for i, match in enumerate(j['matches'], start=1):
                            fdmatch = FDMatch()
                            self.progress_bar(f"C{c}", 'Creando los partidos', i, len(j['matches']))

                            try:
                                fdmatch.load(match, competition_id=j['competition']['id'])
                                partidos.append(fdmatch)
                            except FDException as e:
                                print(f"\n{bcolors.FAIL}[MATCHES]{bcolors.ENDC} {e}")
                        print()
            except RestrictedResourceException as e:
                print(e)

        print()

        return partidos

    def get_starting_lineup(self, remove=False):
        """
        Descarga la alineación inicial de los partidos disponibles
        desde el día de hoy a 5 días vista

        Parameters
        ----------
        remove: bool
            Eliminar el archivo una vez procesado

        Returns
        -------
        list:
            Lista con los equipos que disputan el partido
        """

        print(f"\n{'*' * 86}\n*{'FootballData --> JSON':^84}*\n{'*' * 86}")

        dateFrom = (date.today() - timedelta(days=7)).strftime("%Y-%m-%d")
        dateTo = (date.today() + timedelta(days=7)).strftime("%Y-%m-%d")

        alineaciones = []
        for c in FootballData.competitions:
            comp = comp_info(c)
            print(bcolors.OKBLUE + "[LINEUP]" + bcolors.ENDC + f" Solicitando alineaciones de: "
                                                               f"{comp['name']} ({comp['code']})")

            url = f'/v2/competitions/{c}/matches?dateFrom={dateFrom}&dateTo={dateTo}'
            file_path = FootballData.req_man.request(url, silence=True)
            # file_path = f'/home/david/git/Football-api/adf/db/json/{url.replace("/", "_").replace("&", "_")}'

            if file_path:
                with open(file_path + '.json', 'rb') as f:
                    j = orjson.loads(f.read())
                if j['matches']:
                    for i, match in enumerate(j['matches'], start=1):
                        fdmatch = FDMatch()
                        self.progress_bar(fdmatch.season_short, 'Buscando partidos', i, len(j['matches']))
                        try:
                            fdmatch.load(match, competition_id=j['competition']['id'])
                            alineaciones.append(fdmatch)
                        except FDException as e:
                            print(f"\n\t{bcolors.FAIL}[LINEUP]{bcolors.ENDC} {e}")
                    print()

            if remove:
                os.remove(file_path + '.json')

        print()

        # Ordenar por fecha
        alineaciones.sort(key=lambda x: x.datetime)

        return alineaciones


def main(args=None, parser=None):
    if not args and not parser:
        parser = argparse.ArgumentParser(description="Módulo dedicado a solicitar temporadas, partidos y alineaciones,"
                                                     "analizarlas y actualizar los datos en la base de datos.")

        # Football Data
        # Optionals
        fd_parser = parser.add_argument_group(title="[B] Football Data options",
                                              description='Football Data optional arguments')
        fd_parser.add_argument('-s', '--season', help='Season to download', dest='season', type=int)
        fd_parser.add_argument('-d', '--fd-to-db',
                               help='Save downloaded JSON FootballData to database',
                               dest='df_to_db', action='store_true', default=False)
        fd_parser.add_argument('-n', '--line-up', help='Download scheduled lineups',
                               dest='line_up', action='store_true')

        args = parser.parse_args()

    if args.season or args.df_to_db or args.line_up:
        fd = FootballData()
        if args.season:
            fd.get_season(args.season, process=False)

        if args.df_to_db:
            fd.fd2db()

        # if args.line_up:
        #     fd.get_starting_lineup(remove=True)


if __name__ == "__main__":
    main()
