#!/usr/bin/env python

"""
Módulo encargado de utilizar los modelos aprendidos para
realizar las predicciones sobre nuevas alineaciones
disponibles
"""

import argparse
import configparser
import os
import re

import joblib
import numpy as np
import orjson
import pandas as pd
from tensorflow.keras.models import load_model

from footballapi.colors import bcolors
from footballapi.utils import to_ftp

# Supress tf logging
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.7"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"

# Archivo de configuración
config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
config.read(os.path.join(path, 'settings.cfg'))


def resumir_resultado(summary):
    """
    Crea el resumen del resultado

    Parameters
    ----------
    summary: dict
    """

    for s in summary['predicts']:
        if summary['num_res'] == 2:
            res = f"{str(s['result']).replace('1', 'X2').replace('0', '1')}"
        else:
            res = f"{str(s['result']).replace('1', 'X').replace('0', '1')}"

        print(bcolors.OKBLUE + f"[J{s['jornada']}] " + bcolors.ENDC + f"{s['homeTeam']} - {s['awayTeam']}: {res}")


def print_report(summary):
    print_media = lambda i: f"{round(mean[i] * 100, 2)}%"
    print_std = lambda i: f"(±{round(std[i] * 100, 2)}%)"
    for match in summary['predicts']:
        mean = match['stats']['mean']
        std = match['stats']['std']

        if summary['num_res'] == 2:
            cabecera = f"║ {'1':^32}    {'X2':^32} ║"
            media_str = f"║ {print_media(0):^34}{print_media(1):^34} ║"
            std_str = f"║ {print_std(0):^34}{print_std(1):^34} ║"
            pred = f"{str(match['stats']['predict']).replace('1', 'X2').replace('0', '1')}?"
            equipos = f"{match['homeTeam']:^34}{match['awayTeam']:^34}"
        else:
            cabecera = f"║ {'1':^22} {'X':^22} {'2':^22} ║"
            media_str = f"║ {print_media(0):^22} {print_media(1):^22} {print_media(2):^22} ║"
            std_str = f"║ {print_std(0):^22} {print_std(1):^22} {print_std(2):^22} ║"
            pred = f"{str(match['stats']['predict']).replace('1', 'X').replace('0', '1')}? "
            equipos = f"{match['homeTeam']:^22} {'':^22} {match['awayTeam']:^22}"

        print(f"\n╔{'═' * 28}╣ PREDICCIÓN ╠{'═' * 28}╗\n"
              # f"║ {' ' * 26} Temporada: {temporada} {' ' * 25} ║\n"
              f"║ {' ' * 28} Jornada: {match['jornada']:2} {' ' * 27} ║\n"
              f"║ {' ' * 26} Predicción: {pred:^4}{' ' * 26}║\n"
              f"╠{'═' * 70:70}╣\n"
              f"║ {equipos} ║\n"
              f"║{' ':70}║\n"
              f"{cabecera}\n"
              f"{media_str}\n"
              f"{std_str}\n"
              f"╚{'═' * 70}╝\n")


def predict(fd_id_equipo: str):
    summary = {
        'predicts': [],
        'matches': 0,
    }
    features_columns = [
        "L_O", "L_T", "L_MO", "L_POT", "L_ME", "L_DE", "L_P", "V_O", "V_T", "V_MO", "V_POT", "V_ME", "V_DE", "V_P"
    ]
    regex = re.compile(
        "predict\-([0-9]{4}\-[0-9]{2}\-[0-9]{2}_[0-9]{2}\:[0-9]{2}\:[0-9]{2})\-([0-9]+)-(.+)\-(.+)\-J([0-9]{1,2}|None)\.(csv|npy)")

    files = list(filter(lambda x: regex.search(x), os.listdir(config['PATHS']['predict_dir'])))
    if files:
        # Buscar los modelos clásicos
        ml2_regex = re.compile(f"((_?[A-Z][a-z]*)+)_([.0-9]+)_{fd_id_equipo}_2\.joblib")
        ml3_regex = re.compile(f"((_?[A-Z][a-z]*)+)_([.0-9]+)_{fd_id_equipo}_3\.joblib")
        ml2 = list(filter(lambda x: ml2_regex.search(x), os.listdir(config['PATHS']['prod'])))
        ml3 = list(filter(lambda x: ml3_regex.search(x), os.listdir(config['PATHS']['prod'])))
        ml2_names = list(map(lambda x: ml2_regex.search(x).group(1).replace('_', ' '), ml2))
        ml3_names = list(map(lambda x: ml3_regex.search(x).group(1).replace('_', ' '), ml3))
        ml2_acc = list(map(lambda x: ml2_regex.search(x).group(3).replace('_', ' '), ml2))
        ml3_acc = list(map(lambda x: ml3_regex.search(x).group(3).replace('_', ' '), ml3))

        # Cargar los modelos
        ml2 = list(map(lambda x: joblib.load(os.path.join(config["PATHS"]["prod"], x)), ml2))
        ml3 = list(map(lambda x: joblib.load(os.path.join(config["PATHS"]["prod"], x)), ml3))

        assert len(ml2) and len(ml3), "No hay modelos clásicos disponibles"

        # Buscar los modelos profundos
        dl2_regex = re.compile(f"(.+)_([.0-9]+)_{fd_id_equipo}_2\.h5")
        dl3_regex = re.compile(f"(.+)_([.0-9]+)_{fd_id_equipo}_3\.h5")
        dl2 = list(filter(lambda x: dl2_regex.search(x), os.listdir(config['PATHS']['prod'])))
        dl3 = list(filter(lambda x: dl3_regex.search(x), os.listdir(config['PATHS']['prod'])))
        dl2_names = list(map(lambda x: dl2_regex.search(x).group(1).replace('_', ' '), dl2))
        dl3_names = list(map(lambda x: dl3_regex.search(x).group(1).replace('_', ' '), dl3))
        dl2_acc = list(map(lambda x: dl2_regex.search(x).group(2).replace('_', ' '), dl2))
        dl3_acc = list(map(lambda x: dl3_regex.search(x).group(2).replace('_', ' '), dl3))

        # Cargar los modelos
        dl2 = list(map(lambda x: load_model(os.path.join(config["PATHS"]["prod"], x)), dl2))
        dl3 = list(map(lambda x: load_model(os.path.join(config["PATHS"]["prod"], x)), dl3))

        assert len(dl2) and len(dl3), "No hay modelos profundos disponibles"

        for f in files:
            datefull, competition, local_name, visitante_name, matchday, ftype = regex.search(f).groups()

            # Cargar las variables predictoras
            if ftype == "csv":
                df = pd.read_csv(os.path.join(config['PATHS']['predict_dir'], f), index_col=0)
                X = df[features_columns].values
            elif ftype == "npy":
                path = os.path.join(config['PATHS']['predict_dir'], f)
                x = np.load(path)
                X = np.reshape(x, (1, *x.shape, 1))
            else:
                raise ValueError(f"La extensión {ftype} no se reconoce")

            # Buscar el partido en el sumario
            matches = list(filter(lambda x: (x['homeTeam'], x['awayTeam'], x['date']) == (
                local_name.replace('_', ' '), visitante_name.replace('_', ' '), datefull.replace('_', ' ')),
                                  summary['predicts']))
            if not matches:
                match = {
                    'homeTeam': local_name.replace('_', ' '),
                    'awayTeam': visitante_name.replace('_', ' '),
                    'competition': competition,
                    'result': {"2": [], "3": []},
                    'date': datefull.replace('_', ' '),
                    'jornada': matchday,
                }
                # Añadimos el partido
                summary['predicts'].append(match)
                summary['matches'] += 1

            else:
                match = matches[0]

            if ftype == "csv":
                # Realizar la predicción para 2 resultados con modelos clásicos
                for model_name, model, acc in zip(ml2_names, ml2, ml2_acc):
                    y_pred = model.predict(X)
                    match['result']["2"].append({
                        "model": model_name,
                        "result": int(y_pred[0]),
                        "accuracy": float(acc),
                    })

                # Realizar la predicción para 3 resultados con modelos clásicos
                for model_name, model, acc in zip(ml3_names, ml3, ml3_acc):
                    y_pred = model.predict(X)
                    match['result']["3"].append({
                        "model": model_name,
                        "result": int(y_pred[0]),
                        "accuracy": float(acc),
                    })

            elif ftype == "npy":
                # Realizar la predicción para 2 resultados con modelos profundos
                for model_name, model, acc in zip(dl2_names, dl2, dl2_acc):
                    y_pred = model.predict(X)
                    match['result']["2"].append({
                        "model": model_name,
                        "result": int(np.argmax(y_pred)),
                        "accuracy": float(acc),
                    })

                # Realizar la predicción para 3 resultados con modelos profundos
                for model_name, model, acc in zip(dl3_names, dl3, dl3_acc):
                    y_pred = model.predict(X)
                    match['result']["3"].append({
                        "model": model_name,
                        "result": int(np.argmax(y_pred)),
                        "accuracy": float(acc),
                    })

    return summary


def main(args=None, parser=None):
    if not args and not parser:
        parser = argparse.ArgumentParser(
            description="Genera la predicción para los archivos (alineaciones) que esten preparados para la predidcción"
                        f"en el directorio {config['PATHS']['predict_dir']}")
        predictor_parser = parser.add_argument_group(title="Predictor", description='Argumentos para la predicción')
        predictor_parser.add_argument('--team-fd-id', help='Team FootballData id', dest='team_fd_id', default='all')
        predictor_parser.add_argument('-ftp', help="Upload predictions to FTP server", dest='ftp',
                                      action='store_true', default=False)
        args = parser.parse_args()
    # if args.dl or args.ml:
    print(f"\n{'*' * 86}\n*{'.joblib/.h5 --> FTP/$$$':^84}*\n{'*' * 86}")

    summary = predict(args.team_fd_id)

    # Sort by accuracies
    for match in summary['predicts']:
        # Sort 2 results
        match['result']["2"].sort(key=lambda x: x['accuracy'], reverse=True)
        # Sort 3 results
        match['result']["3"].sort(key=lambda x: x['accuracy'], reverse=True)

    # Sort by date
    summary['predicts'].sort(key=lambda x: x['date'], reverse=True)

    # Export to json
    with open("summary.json", "wb") as f:
        f.write(orjson.dumps(summary))

    if args.ftp:
        # Upload predictions to FTP server
        to_ftp(f"summary.json", f"summary.json")


if __name__ == '__main__':
    main()
