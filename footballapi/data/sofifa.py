#!/usr/bin/env python

import argparse
import configparser
import datetime
import os
import re
import sys
import webbrowser
from difflib import SequenceMatcher

import orjson
from googleapiclient.discovery import build
from lxml import html
from lxml.etree import ParserError
from requests import Session
from ruia import TextField, Item, Spider
from unidecode import unidecode

from footballapi.data.Singleton import Singleton
from footballapi.colors import bcolors
from footballapi.data.sofifadb import SoFifaDB

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.7"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"


class Player(Item):
    # Define XPaths
    root = "//div[@class='columns']/div"
    sofifa_id_xpath = f'{root}/div[@class="bp3-card player"]/div[@class="info"]'
    name_xpath = f'{root}/div[@class="bp3-card player"]/div[@class="info"]/h1/text()'
    height_xpath = f'{root}/div[@class="bp3-card player"]/div[@class="info"]/div[@class="meta bp3-text-overflow-ellipsis"]'
    weight_xpath = f'{root}/div[@class="bp3-card player"]/div[@class="info"]/div[@class="meta bp3-text-overflow-ellipsis"]'
    current_position_xpath = f'{root}/div[@class="bp3-card player"]/div[@class="info"]/div[@class="meta bp3-text-overflow-ellipsis"]/span[contains(@class, "pos pos")]'
    positions_xpath = f'{root}/div[@class="bp3-card player"]/div[@class="info"]/div[@class="meta bp3-text-overflow-ellipsis"]/span[contains(@class, "pos pos")]'
    specific_stats_xpath = f'{root}/div[@class="bp3-card double-spacing"]/ul/li/span[contains(@class, "bp3-tag") and number()]'
    generic_stats_xpath = f'{root}/div[@class="bp3-card player"]/section[@class="spacing"]/div[@class="columns"]/div/div/span[contains(@class, "bp3-tag") and text() and number()]'
    date_xpath = f'{root}/div[@class="bp3-card player"]/div[@class="info"]/h1/span[@class="bp3-tag bp3-minimal bp3-intent-success"]'

    target_item = TextField(xpath_select=f'{root}')
    sofifa_id = TextField(xpath_select=sofifa_id_xpath)
    name = TextField(xpath_select=name_xpath, )
    height = TextField(xpath_select=height_xpath, )
    weight = TextField(xpath_select=weight_xpath, )

    current_position = TextField(xpath_select=current_position_xpath, many=True)
    positions = TextField(xpath_select=positions_xpath, many=True)
    specific_stats = TextField(xpath_select=specific_stats_xpath, many=True)
    generic_stats = TextField(many=True, xpath_select=generic_stats_xpath)
    # roaster = AttrField(
    #     xpath_select='/html/body/header/nav/div/div[@class="bp3-navbar-group bp3-align-left"]/form[@class="bp3-input-group pjax-form"]/input',
    #     attr='data-roaster')
    date = TextField(xpath_select=date_xpath, )

    async def clean_sofifa_id(self, value):
        try:
            regex = int(re.search(r'[0-9]{2,7}', value).group())
        except AttributeError:
            regex = ""
        finally:
            return regex

    async def clean_name(self, value):
        try:
            regex = re.search('([A-Z][a-z]+ )+', unidecode(value)).group().strip()
        except AttributeError:
            regex = None
        finally:
            return regex

    async def clean_height(self, value):
        try:
            return int(re.search(r'([0-9]{3})cm', value).group(1))
        except AttributeError:
            return '?'

    async def clean_weight(self, value):
        try:
            return int(re.search(r'([0-9]{2,3})kg', value).group(1))
        except AttributeError:
            return '?'

    async def clean_current_position(self, value):
        assert len(value) > 0, f"Player current position not found: {value}"
        return value[0]

    async def clean_positions(self, value):
        if len(value) > 1:
            return value[1:]
        else:
            return []

    async def clean_specific_stats(self, value):
        stats = [int(item) for item in value if type(item) is not list]

        # assert len(stats) == 34, f"Specific stats length error: {stats}"
        if len(stats) != 34:
            raise InconsistentStatsLengthException(f"length: {len(stats)} != 34")
        s = sum(1 for n in stats if n < 0)
        if s != 0:
            raise InconsistentStatsLengthException(f"sum: {s} != 0")

        return stats

    async def clean_generic_stats(self, value):
        stats = [int(item) for item in value if type(item) is not list]

        assert len(stats) == 2, f"Generic stats length error: {stats}"
        assert sum(1 for n in stats if n < 0) == 0

        return stats

    # async def clean_roaster(self, value):
    #     # TODO: Convertir la fecha en roaster
    #     return value

    async def clean_date(self, value):
        return re.search(r'FIFA [0-9]{2} (.*)', value).group(1)


class SoFifaSpider(Spider):
    name = "SoFifa"
    start_urls = []
    _sofifadb = SoFifaDB()
    worker_numbers = 1

    async def parse(self, response):
        try:
            async for item in Player.get_items(html=response.html):
                yield item
        except InconsistentStatsLengthException as e:
            # print(f"\n{e}")
            raise e
        except Exception as e:
            print(f"\n{e}")

    async def process_item(self, item: Player):
        # Save into the database
        print(f"\r{bcolors.OKBLUE}[SOFIFA RUIA]{bcolors.ENDC} Saving {item.name} ({item.sofifa_id}) "
              f"with date {item.date}", end="")
        SoFifaSpider._sofifadb.insertar_jugador(item.results)


class SofifaPlayerNotFoundException(Exception):
    """Un jugador no existe en Sofifa"""
    pass


class InconsistentStatsLengthException(Exception):
    """Los atributos no tienen 34 características o alguna es negativa"""
    pass


class SoFifa(metaclass=Singleton):
    history_urls = None
    history_updated = False

    def __init__(self, update_history=False):
        self._session = Session()
        self._sofifadb = SoFifaDB()

        # Archivo de configuración
        self.config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
        path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
        self.config.read(os.path.join(path, 'settings.cfg'))

        # Selenium driver
        self.driver = None

        self.update_history = update_history

        # Google Custom Search Engine
        self.cse_service = None

    def __del__(self):
        if self.driver:
            self.driver.close()

    def _request(self, url, extra_params=True):
        """
        Solicita la página web de sofifa de un jugador, un equipo o una liga.

        :param url:
        :param extra_params: Mandar la solicitud con parámetros extra,
        como el idioma y el tipo de unidades.
        :return: str html
        """

        # try:
        #     file_name = f'{re.search(r"r=([0-9]+)", url).group(1)}.html' if url.find('r=') > 0 else url.split('/')[-1]
        #     file_path = os.path.join(self.config['PATHS']['db_dir'], 'player', re.search(r"([0-9]+)", url).group(1))
        # except AttributeError:
        #     file_name, file_path = '', ''

        # if os.path.exists(os.path.join(file_path, file_name)) and not force:
        #     response = SoFifa._open_saved_data(os.path.join(file_path, file_name))

        # else:
        full_url = f"{self.config['SOFIFA']['sf_request_url']}/{url}"

        if extra_params:
            full_url = f"{full_url}{self.config['SOFIFA']['http_params']}"

        # Creamos una sesión en vez de solicitar la página directamente
        # para que se carguen los javascripts ajax y toda esa mierda
        self._session.head(full_url)
        r = self._session.post(full_url)

        if r.status_code == 200:
            # Ok
            # self._save_response(r.text, file_path, file_name)
            response = r.text
            # print(f"{bcolors.OKBLUE}[SOFIFA]{bcolors.ENDC} {full_url} successfully obtained.")
        else:
            # Unknown error
            print(f"{bcolors.FAIL}[SOFIFA]{bcolors.ENDC} Unknown error ocurred: {r.status_code}: {r.reason}")
            sys.exit(r.status_code)

        return response

    def update_history_items(self, save=False):
        """
        Busca en el histórico de sofifa a ver si hay nuevos items para descargar.
        """
        urls = ["?r=180084&set=true", "?r=190075&set=true", "?r=200001&set=true"]
        self.history_urls = {"roasters": []}

        for url in urls:
            response_text = self._request(url, extra_params=False)

            try:
                tree = html.fromstring(response_text)
            except ParserError as e:
                # Es posible que response_text este vacío por cualquier motivo
                print(f"{bcolors.FAIL}[SOFIFA]{bcolors.ENDC} "
                      f"No se ha podido analizar el html, parece que el html estaba vacío.")
                raise e

            # Obtener la fecha y el roaster
            for element in tree.xpath('//div[@class="dropdown"]/div[@class="bp3-menu"]/a'):
                fecha = re.match(r"[a-zA-Z]{2,4} [0-9]{1,2}, [0-9]{4}", element.xpath('text()')[0])
                if fecha:
                    fecha = fecha.group()
                    roaster = re.search(r"r=([0-9]+)", element.xpath('@href')[0]).group(1)

                    # Admitir sólo temporadas 17-18 o superiores
                    if roaster >= '180001':
                        self.history_urls["roasters"].append((roaster, fecha))

        assert len(self.history_urls['roasters']) > 0, "Revisa el método 'update_history_items()', parece que el " \
                                                       "XPath para buscar los históricos de Sofifa ha cambiado"

        # Ordenar por roaster
        self.history_urls['roasters'].sort(key=lambda x: datetime.datetime.strptime(x[1], "%b %d, %Y"), reverse=True)

        # Guardar los nuevos roasters
        if save:
            with open(self.config['PATHS']['history_file'], "wb") as history:
                history.write(orjson.dumps(self.history_urls))

        self.history_updated = True
        return self.history_urls

    def _get_players_history_url(self):
        """
        Accede al archivo history.json y lo carga en memoria si no se ha cargado ya
        """
        if self.update_history and not self.history_updated:
            self.update_history_items()
        else:
            if not self.history_urls:
                with open(self.config['PATHS']['history_file'], "rb") as history:
                    data = orjson.loads(history.read())

                self.history_urls = data

    def get_player(self, players, roasters=None, history_items=1, from_roaster=0, force=False):
        """
        Guarda en la base de datos el jugador dado. Si se especifica
        history_items, se guardarán los últimos items disponibles, sino
        únicamente el último disponible en history.json

        Parameters
        ----------
        players: str, lst
            Sofifa Id o lista con los ids de los jugador(es)
            que se desea descargar
        roasters: lst
            Roasters específicos a descargar. Invalida a history_items
            y a from_roasters. Además, se fuerza la descarga.
        history_items: int
            Número de items a descargar desde la fecha de hoy hacia atrás.
        from_roaster: int
            Descargar desde este roaster en adelante.
        force: Bool
            Forzar la descarga
        """

        if type(players) not in (list, tuple):
            players = [players]

        if not roasters:
            # Mismo roaster para todos los players
            for player in players:
                print(f"{bcolors.OKBLUE}[SOFIFA]{bcolors.ENDC} Getting '{player}' player")
                if type(player) is not int:
                    assert player.isdigit(), f"El id del jugador no esta compuesto únicamente por números: {player}"
                url = f'player/{player}'

                # Carga el archivo history.json en memoria
                self._get_players_history_url()

                # Calcular los roasters disponibles
                roasters = [(r, d) for (r, d) in self.history_urls['roasters'] if int(r) >= int(from_roaster)]
                roasters.sort(key=lambda x: x[0], reverse=not bool(from_roaster))

                # Calcular el número máximo de items que se pueden descargar
                if history_items > len(roasters) or history_items == 0:
                    history_items = len(roasters)

                # Guardar los urls de descarga
                for i in range(history_items):
                    roaster, date = roasters[i]
                    if force or not self._sofifadb.contains_player_stats(player, date):
                        historical_url = f"{self.config['SOFIFA']['sf_request_url']}/{url}/{roaster}/?units=mks"

                        # Añadir la url al spider
                        SoFifaSpider.start_urls.append(historical_url)
        else:
            # Un roaster para cada player
            assert len(roasters) == len(players), "No hay el mismo número de roasters que de players"
            # Carga el archivo history.json en memoria
            self._get_players_history_url()

            for player, roaster in zip(players, roasters):
                if type(player) is not int:
                    assert player.isdigit(), f"El id del jugador no esta compuesto únicamente por números: {player}"
                url = f'player/{player}'

                historical_url = f"{self.config['SOFIFA']['sf_request_url']}/{url}/{roaster}/?units=mks"
                # Añadir la url al spider
                SoFifaSpider.start_urls.append(historical_url)

        # Ejecutar el spider para guardar
        if len(SoFifaSpider.start_urls) > 0:
            SoFifaSpider.start()
            SoFifaSpider.start_urls = []

    def find_id_of(self, name, equipo="", allow_input=True):
        """
        Busca el nombre del jugador dado en Sofifa y devuelve su id.

        Parameters
        ----------
        name
        equipo: str
        allow_input: bool
            Habilita la opción para introducir los ids a mano.
            Puede ser útil en caso de ejecutarse trás un crond
            en el que no pueda haber intervención del usuario,
            en cuyo caso saltará una excepción.

        Returns
        -------
        int
            Id de sofifa
        """

        search_url = f"players?keyword={name.replace(' ', '%20')}"
        response_text = self._request(search_url, extra_params=False)

        # Scrap to find team url
        tree = html.fromstring(response_text)
        raw_players = tree.xpath(
            '//table[@class="table table-hover persist-area"]/tbody/tr')
        players = [
            (p.xpath('td[@class="col-name"]/div[@class="bp3-text-overflow-ellipsis"]/a/text()'),  # Nombre y equipo
             p.xpath('td[@class="col-name"]/div[@class="bp3-text-overflow-ellipsis"]/a[@class="nowrap"]/@href')[0])
            for p in raw_players]

        # Filtrar por equipo
        if equipo != "":
            players = list(filter(lambda x: SequenceMatcher(None, x[0][1], equipo).ratio() >= 0.5, players))

        # Mostrar los items recopilados
        for i in range(len(players)):
            print(f'[{i}] {players[i][0][0]} ({players[i][0][1]}) -> {players[i][1]}')

        if len(players) > 1:
            # Si hay más de 1 item, dar la opción a escoger
            webbrowser.open_new_tab(f"{self.config['SOFIFA']['sf_request_url']}/{search_url}")

            if allow_input:
                try:
                    index = int(input(f'\n> Select: '))
                    while not 0 <= index <= len(players):
                        index = int(input(f'\n> Select: '))
                except ValueError:
                    print("\n")
                    print(f"{bcolors.FAIL}[SOFIFA]{bcolors.ENDC} No se ha especificado ningún jugador!")
                    s = input("Introduzca su sofifa_id a mano: ")
                    if 4 < len(s) < 7:
                        return int(s)
                    return None

                # print("\n")

                # Scrap item's url
                # url, _ = self._get_url(players[index][1])
                # item_id = url.split('/')[2]
                item_id = players[index][1].split('/')[2]
            else:
                raise SofifaPlayerNotFoundException(f"No se encuentra el jugador en Sofifa: {name} ({equipo})")

        elif len(players) == 1:
            print(f"{bcolors.OKBLUE}[SOFIFA]{bcolors.ENDC} "
                  f"Elegido {players[0][0][0]} ({players[0][0][1]}) -> {players[0][1]}")
            item_id = players[0][1].split('/')[2]
        else:
            # item_id = self.find_id_selenium(name, equipo=equipo)
            item_id = self.find_id_on_google(name, equipo=equipo, allow_input=allow_input)
            if allow_input:
                if not item_id:
                    print(f"{bcolors.WARNING}[SOFIFA]{bcolors.ENDC} "
                          f"No se ha podido encontrar el jugador {name}. Búsquelo manualmente para poder "
                          f"seguir en:\n\t-> {self.config['SOFIFA']['sf_request_url']}/{search_url}")
                    webbrowser.open_new_tab(f"{self.config['SOFIFA']['sf_request_url']}/{search_url}")

                    item_id = None
                    while not item_id:
                        try:
                            item_id = int(input("\nIntroduzca el id: "))
                        except ValueError:
                            pass
            else:
                raise SofifaPlayerNotFoundException(f"No se encuentra el jugador en Sofifa: {name} ({equipo})")

        return item_id

    def find_id_on_google(self, name, equipo="", allow_input=True):
        """
        Busca el id del jugador buscando en Google

        Parameters
        ----------
        name: str
            Nombre del jugador buscado
        equipo: str
            Nombre del equipo del jugador
        allow_input: bool
            Habilita la opción para introducir los ids a mano.
            Puede ser útil en caso de ejecutarse trás un crond
            en el que no pueda haber intervención del usuario,
            en cuyo caso saltará una excepción.

        Returns
        -------
        int
            Sofifa id del jugador

        See Also
        --------
        Google Custom Engine: https://stackoverflow.com/a/37084643
        """
        # Enable CSE
        if not self.cse_service:
            assert os.environ['cse_key'], "Variable de entorno cse_key no definida"
            self.cse_service = build("customsearch", "v1", developerKey=os.environ['cse_key'])
        assert os.environ['cse_id'], "Variable de entorno cse_key no definida"

        id_regex = re.compile(r"player/([0-9]{5,7})")
        # name_regex = re.compile('(.+) FIFA [0-9]+ [a-zA-Z]+ [0-9]+, [0-9]{4} SoFIFA')
        print(f"\r{bcolors.OKBLUE}[GOOGLE]{bcolors.ENDC} Deep search of: {name} ({equipo})", end="")

        # Realizar la búsqueda
        res = self.cse_service.cse().list(
            q=f"{unidecode(name)} {unidecode(equipo)}", cx=os.environ['cse_id']).execute()

        if 'items' in res:
            # Filtrar resultados
            urls = list(filter(lambda x: id_regex.search(x['link']), res['items']))
        else:
            urls = None

        if not urls:
            # Si no encuentra nada, buscar sólo por nombre
            # Realizar la búsqueda
            res = self.cse_service.cse().list(q=f"{unidecode(name)}", cx=os.environ['cse_id']).execute()
            # Filtrar resultados
            if 'items' in res:
                urls = list(filter(lambda x: id_regex.search(x['link']), res['items']))

        if not urls:
            webbrowser.open_new_tab(f"https://cse.google.com/cse?cx={os.environ['cse_id']}")
            try:
                if allow_input:
                    sofifa_id = int(input(f"Introduce manualmente el id de {name} ({equipo}): "))
                else:
                    raise SofifaPlayerNotFoundException(f"No se encuentra el jugador en Sofifa: {name} ({equipo})")
            except Exception:
                raise SofifaPlayerNotFoundException(f"No se encuentra el jugador en Sofifa: {name} ({equipo})")
        else:
            sofifa_id = id_regex.search(urls[0]['link']).group(1)
            print(f" -> {sofifa_id}")

        return sofifa_id


def main(args=None, parser=None):
    if not args and not parser:
        parser = argparse.ArgumentParser(description="SoFifa crawler.")

        # SoFifa parser
        # Required
        sofifa_parser = parser.add_argument_group(title="[A] SoFifa", description='SoFifa crawler required arguments')
        sofifa_required = sofifa_parser.add_mutually_exclusive_group()
        sofifa_required.add_argument('-p', '--player', help='Player name or SoFifa id', dest='player')

        # Optionals
        sofifa_parser_optionals = parser.add_argument_group(title="[A] SoFifa options",
                                                            description='SoFifa crawler optional arguments')
        sofifa_parser_optionals.add_argument('-i', '--history-items',
                                             help='Get last <n> versions data of player. (Default: 1)',
                                             dest='history_items', type=int, default=1)
        sofifa_parser_optionals.add_argument('-u', '--update-history', help='Update history items. (Default: false)',
                                             dest='update_history', action='store_true', default=False)
        sofifa_parser_optionals.add_argument('-f', '--force', help="Force scraping data saving", default=False,
                                             dest='force', action='store_true')

        args = parser.parse_args()

    if args.player or args.update_history:
        if True or args.async_mode:
            # Asynchronous mode
            sf = SoFifa(update_history=args.update_history)

            if args.update_history:
                # Update history items
                sf.update_history_items(save=True)

            if args.player:
                # Players
                sf.get_player(args.player, history_items=args.history_items, force=args.force)


if __name__ == "__main__":
    # main()
    # sys.exit(0)

    sf = SoFifa()
    sf.get_player('158023', force=True)
    # sf.find_id_on_google("Benjamin Mee")
    # sf.find_id_on_google("Dejan Kulusekvski")
    # sf.find_id_on_google("Robin van Persie")
