#!/usr/bin/env python

"""
Módulo principal desde dónde lanzar el proyecto
"""

import argparse

import sys

from DataAnalysis import DataAnalysis
from FootballData import main as fd_main
from prediction import main as pred_main
from sofifa import main as sofifa_main

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.7"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="If many options passed, execution will be in order: SoFifa > Football Data > Data Analysis")

    # SoFifa parser
    # Required
    sofifa_parser = parser.add_argument_group(title="[A] SoFifa",
                                              description='SoFifa crawler required arguments')
    sofifa_required = sofifa_parser.add_mutually_exclusive_group()
    sofifa_required.add_argument('-p', '--player', help='Player name or SoFifa id', dest='player')

    # Optionals
    sofifa_parser_optionals = parser.add_argument_group(title="[A] SoFifa options",
                                                        description='SoFifa crawler optional arguments')
    sofifa_parser_optionals.add_argument('-i', '--history-items',
                                         help='Get last <n> versions data of player. (Default: 1)',
                                         dest='history_items', type=int, default=1)
    sofifa_parser_optionals.add_argument('-u', '--update-history', help='Update history items. (Default: false)',
                                         dest='update_history', action='store_true', default=False)
    sofifa_parser_optionals.add_argument('-f', '--force', help="Force scraping data saving", default=False,
                                         dest='force', action='store_true')

    # Football Data
    # Optionals
    fd_parser = parser.add_argument_group(title="[B] Football Data options",
                                          description='Football Data optional arguments')
    fd_parser.add_argument('-s', '--season', help='Season to download', dest='season', type=int)
    fd_parser.add_argument('-d', '--fd-to-db',
                           help='Save downloaded JSON FootballData to database',
                           dest='df_to_db', action='store_true', default=False)

    # Data Analysis
    # Required
    da_parser = parser.add_argument_group(title="[C] Data Analysis", description="Data Analysis required arguments")
    da_parser.add_argument('-c', '--csv', help='Generate csv file with season data. \'all\' for all seasons',
                           dest='csv', type=str)

    # Optionals
    da_parser_optionals = parser.add_argument_group(title="[C] Data Analysis options",
                                                    description="Data Analysis optional arguments")
    da_parser_optionals.add_argument('-n', '--line-up', help='Line-up from season\'s matchday',
                                     dest='line_up', action='store_true')
    da_parser_optionals.add_argument('-nr', '--num_results', default=2, type=int,
                                     help='Number of results in class label, 2 (0: home win, 1: home doesn\'t win) or '
                                          '3 (0: home win, 1: draw, 2: away win) (Default: 2)', dest='num_results')
    da_parser_optionals.add_argument('--team-fd-id', help='Team FootballData id', dest='team_fd_id', type=str,
                                     default='all')
    # da_parser_optionals.add_argument('--agregation', dest='agregacion', default='shin_gasparyan',
    #                                  help="Agregation function to compute data (Default: shin_gasparyan)")
    da_parser_optionals.add_argument('--radicale', dest='radicale', action='store_true', default=False,
                                     help="Saves scheduled match in radicale server")
    da_parser_optionals.add_argument('--allow_input', dest="allow_input",
                                     help="Permitir la entrada por teclado, útil para servicios",
                                     action='store_true', default=False)

    # Predictions
    pred_parser = parser.add_argument_group(title='[D] Predictions',
                                            description="Genera la predicción para los archivos (alineaciones) que "
                                                        "esten preparados para la predicción")
    pred_parser.add_argument('--predict', help="Predict new outcomes", dest='predict', action='store_true',
                             default=False)
    pred_parser.add_argument('-r', '--report', help="Prints report", dest='report', action='store_true',
                             default=False)
    pred_parser.add_argument('-ftp', help="Upload predictions to FTP server", dest='ftp',
                             action='store_true', default=False)
    args = parser.parse_args()

    try:
        sofifa_main(args=args, parser=parser)
        fd_main(args=args, parser=parser)

        if args.csv or args.line_up:
            da = DataAnalysis(num_resultados=args.num_results, allow_input=args.allow_input)

            if args.line_up:
                da.exportar_prediccion(to_radicale=args.radicale)

            if args.csv:
                da.create_csv(args.csv, fd_id_equipo=args.team_fd_id)

        if args.predict:
            pred_main(args=args, parser=parser)

        sys.exit(0)

    except KeyboardInterrupt:
        print()
        print("Saliendo")
        sys.exit(1)

    except Exception as e:
        print()
        print("Ha habido algún error inesperado:\n\t--> {0}".format(e))
        sys.exit(1)
