#!/usr/bin/env python

"""
Módulo encargado de analizar los datos de la base de datos,
crear el conjunto de datos agregados Shin-Gasparyan y exportar
a CSV o .npz
"""

import configparser
import os
from datetime import datetime

import numpy as np
import pytz
from unidecode import unidecode

from DataRequest import DataRequest
from FootballData import FootballData, FDMatch
from footballapi.colors import bcolors
from footballapi.utils import parse_seasons
from sofifa import SoFifa
from sofifadb import SoFifaDB

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.7"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"


class DataAnalysis:

    def __init__(self, num_resultados=2, allow_input=True):
        """
        Clase encargada de realizar el análisis de datos.

        Parameters
        ----------
        num_resultados: int
            Número de clases que puede haber:
                - 2: 0 gana local, 1 en otro caso.
                - 3: 0 gana local, 1 empata y 2 gana visitante
        allow_input: bool
            Habilita la opción para introducir los ids a mano.
            Puede ser útil en caso de ejecutarse trás un crond
            en el que no pueda haber intervención del usuario,
            en cuyo caso saltará una excepción.
        """
        self._data_request = DataRequest()
        self._fd = FootballData()
        self._sf = SoFifa()
        self._sofifadb = SoFifaDB()

        self._num_resultados = num_resultados

        # Caldav stuff
        self.caldav_client = None
        self._main_cal = None

        # Archivo de configuración
        self.config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
        path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
        self.config.read(os.path.join(path, 'settings.cfg'))

        self.allow_input = allow_input

    def _init_caldav(self):
        """
        Inicializa la conexión con el servidor de calendario.
        """
        assert os.environ['CALDAV_PASSWD'], "Contraseña CALDAV no definida"
        import caldav

        caldav_client = caldav.DAVClient(self.config['CALDAV']['url'],
                                         username=self.config['CALDAV']['user'], password=os.environ['CALDAV_PASSWD'])
        principal = caldav_client.principal()
        self._main_cal = principal.calendars()

    def _to_caldav(self, jornada, fecha, homeTeam, awayTeam):
        """
        Añade al calendario un nuevo evento con el partido dado

        Parameters
        ----------
        jornada: int, str
            Jornada del encuentro
        fecha: datetime
            Fecha del encuentro
        homeTeam: str
            Nombre del equipo local
        awayTeam: str
            Nombre del equipo visitante

        """
        assert type(fecha) == datetime, "La fecha dada no es de tipo datetime"

        if not self._main_cal:
            self._init_caldav()

        # Añade el evento al calendario
        if len(self._main_cal) > 0:
            calendar = self._main_cal[0]

            tstamp = datetime.utcnow()
            summary = f"[J{jornada}] {homeTeam} - {awayTeam}"
            uid = f"{jornada}{homeTeam}{awayTeam}".lower().replace(' ', '')
            exists = False

            results = calendar.date_search(fecha.astimezone(pytz.utc), tstamp)
            if len(results) > 0:
                for result in results:
                    if result.url.find(uid) >= 0:
                        exists = True
                        break

            if not exists and datetime.today().astimezone(pytz.utc) < fecha.astimezone(pytz.utc):
                event = self.config['CALDAV']['event'].format(
                    uuid=uid,
                    tstamp=tstamp.astimezone(pytz.utc).strftime("%Y%m%dT%H%M%SZ"),
                    tstart=fecha.astimezone(pytz.utc).strftime("%Y%m%dT%H%M%SZ"),
                    summary=summary,
                )
                calendar.add_event(event)

    def _agregar_jugadores_masivamente(self, partidos):
        """
        Genera una lista con los ids de los jugadores de los partidos
        que aun no existan en la base de datos junto a su roaster
        correspondiente. De esta manera, se pueden descargar todos
        asíncronamente con Ruia.

        Parameters
        ----------
        partidos: list
            Lista con los partidos
        """
        print(f"{bcolors.OKBLUE}[+]{bcolors.ENDC} Comprobando que los jugadores esten en la base de datos. "
              f"Puede tardar un rato.")
        jugadores = []
        for i, partido in enumerate(partidos):
            fecha = partido['FECHA'].strftime("%Y-%m-%d")
            for tipo in ['LOCAL_ID', 'VISITANTE_ID']:
                try:
                    t = self._data_request.get_equipo_titular(partido['TEMPORADA'], partido['JORN'], partido[tipo])
                    jugadores.extend([(t['J0'], fecha), (t['J1'], fecha), (t['J2'], fecha),
                                      (t['J3'], fecha), (t['J4'], fecha), (t['J5'], fecha), (t['J6'], fecha),
                                      (t['J7'], fecha), (t['J8'], fecha), (t['J9'], fecha), (t['J10'], fecha)
                                      ])
                except IndexError as e:
                    print(f"{bcolors.FAIL}[!]{bcolors.ENDC} No existe el equipo titular: T{partido['TEMPORADA']}-"
                          f"{partido['JORN']}-{partido[tipo]}")
                    pass

        # Convertir el FD_ID a SOFIFA_ID
        jugadores = map(lambda x: (self._data_request.fdid2sofifaid(x[0], None, allow_input=self.allow_input),
                                   self._data_request.fecha_mas_proxima_a(x[1])), jugadores)
        # Filtrar los jugadores que esten ya en la base de datos
        sofifadb = SoFifaDB()
        jugadores = list(filter(lambda x: not sofifadb.contains_player_stats(x[0], x[1]), jugadores))

        if len(jugadores) > 0:
            jugadores, roasters = list(zip(*map(lambda x: (x[0], sofifadb.date2roaster(x[1])), jugadores)))

            print(f"{bcolors.OKBLUE}[+]{bcolors.ENDC} Se descargarán {len(jugadores)} jugadores y se añadirán a "
                  f"la base de datos. Puede tardar un rato.")

            # Descargar por bloques
            bsize = 500
            bloques = int(len(jugadores) / bsize) + 1
            for i in range(1, bloques + 1):
                # Descargar los jugadores
                if bsize * i > len(jugadores):
                    self._sf.get_player(jugadores[bsize * (i - 1):], roasters=roasters[bsize * (i - 1):])
                else:
                    self._sf.get_player(jugadores[bsize * (i - 1):bsize * i],
                                        roasters=roasters[bsize * (i - 1):bsize * i])

    def _alineacion_prediccion(self, to_radicale=False):
        """
        Solicita al servidor de Football Data si existen alineaciones
        disponibles para la jornada dada.

        Parameters
        ----------
        agregacion: str
            Tipo de agregación en la que exportar los datos.
        to_radicale: bool
            Exportar las fechas al servidor CalDAV

        Returns
        -------
        list
            Lista con las alineaciones disponibles para exportar
        """
        # Actualizar los partidos programados
        alineaciones = self._fd.get_starting_lineup()

        disponibles = []
        if alineaciones:
            print("═" * 86)
            for match in alineaciones:
                if match.available_lineups:
                    try:
                        # Agregar masivamente
                        jugadores, roasters = match.massive_download()
                        if jugadores and roasters:
                            self._sf.get_player(jugadores, roasters=roasters)
                        match.build()
                        disponibles.append(match)
                        print(f"{bcolors.OKGREEN}[+]{bcolors.ENDC} {match}")
                    except Exception as e:
                        print(f"{bcolors.FAIL}[!]{bcolors.ENDC} {match}: {e}")
                # else:
                #     print(f"{bcolors.FAIL}[!]{bcolors.ENDC} {match}")

                if to_radicale:
                    try:
                        self._to_caldav(match.matchday, match.datetime, match.local_team.name, match.away_team.name)
                    except Exception as e:
                        print(bcolors.FAIL + "[!] " + bcolors.ENDC + f"No se ha podido exportar a CalDAV: {e}")

            print("═" * 86)

        return disponibles

    def exportar_prediccion(self, to_radicale=False):
        """
        Exporta las alineaciones disponibles según el tipo de agregación.

        Parameters
        ----------
        to_radicale: bool
            Exportar las fechas al servidor CalDAV

        Examples
        --------
        >>> exportar_prediccion(to_radicale=True)
        """
        partidos = self._alineacion_prediccion(to_radicale=to_radicale)
        if not os.path.exists(self.config['PATHS']['predict_dir']):
            os.makedirs(self.config['PATHS']['predict_dir'], exist_ok=True)

        if partidos:
            ############################################
            # Crear la agregación shin_gasparyan
            ############################################
            # Header
            csv_header = "P,L_O,L_T,L_MO,L_POT,L_ME,L_DE,L_P,V_O,V_T,V_MO,V_POT,V_ME,V_DE,V_P,R\n"
            for p, partido in enumerate(partidos, start=1):
                line = partido.sg2str()
                csv_data = f"{csv_header}{p},{line}\n"

                # Guardar los datos
                name = os.path.join(
                    self.config['PATHS']['predict_dir'],
                    f"predict-{partido.date_full.replace(' ', '_')}-{partido.competition_id}-"
                    f"{unidecode(partido.local_team.name.replace(' ', '_'))}-"
                    f"{unidecode(partido.away_team.name.replace(' ', '_'))}-J{partido.matchday}"
                )

                with open(f"{name}.csv", "w") as f:
                    f.write(csv_data)

            print(f"{bcolors.OKGREEN}[*]{bcolors.ENDC} Predicción generada correctamente")
            ############################################
            # Crear la agregación revillas
            ############################################
            for p, partido in enumerate(partidos, start=1):
                name = os.path.join(
                    self.config['PATHS']['predict_dir'],
                    f"predict-{partido.date_full.replace(' ', '_')}-{partido.competition_id}-"
                    f"{unidecode(partido.local_team.name.replace(' ', '_'))}-"
                    f"{unidecode(partido.away_team.name.replace(' ', '_'))}-J{partido.matchday}"
                )

                local = partido.full_attr[0]
                # local_tactica = partido.local_team.tactica
                visitante = partido.full_attr[1]
                # visitante_tactica = partido.away_team.tactica

                # local_perms_idx = fixed_permutation(local_tactica)
                # np.random.shuffle(local_perms_idx)
                # visitante_perms_idx = fixed_permutation(visitante_tactica)
                # np.random.shuffle(visitante_perms_idx)

                # Reservar espacio para las máximas permutaciones posibles
                # max_perms = min(local_perms_idx.shape[0], visitante_perms_idx.shape[0])
                partido_np = np.zeros((22, 34))

                print(f"\rExportando el partido [{p:>3}/{len(partidos)}] "
                      f"[" + "#" * int(34 * p / len(partidos)) + " " * (34 - int(34 * p / len(partidos))) + "]",
                      end="")

                # Convertir los índices al partido
                # local_perm = np.array([np.array([local[i] for i in perm]) for perm in local_perms_idx[:max_perms]])
                # visitante_perm = np.array(
                #     [np.array([visitante[i] for i in perm]) for perm in visitante_perms_idx[:max_perms]])

                partido_np[:11, :] = local
                partido_np[11:, :] = visitante

                # Save numpy array compressed
                np.save(name, partido_np)

            print()
        else:
            print(bcolors.WARNING + "[!]" + bcolors.ENDC + f" Aún no hay alineaciones disponibles")

    def _parse_partidos(self, partidos: list):
        """
        Genera un json con la agregación de los partidos dados

        Parameters
        ----------
        partidos: list
            Lista de los partidos

        Returns
        -------
        list
            Lista con diccionarios de los partidos agregados
        """
        instantiated_partidos = []

        if partidos and len(partidos) > 0:
            # Realizar una pasada de todos los partidos juntando los ids de los jugadores
            # con la fecha del partido para buscarlos todos de forma asíncrona con ruia y no
            # jugador a jugador, si no parace que fuera síncrona y ruia no serviría de nada
            self._agregar_jugadores_masivamente(partidos)

            partidos_omitidos = 0
            for i, partido in enumerate(partidos, start=1):
                print(f"\r{bcolors.OKBLUE}[TEMP {partido['TEMPORADA']}]{bcolors.ENDC}" +
                      f" Analizando el partido [{i:>3}/{len(partidos):>3}] "
                      f"[" + "#" * int(39 * i / len(partidos)) + " " * (39 - int(39 * i / len(partidos))) + "]", end="")
                fdmatch = FDMatch()
                fdmatch.load_sql(partido)

                try:
                    # Cargar los equipos titulares
                    fdmatch.local_team.load_sql_lineup(partido['TEMPORADA'], partido['JORN'], partido['LOCAL_ID'])
                    fdmatch.away_team.load_sql_lineup(partido['TEMPORADA'], partido['JORN'], partido['VISITANTE_ID'])

                    # Generar las agregaciones de los equipos titulares
                    fdmatch.build()
                    instantiated_partidos.append(fdmatch)
                except Exception as e:
                    # No incluir el partido y listo
                    print(f"\n{bcolors.WARNING}[!]{bcolors.ENDC} id {e}. Omitiendo")
                    partidos_omitidos += 1
            print(f"\n\n{bcolors.OKBLUE}[*]{bcolors.ENDC} "
                  f"Partidos correctos: {round(100 * len(instantiated_partidos) / len(partidos), 2)}%; "
                  f"Partidos omitidos: {round(100 * partidos_omitidos / len(partidos), 2)}%")

        return instantiated_partidos

    def _create_season(self, temporada: str, fd_id_equipo=None):
        """
        Crea un json de la temporada dada con la información de los partidos
        almacenada en la base de datos una vez agregados.

        Parameters
        ----------
        agregacion: str
            Agregación a utilizar
        temporada: str
            Temporada a generar (Ej.: "18-19")
        fd_id_equipo: str
            Id del equipo a obtener sus resultados.

        Returns
        -------
        dict
        """
        temporada = parse_seasons(str(temporada), first=True)
        # name = os.path.join(self.config['PATHS']['json_file'].format(temporada=temporada,
        #                                                              agregacion=agregacion,
        #                                                              num_res=self._num_resultados,
        #                                                              fd_id_equipo='all' if not fd_id_equipo else fd_id_equipo))

        # Generar el archivo json
        partidos = self._data_request.get_partidos(
            temporada=temporada,
            fd_id_equipo=None if fd_id_equipo == 'all' else fd_id_equipo)
        partidos_disponibles = []

        if not partidos:
            # No se han encontrado partidos
            print(bcolors.WARNING + "[*] " + bcolors.ENDC + f"No hay partidos para la temporada {temporada}")
        else:
            partidos_disponibles = self._parse_partidos(partidos)

        return partidos_disponibles

    def create_csv(self, temporadas: str, fd_id_equipo=None):
        """
        Genera un archivo csv con los datos almacenados en la base de datos
        de las temporadas dadas.

        Parameters
        ----------
        temporadas: str
            Temporadas a generar (Ej.: "18-19")
        agregacion: str
            Agregación a utilizar
        fd_id_equipo: str
            Id del equipo a obtener sus resultados.
        from_file: bool
            Obtener la temporada desde el archivo json si existe
        """
        print(f"\n{'*' * 86}\n*{'Database --> Analysis --> .csv/.npz':^84}*\n{'*' * 86}")

        temporadas = parse_seasons(str(temporadas))
        for temporada in temporadas:
            # name = os.path.join(self.config['PATHS']['json_file'].format(temporada=temporada,
            #                                                              agregacion=agregacion,
            #                                                              num_res=self._num_resultados,
            #                                                              fd_id_equipo='all' if not fd_id_equipo else fd_id_equipo))
            #
            # if from_file and os.path.exists(name):
            #     with open(name, 'rb') as f:
            #         partidos = orjson.loads(f.read())
            # else:
            partidos = self._create_season(temporada, fd_id_equipo=fd_id_equipo)

            if partidos:
                ############################################
                # Crear la agregación shin_gasparyan
                ############################################
                # Crear la agregación oportuna
                # Header
                csv_header = "P, L_O, L_T, L_MO, L_POT, L_ME, L_DE, L_P, V_O, V_T, V_MO, V_POT, V_ME, V_DE, V_P, R\n"
                csv_data = csv_header
                for p, partido in enumerate(partidos, start=1):
                    line = partido.sg2str(num_res=self._num_resultados)
                    csv_data += f"{p},{line}\n"

                # Guardar los datos
                name = os.path.join(self.config['PATHS']['csv_file'].format(temporada=temporada,
                                                                            num_res=self._num_resultados,
                                                                            fd_id_equipo='all' if not fd_id_equipo else fd_id_equipo))

                # Crea el archivo csv
                if csv_data != "":
                    print(bcolors.OKBLUE + "[*]" + bcolors.ENDC +
                          f" Exportando los datos a csv con agregación 'shin_gasparyan'...")
                    with open(f"{name}", "w") as f:
                        f.write(csv_data)
                    print(bcolors.OKGREEN + "[+]" + bcolors.ENDC +
                          f" Conjunto de entrenamiento generado correctamente en {name}")
                else:
                    print(bcolors.FAIL + "[!]" + bcolors.ENDC +
                          f"No se ha generado el csv porque no hay partidos que guardar")

                ############################################
                # Crear la agregación revillas
                ############################################
                # Definir los numpy arrays y reservar espacio
                np_name = os.path.join(self.config['PATHS']['npz_file'].format(temporada=temporada,
                                                                               fd_id_equipo='all' if not fd_id_equipo else fd_id_equipo,
                                                                               num_res=self._num_resultados))
                temporada_np = np.zeros((len(partidos), 22, 34))
                temporada_res_np = np.zeros(len(partidos), dtype=int)

                if not os.path.exists(self.config['PATHS']['npz_dir']):
                    os.makedirs(self.config['PATHS']['npz_dir'], exist_ok=True)

                for p, partido in enumerate(partidos, start=1):
                    print(f"\r{bcolors.OKBLUE}[TEMP {temporada}]{bcolors.ENDC} "
                          f"Exportando el partido [{p:>3}/{len(partidos):>3}] "
                          f"[" + "#" * int(39 * p / len(partidos)) + " " * (39 - int(39 * p / len(partidos))) + "]",
                          end="")

                    local = partido.full_attr[0]
                    # local_tactica = partido.local_team.tactica
                    visitante = partido.full_attr[1]
                    # visitante_tactica = partido.away_team.tactica

                    # local_perms_idx = fixed_permutation(local_tactica)
                    # np.random.shuffle(local_perms_idx)
                    # visitante_perms_idx = fixed_permutation(visitante_tactica)
                    # np.random.shuffle(visitante_perms_idx)

                    # Convertir los índices al partido
                    # local_perm = np.array(
                    #     [np.array([local[i] for i in perm]) for perm in local_perms_idx[:max_perms]])
                    # visitante_perm = np.array(
                    #     [np.array([visitante[i] for i in perm]) for perm in visitante_perms_idx[:max_perms]])

                    temporada_np[p - 1, :11, :] = np.array(local)
                    temporada_np[p - 1, 11:, :] = np.array(visitante)
                    temporada_res_np[p - 1] = partido.res2str(self._num_resultados)

                # Save numpy array compressed
                print(f"\n{bcolors.OKBLUE}[*]{bcolors.ENDC} Guardando el archivo {np_name}...")
                np.savez_compressed(np_name, stats=temporada_np, results=temporada_res_np)
                print(bcolors.OKGREEN + "[+]" + bcolors.ENDC +
                      f"Conjunto de entrenamiento generado correctamente en {np_name}")

                print()
