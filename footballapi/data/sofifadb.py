#!/usr/bin/env python

import configparser
import os
import re
from datetime import datetime, date

import orjson
from MySQLdb._exceptions import IntegrityError

from footballapi.data.Database import Database
from footballapi.data.Singleton import Singleton
from footballapi.colors import bcolors

__author__ = "David Revillas"
__license__ = "MIT License"
__version__ = "0.9.7"
__maintainer__ = "David Revillas"
__email__ = "r3v1@pm.me"
__status__ = "Development"


class SoFifaDB(metaclass=Singleton):
    _months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    def __init__(self):
        self.db = Database(is_admin=True)
        self._history = []
        self._history_cache = {}

        # Archivo de configuración
        self.config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
        path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
        self.config.read(os.path.join(path, 'settings.cfg'))

    def insertar_id(self, sofifa_id, fd_id):
        """
        Inserta un par sofifa_id y fd_id en la base de datos

        Parameters
        ----------
        sofifa_id: str, int
            Sofifa id del jugador a insertar
        fd_id: str, int
            Football Data id del jugador a insertar
        """
        self.db.orden('INSERT INTO %s VALUES(%s, %s)', ('ID', sofifa_id, fd_id))

    def insertar_ids(self):
        """
        Inserta los ids que haya en player_id_table.json en la tabla de ids
        """

        with open(self.config['PATHS']['player_id_file'], 'rb') as f:
            data = orjson.loads(f.read())

        for key in data.keys():
            self.insertar_id(data[key], key)

    def insertar_jugador(self, player_stats):
        """
        Inserta el jugador y sus atributos en la base de datos

        Parameters
        ----------
        player_stats: iterable
            Atributos del jugador

        Raises
        ------
        MySQLdb._exceptions.IntegrityError
            En caso de no poder añadir el jugador por cualquier motivo
        """

        dim = self._dimensiones(player_stats)

        if dim <= 0:
            player_stats = [player_stats]

        for stats in player_stats:
            try:
                fecha = datetime.strptime(stats['date'], '%b %d, %Y').strftime('%Y-%m-%d')
            except ValueError:
                fecha = datetime.strptime(stats['date'], '%b %d %Y').strftime('%Y-%m-%d')

            # Insertar primero la fecha
            self.db.orden('INSERT INTO %s VALUES("%s")', ('FECHAS', fecha))

            # Insertamos la ficha del jugador
            try:
                self.db.orden('INSERT INTO %s VALUES("%s", "%s", %s, %s, "%s")',
                              ('JUGADOR', stats['sofifa_id'], stats['name'], stats['height'],
                               stats['weight'], stats['current_position']))

                general = stats['generic_stats'][0]
                potencial = stats['generic_stats'][1]
                ofensivos = ",".join(map(str, stats['specific_stats'][0:5]))
                tecnica = ",".join(map(str, stats['specific_stats'][5:10]))
                movimiento = ",".join(map(str, stats['specific_stats'][10:15]))
                potencia = ",".join(map(str, stats['specific_stats'][15:20]))
                mentalidad = ",".join(map(str, stats['specific_stats'][20:26]))
                defensa = ",".join(map(str, stats['specific_stats'][26:29]))
                portero = ",".join(map(str, stats['specific_stats'][29:]))

                # Insertamos los atributos
                assert potencial >= 0 and general >= 0
                self.db.orden('INSERT INTO %s VALUES("%s","%s",%s,%s)',
                              ('ATRGENERALES', stats['sofifa_id'], fecha, general, potencial))

                assert ofensivos.find('-') < 0
                self.db.orden('INSERT INTO %s VALUES("%s","%s",%s)',
                              ('ATROFENSIVO', stats['sofifa_id'], fecha, ofensivos))

                assert tecnica.find('-') < 0
                self.db.orden('INSERT INTO %s VALUES("%s","%s",%s)',
                              ('ATRTECNICA', stats['sofifa_id'], fecha, tecnica))

                assert movimiento.find('-') < 0
                self.db.orden('INSERT INTO %s VALUES("%s","%s",%s)',
                              ('ATRMOVIMIENTO', stats['sofifa_id'], fecha, movimiento))

                assert potencia.find('-') < 0
                self.db.orden('INSERT INTO %s VALUES("%s","%s",%s)',
                              ('ATRPOTENCIA', stats['sofifa_id'], fecha, potencia))

                assert mentalidad.find('-') < 0
                self.db.orden('INSERT INTO %s VALUES("%s","%s",%s)',
                              ('ATRMENTALIDAD', stats['sofifa_id'], fecha, mentalidad))

                assert defensa.find('-') < 0
                self.db.orden('INSERT INTO %s VALUES("%s","%s",%s)',
                              ('ATRDEFENSA', stats['sofifa_id'], fecha, defensa))

                assert portero.find('-') < 0
                self.db.orden('INSERT INTO %s VALUES("%s","%s",%s)',
                              ('ATRPORTERO', stats['sofifa_id'], fecha, portero))

            except IntegrityError as e:
                print(f"\n{bcolors.FAIL}[!]{bcolors.ENDC} No se puede insertar el jugador "
                      f"{stats['sofifa_id']}:{stats['name']} por que no esta registrado en la base de datos")
                raise e

    def contains_player_stats(self, sofifa_id, fecha):
        """
        Dado el sofifa id de un jugador y una fecha en formato month day, year,
        comprueba si existe dicho jugador registrado en la base de datos en esa fecha
        concreta. Es decir, si se existen o aún no se han descargado los datos del
        jugador de esa fecha.

        Parameters
        ----------
        sofifa_id: str
            Sofifa id del jugador
        fecha: str
            Fecha, i.e. month day, year

        Returns
        -------
        bool
        """
        if re.match(r'[a-zA-Z]{3} [0-9]{1,2}, [0-9]{4}', fecha):
            data = self.db.consulta("SELECT * FROM ATRGENERALES WHERE SOFIFA_ID=%s AND FECHA='%s'",
                                    (sofifa_id, datetime.strptime(fecha, '%b %d, %Y').strftime('%Y-%m-%d')))
        elif re.match(r'[0-9]{4}-[0-9]{2}-[0-9]{2}', fecha):
            data = self.db.consulta("SELECT * FROM ATRGENERALES WHERE SOFIFA_ID=%s AND FECHA='%s'", (sofifa_id, fecha))
        else:
            print(f"Fecha no definida: {fecha}")

        if data and len(data) > 0:
            return True
        else:
            return False

    def _dimensiones(self, lista):
        """
        Devuelve el número de dimensiones de la lista,
        asumiendo un tensor cuadrado

        Parameters
        ----------
        lista: iterable

        Returns
        -------
        int
            Número de dimensiones
        """

        if type(lista) == list:
            return 1 + self._dimensiones(lista[0])
        else:
            return 0

    def date2roaster(self, fecha):
        """
        Convierte una fecha month day, year a roaster.

        Parameters
        ----------
        fecha: str, date
            Fecha, i.e. month day, year

        Returns
        -------
        int
        """
        # Buscarla en caché primero
        if fecha in self._history_cache.keys():
            return self._history_cache[fecha]

        if type(fecha) == str:
            try:
                fecha = datetime.strptime(fecha, '%Y-%m-%d')
            except ValueError:
                fecha = datetime.strptime(fecha, '%b %d, %Y')
        elif type(fecha) == date:
            fecha = datetime(fecha.year, fecha.month, fecha.day)
        else:
            raise ValueError

        if len(self._history) == 0:
            with open(self.config['PATHS']['history_file'], 'r') as f:
                self._history = orjson.loads(f.read())['roasters']

        fechas = self._history.copy()
        fechas.sort(key=lambda x: abs((fecha - datetime.strptime(x[1], '%b %d, %Y')).days))
        # fechas = list(filter(lambda x: (fecha - datetime.strptime(x[1], '%b %d, %Y')).days >= 0, fechas))

        roaster = fechas[0][0]
        self._history_cache[fecha.strftime('%Y-%m-%d')] = roaster

        return roaster
