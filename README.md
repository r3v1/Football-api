# SISTEMA DE PREDICCIÓN DE PARTIDOS DE FÚTBOL

Introducción
============

El propósito de este proyecto es el de implementar el estudio que realizaron 
[Jongho Shin y Robert Gasparyan](http://cs229.stanford.edu/proj2014/Jongho%20Shin,%20Robert%20Gasparyan,%20A%20novel%20way%20to%20Soccer%20Match%20Prediction.pdf) 
acerca de un método novedoso de predicción de partidos de fútbol basado en 
datos de los jugadores previos al partido. Se trata de una forma de agregación
de datos en el que no se tienen en cuenta todos y cada uno de los datos, sino
que se agrupan los 5 **mejores** jugadores de cada categoría en una suma para
trabajar con ella. En pocas palabras, trabajar con un vector de 15 componentes
en vez de uno con 749 (por partido).

A diferencia de la manera clásica de predicción, basada en analizar las
estadísticas de partidos anteriores, como *disparos a puerta*, *faltas
cometidas*, *tarjetas recibidas* o *asistencias* entre otras, este
método utiliza 34 datos *virtuales* asociados de alguna manera al
*estado* físico del jugador. De esta manera, la predicción puede ser
llevada a cabo incluso antes de empezar el partido, únicamente con la
alineación inicial de los dos equipos.

Es decir, predecir el resultado en función de las características de los
jugadores y no de las estadísticas que podrían ofrecer los partidos
anteriores ya jugados.    

## Resumen formal
[Aquí](./doc/report01072019.pdf) se puede ver el informe redactado con los resultados obtenidos con una [versión
preliminar de la aplicación](https://gitlab.com/r3v1/Football-api/tree/v0.1) el 1 de julio de 2019.

## Instalación
1. Crear un entorno virtual con `virtualenv venv` y activarlo `. venv/bin/activate`. 
Luego instalar los paquetes con `pip install -r requirements.txt`.

### Contacto
EHU/UPV: `drevillas002@ikasle.ehu.eus`
