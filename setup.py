from setuptools import setup, find_packages

setup(
    name='footballapi',
    version='0.9.7',
    description='Machine Learning automated model development framework for Football analysis',
    author='David Revillas',
    url='https://gitlab.com/r3v1/Football-api',
    author_email='drevillas002@ikasle.ehu.eus',
    license='MIT',
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 4 - Beta',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Scientific/Engineering :: Artificial Intelligence',

        # Pick your license as you wish (should match "license" above)
         'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.

        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    keywords='ML development framework',
    packages=find_packages(),
    install_requires=[
        "scikit-learn==0.22.1",
        "pandas",
        "numpy",
        "orjson",
        "joblib",
        "tpot",
        "xgboost",
        "google-api-python-client",
        "unidecode",
        "seaborn",
        "matplotlib",
        "lxml",
        "mysqlclient",
        "aiohttp",
        "ruia",
        "pytest",
        "requests",
        "pydot",
        "scikit-posthocs",
        "caldav",
        "tensorflow-gpu==2.0.0",
    ],
)
